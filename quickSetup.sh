#!/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
#
# Cross check https://twiki.cern.ch/twiki/bin/view/AtlasProtected/RivetMCValidation for recent RIVET releases
#

#asetup 20.20.8.1,here #RIVET v2.5.3
#asetup 20.7.8.5,here #RIVET v2.5.2
#asetup 20.7.7.7,here,slc6,64 #RIVET v2.4.3
#asetup 19.2.5.9,here #RIVET v2.4.2
asetup 19.2.5.5,here #RIVET v2.4.0


export RIVETBASE=$PWD/routines/TopRoutines


