import os
theApp.EvtMax = -1

from AthenaCommon.AppMgr import ServiceMgr as svcMgr


import AthenaPoolCnvSvc.ReadAthenaPool

svcMgr.EventSelector.InputCollections=['/afs/cern.ch/work/s/sthenkel/public/forRIVETTutorial/EVNT.10867034._001484.pool.root.1']
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

OutputRoot  = "<root_filename>.root"
OutputYoda  = "<yoda_filename>.yoda"

from Rivet_i.Rivet_iConf import Rivet_i

rivet = Rivet_i("Rivet")
rivet.AnalysisPath = os.environ['PWD']

rivet.Analyses +=["MC_TTbar_TestSel"]
#rivet.Analyses +=["MC_TTbar_TruthSel"]
#rivet.Analyses += ["ATLAS_2014_I1304289"]
#rivet.Analyses += ["ATLAS_2015_I1345452"]

#rivet.Analyses += ["ATLAS_2014_I1304688"]
#rivet.Analyses += ["ATLAS_2013_I1243871"]

#rivet.Analyses += ['TTZ_analysis','hepmc_analysis','TTBAR_ANA']
#rivet.Analyses += [ 'MC_JET','PDFS','GENERIC','PHOTONS','PHOTONINC','WINC','TTBAR_ANA','HFJET_ANA','ZINC','JETTAGS']
#rivet.Analyses+=["TTBAR_ANA"]
rivet.RunName = ""
rivet.HistoFile = OutputYoda
# Specify MC cross section in pb (if necessary for your analysis)
rivet.CrossSection = xs
print '#####################'
print '####  USERINPUT  ####'
print '### >> : Cross-section -- ' + str(xs) + ' [pb]'
print '#####################'
job += rivet

from GaudiSvc.GaudiSvcConf import THistSvc
svcMgr += THistSvc()
svcMgr.THistSvc.Output = ["Rivet DATAFILE='"+OutputRoot+"' OPT='RECREATE'"]
