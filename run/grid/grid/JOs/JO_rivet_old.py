import os
theApp.EvtMax = -1

from AthenaCommon.AppMgr import ServiceMgr as svcMgr


import AthenaPoolCnvSvc.ReadAthenaPool
#for grid submission, the InputCollection serves as dummy for inDS
svcMgr.EventSelector.InputCollections=["/afs/cern.ch/user/s/sthenkel/eos/atlas/user/s/sthenkel/13TeV/MC/TOP_EVNT/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.evgen.EVNT.e3698/EVNT.06550123._024498.pool.root.1"]
svcMgr.EventSelector.SkipBadFiles = True
svcMgr.EventSelector.BackNavigation = True

from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

OutputRoot  = "<root_filename>.root"
OutputYoda  = "<yoda_filename>.yoda"

from Rivet_i.Rivet_iConf import Rivet_i
rivet = Rivet_i()
rivet.AnalysisPath = os.environ['PWD']


# analyses implemented in rivet
#rivet.Analyses += [ 'ATLAS_2013_I1243871','ATLAS_2014_I1304688' ]
# user defined routines
#rivet.Analyses += [ 'MC_TTbar_TruthSel', 'ATLAS_2014_I1304289', 'ATLAS_2015_I1345452' ]
rivet.Analyses += [ 'MC_TTbar_TruthSel' ]
rivet.RunName = ""
rivet.HistoFile = OutputYoda
#rivet.MCEventKey
#rivet.CrossSection = 1
job += rivet

from GaudiSvc.GaudiSvcConf import THistSvc
svcMgr += THistSvc()
svcMgr.THistSvc.Output = ["Rivet DATAFILE='"+OutputRoot+"' OPT='RECREATE'"]
