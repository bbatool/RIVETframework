#!/bin/python
import os,re,sys

path = os.environ['PWD']
Xsection = 831.76 # for top quark mass of 172.5 GeV (see https://twiki.cern.ch/twiki/bin/view/LHCPhysics/TtbarNNLO#Top_quark_pair_cross_sections_at)
Samples = {

    'mc15_13TeV.410501.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad.evgen.EVNT.e5458': 451.64568

    }

filesToInclude = [
    'RivetMC_MC_TTbar_TestSel.so',
    'MC_TTbar_TestSel.yoda'
    ]


UserName = os.environ['USER']
#UserName = 'sthenkel'

Version = 'RIVETtut'
JOs = path+'/JOs/JO_rivet_withXsec.py'


#athenaRel = '19.2.1.3,slc6'
#cmtConf = "x86_64-slc6-gcc47-opt"
nJobs = 1
FilesPerJob = 100000
GBperJob = 'MAX'
script_download = open("../download/download_jobs.sh","w")
script_download.write("#!/bin/bash \n")


commands = []
RegEx = re.compile( '(\w*)\.(\d*)\.(\w*)\.(\w*)\.(\w*)\.(\w*)/' )

for sample in Samples:
    Match = RegEx.match( sample )
    outSample = 'user.' + UserName + '.' + Version + '.' + Match.group( 2 ) + '.' + Match.group( 3 ) +'.' + Match.group( 5 ) + '.' + Match.group( 6 ) + '/'
    output_yoda = Match.group(2) + '_' +Match.group(6) +'.yoda'
    output_root = Match.group(2) + '_' +Match.group(6) +'.root'

    # JO handling and adjustment for each sample
    os.system("cp -r "+JOs+" "+path+"/JO_tmp.py")
    updated_JOs = path+"/JO_tmp.py"
    inf = open(updated_JOs).read()
    outf = open(updated_JOs, 'w')
    replacements = {'<yoda_filename>.yoda': output_yoda, '<root_filename>.root': output_root}

    for i in replacements.keys():
        inf = inf.replace(i, replacements[i])
    print 'In the JOs : '+updated_JOs+' replacing : '+str(i)+' with : '+str(replacements[i])
    outf.write(inf)
    outf.close

    (prefix, sep, suffix) = ("JO_tmp.py").rpartition('_')
    new_JOs = prefix + '_'+ Match.group(2) + '_' +Match.group(6) + '.py'
    os.system("mv JO_tmp.py "+new_JOs)

    # get files to include
    includeFiles = ','.join(filesToInclude)

    Xsec=Samples[sample]
    print '######## >> : '+str(Xsec)
    # Defining command and prepare sending the job
    print '###############################################################################'
    print '### >>> sending job for / preparing command : '+outSample
    print '###############################################################################\n'


    command = 'pathena -c \'xs = %f\' \'%s\' --extFile=\'%s\' --nJobs=\'%d\'  --nFilesPerJob=\'%s\' --long  --extOutFile=\'%s\' --inDS=\'%s\' --outDS=\'%s\' ' % ( Xsec, new_JOs, includeFiles, nJobs, FilesPerJob, output_yoda, sample, outSample)


    # create submission script
    submissionscript="sub_"+Match.group(2) + '_' +Match.group(6)+".sh"
    script_sub = open(submissionscript,"w")
    #       script.write("source /afs/cern.ch/atlas/software/dist/AtlasSetup/scripts/asetup.sh "+str(athenaVersion)+", here \n")
    script_sub.write( command + "\n")
    script_sub.close()
    (file_d, sep_d, suffix_d) = outSample.rpartition('/')
    # prepare download script to download files once done on eht grid
    script_download.write( "rucio download " + file_d +"_EXT0/"+"\n")
    script_download.write( "rucio download " + file_d +"_Rivet/"+"\n")


    print '###########################################################'
    print command
#    os.system(command)
    print '###########################################################'
    print '#### >>> Spit out submission script : '+str(submissionscript)
    print '###########################################################\n'
 #   print '###############################################################################'
 #   print '### >>> command : '+command
 #   print '###############################################################################\n'
 #   os.system( command )
    commands.append(command)

script_download.close()
print '###############################################\n'
print '#### >>> Spit out download script to ./downloads folder '

subscript="Finalsub_"+str(Version)+"_jobs.sh"
script_sub = open(subscript,"w")
script_sub.write("#!/bin/bash \n")
script_sub.write("export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase \n")
script_sub.write("source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh \n")
script_sub.write("asetup 20.7.7.7,slc6,64,here \n")
script_sub.write("voms-proxy-init -voms atlas \n")
script_sub.write("lsetup panda \n")
for com in commands:
    script_sub.write( com + "\n")
script_sub.close()
print '#### >>> Spit out final submission script ('+str(subscript)+') with all commands '
print '#### Execute, $> . '+str(subscript)+''







#    execfile("source "+path+"/sub_"+Match.group(2) + '_' +Match.group(6)+".sh")
#    os.system("chmod 755 batchscripts/"+str(jobname)+".lsf")
#execfile("source "+path+"/sub_410000_e3698.sh")
#execfile("source "+path+"/sub_"+Match.group(2) + '_' +Match.group(6)+".sh")
#    os.system("source sub_"+Match.group(2) + '_' +Match.group(6)+".sh")
#pathena --noBuild --extFile=RivetMC_TTbar_TruthSel.so,RivetMC_ATLAS_2014_I1304289.so,Steering_TTbar_Truth --nJobs=1 --nFilesPerJob=2 --long --extOutFile=Output_test.yoda --inDS=mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.evgen.EVNT.e3698/ --outDS=user.sthenkel.rivet_test_wSteer3.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.EVNT.e3698/ JO.py


#    command = 'pathena %s --athenaTag=%s --cmtConfig=%s --nJobs=%d --long --nFilesPerJob=%d --nGBPerJob=%s --inDS=%s --outDS=%s --extFile=%s' % (JOs, athenaRel, cmtConf, nJobs, FilesPerJob, GBperJob, sample, outSample, OwnRivetAnalysis)
