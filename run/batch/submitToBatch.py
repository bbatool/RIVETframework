#!/python
import glob,os,sys
import time,datetime

#function definition
def chunks(l, amount):
        if amount < 1:
            raise ValueError('amount must be positive integer')
        chunk_len = len(l) // amount
        leap_parts = len(l) % amount
        remainder = amount // 2  # make it symmetrical
        i = 0
        while i < len(l):
            remainder += leap_parts
            end_index = i + chunk_len
            if remainder >= amount:
                remainder -= amount
                end_index += 1
            yield l[i:end_index]
            i = end_index



#STEERING PART
routineToRun = ['ATLAS_2014_I1304289']
steeringFile= "Steering_ATLAS_2014_I1304289"
routineLocation = os.path.abspath('.')+"/routines"
#routineLocation = os.path.abspath('.')

Samples = {
	'/afs/cern.ch/work/m/mfenton/public/Pythia6_7TeV_MTop_Semi/': 'Powheg_Pythia6_7TeV_MTop_Semi'
#	'/afs/cern.ch/work/m/mfenton/public/Sherpa/mc12_7TeV.117283.Sherpa_CT10_ttbar_SingleLeptonM_MEPS_NLO.evgen.EVNT.e3484_tid04784477_00/': 'Sherpa_MEPSNLO_CT10'

	}
#SampleName = ['Pythia6_7TeV_MTop_Semi']
inputFileDir = '/afs/cern.ch/work/m/mfenton/public/Pythia6_7TeV_MTop_Semi/'
athenaVersion = 'AtlasProduction,19.2.1.3,slc6,64'
nJobs=20
QUEUE = "8nh"
ts = time.time()
timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d-%H-%M')





#print inputFiles
for sample in Samples:
	inputFiles = glob.glob(str(sample)+'*')
	chunk = list(chunks(inputFiles,nJobs))
	counter = 0
	for itemlist in chunk:
		loc = os.environ["PWD"]
		testarea=os.environ["TestArea"]+"/"
		os.system("mkdir -p BatchJobHandler")
		os.system("mkdir -p BatchJobOutput")
		os.system("mkdir -p BatchJobHandler/batchscripts")
		os.system("mkdir -p BatchJobHandler/batchscripts/"+str(Samples[sample])+"_"+str(timestamp))
		os.system("mkdir -p BatchJobHandler/LSF")
		os.system("mkdir -p BatchJobHandler/LSF/"+str(Samples[sample])+"_"+str(timestamp))
		os.system("mkdir -p BatchJobHandler/LSF/"+str(Samples[sample])+"_"+str(timestamp)+"/"+str(counter))

		command = "athena.py "+str(loc)+"/RunRivet_batch.py -c \"inputFiles = "+str(itemlist)+"; SampleName = '"+Samples[sample]+"'; routineLoc = '"+str(routineLocation)+"'; routine = "+str(routineToRun)+" \" "

	#	command = "athena.py RunRivet_batch.py -c \"inputFiles = "+str(itemlist)+"\""
		jobname = "batch_script_"+str(Samples[sample])+"_"+str(counter)

	# create submission script
		script = open("BatchJobHandler/batchscripts/"+str(Samples[sample])+"_"+str(timestamp)+"/batch_script_"+str(Samples[sample])+"_"+str(counter)+".lsf","w")
		script.write("#BSUB -J "+str(jobname)+" \n")
		script.write("#BSUB -o "+str(loc)+"/BatchJobHandler/LSF/"+str(Samples[sample])+"_"+str(timestamp)+"/"+str(counter)+"/"+str(jobname)+".log \n")
		script.write("#BSUB -e "+str(loc)+"/BatchJobHandler/LSF/"+str(Samples[sample])+"_"+str(timestamp)+"/"+str(counter)+"/"+str(jobname)+".err \n")
		script.write("#BSUB -q "+str(QUEUE)+"\n")
		script.write("\n")
		script.write("cd "+testarea+" \n")
		script.write("source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh \n")
		script.write("source /afs/cern.ch/atlas/software/dist/AtlasSetup/scripts/asetup.sh "+str(athenaVersion)+",here \n")
		script.write("cd "+str(loc)+" \n")
		script.write("mkdir -p BatchJobOutput/jobs \n")
		script.write("mkdir -p BatchJobOutput/jobs/"+str(QUEUE)+"/"+str(Samples[sample])+"_"+str(timestamp)+"_OUT"+" \n")
		script.write("mkdir -p BatchJobOutput/jobs/"+str(QUEUE)+"/"+str(Samples[sample])+"_"+str(timestamp)+"_OUT"+"/"+str(counter)+" \n")
		script.write("cd BatchJobOutput/jobs/"+str(QUEUE)+"/"+str(Samples[sample])+"_"+str(timestamp)+"_OUT"+"/"+str(counter)+" \n")
		script.write("cp "+str(loc)+"/"+str(steeringFile)+" . \n")
		script.write(command+"\n")
		script.write("cd - \n")
		script.close()
		print "Sending job no.: "+str(counter)
		print '###########################################################'
		print command
		print '###########################################################'
		os.system("chmod 755 BatchJobHandler/batchscripts/"+str(Samples[sample])+"_"+str(timestamp)+"/"+str(jobname)+".lsf")
		os.system("bsub -q "+str(QUEUE)+" < BatchJobHandler/batchscripts/"+str(Samples[sample])+"_"+str(timestamp)+"/"+str(jobname)+".lsf")
		counter+=1
		print 'started chunk no: ', str(counter)
