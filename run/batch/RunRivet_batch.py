
#include("GeneratorUtils/StdEvgenSetup.py")

theApp.EvtMax = -1
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
import AthenaPoolCnvSvc.ReadAthenaPool
from AthenaCommon.AlgSequence import AlgSequence
from Rivet_i.Rivet_iConf import Rivet_i
from GaudiSvc.GaudiSvcConf import THistSvc

svcMgr.EventSelector.InputCollections =[]
svcMgr.EventSelector.SkipBadFiles = True
svcMgr.EventSelector.BackNavigation = True
job = AlgSequence()

if 'inputFiles' in dir():
    print 'SUCCESS : Received --> '+str(inputFiles)
else:
    print "WARNING: No inputfiles defined"

svcMgr.EventSelector.InputCollections = inputFiles

if 'SampleName' in dir():
    print 'SUCCESS : Received --> '+str(SampleName)
else:
    SampleName = "test"

if 'routineLoc' in dir():
    print 'SUCCESS : Received --> '+str(routineLoc)
else:
    print "WARNING : No location for routines"

if 'routine' in dir():
    print 'SUCCESS : Received --> '+str(routine)
else:
    print "WARNING : No routine specified"


OutputRoot  = "Output_"+SampleName+".root"
OutputYoda  = "Output_"+SampleName+".yoda"

rivet = Rivet_i("Rivet")
#rivet.AnalysisPath = os.environ['PWD']+"/routines"
rivet.AnalysisPath = routineLoc
#rivet.Analyses += [ 'MC_TTbar_TruthSel' ]
#7TeV
#rivet.Analyses += [ 'ATLAS_2014_I1304688']
#rivet.Analyses += [ 'ATLAS_2013_I1243871']
#rivet.Analyses += [ 'ATLAS_2014_I1304289']
rivet.Analyses = routine
#Top-fiducial
#rivet.Analyses += [ 'ATLAS_2015_I1345452']
rivet.HistoFile = OutputYoda
#rivet.MCEventKey
#rivet.CrossSection = 1
job += rivet

svcMgr += THistSvc()
svcMgr.THistSvc.Output = ["Rivet DATAFILE='"+OutputRoot+"' OPT='RECREATE'"]
