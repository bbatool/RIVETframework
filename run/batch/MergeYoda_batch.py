#!/bin/python
import os,re,glob

path = os.environ['PWD']
Output_merge = "Output_merged.yoda"

inputDir = ""
inputDirs = glob.glob(str(inputDir)+'*_OUT')


for item in inputDirs:
    fileList = []
    os.chdir(path+"/"+item)
    (prefix, sep, suffix) = item.rpartition('_')
    (prefix2,sep2,suffix2) = prefix.rpartition('_')
    fileToMerge = "Output_"+prefix2+".yoda"
    print '### > '+fileToMerge
    counter=0
    loc = os.path.abspath('.')
    for subdirs,dirs, files in os.walk(loc):
        for file in files:
            if file == fileToMerge:
                fileLoc = os.path.join(subdirs, file)
                counter+=1
                fileList.append(str(fileLoc))
            else:
                continue
    command = "yodamerge -o "+Output_merge+" "
    print 'Merging yoda files in ' + item
    if os.path.isfile(path+"/"+item+"/"+Output_merge):
        print 'WARNING: Already merged the yoda files ->  will delete '+str(Output_merge)
        os.system("rm "+path+"/"+item+"/"+Output_merge)

    for yodaFile in fileList:
        command += yodaFile + " "
    print '####################################'
    print '####################################'
    print 'MERGING yoda files with : \n'+command
    os.system(  command  )
    del command
    os.chdir(path)


#for item in inputDirs:
#    print 'Merging yoda files in ' + item
#    if os.path.isfile(path+"/"+item+"/"+Output_merge):
#        print 'WARNING: Already merged the yoda files ->  will delete '+str(Output_merge)
#        os.system("rm "+path+"/"+item+"/"+Output_merge)

#    files = glob.glob(str(item)+'*')
#    print '#### > '+str(files)
#    command = "yodamerge -o "+Output_merge+" "
#    os.chdir(path+"/"+item)
#    for yodaFile in files:
#        command += path+"/"+yodaFile + " "
#    print '####################################'
#   print '####################################'
#    print 'MERGING yoda files with : \n'+command
#    os.system(  command  )
#    del command
#    os.chdir(path)

