#include("GeneratorUtils/StdEvgenSetup.py")
theApp.EvtMax = 200
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
import AthenaPoolCnvSvc.ReadAthenaPool
from AthenaCommon.AlgSequence import AlgSequence
from Rivet_i.Rivet_iConf import Rivet_i
from GaudiSvc.GaudiSvcConf import THistSvc

from AthenaCommon.AppMgr import ServiceMgr
ServiceMgr.MessageSvc.OutputLevel = INFO

svcMgr.EventSelector.InputCollections =[]
svcMgr.EventSelector.SkipBadFiles = True
svcMgr.EventSelector.BackNavigation = True
job = AlgSequence()


weight_list = [ 'nominal','2muF_NNPDF','0p5muF_NNPDF']


#InputFolder = "/afs/cern.ch/user/s/sthenkel/eos/atlas/user/s/sthenkel/13TeV/MC/TOP_EVNT/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.evgen.EVNT.e3698/"
#NumberOfFiles = 6
#InputFolder = "/afs/cern.ch/user/s/sthenkel/eos/atlas/user/s/sthenkel/13TeV/DiffXsec/user.sthenkel.DAODmatchedEVNT.mc15_13TeV.410000.evgen.EVNT_StreamEVGEN/"
#NumberOfFiles = 30
#InputFolder = "/afs/cern.ch/user/s/sthenkel/eos/atlas/user/s/sthenkel/13TeV/DiffXsec/AndreasFiles/"
InputFolder = "/afs/cern.ch/user/s/sthenkel/eos/atlas/user/s/sthenkel/13TeV/DiffXsec/mc15_valid.429430.PowhegPythia8EvtGen_A14_ttbar_nonallhad_hdamp_0p5mt.evgen.EVNT.e5258/"
NumberOfFiles = 1000

#InputFolder = "/afs/cern.ch/user/s/sthenkel/work/Physics/Analysis/DiffXsec/RIVETValidation/files/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_TOPQ1.e3698_a766_a810_r6282_p2516/"
OutputRoot  = "Output_"
OutputYoda  = "Output_"
FileList = os.listdir(InputFolder)

startfile = 0
for file in FileList[startfile:NumberOfFiles+startfile]:
#    if not "1.pool.root" in file:
#        continue
    inputFile = InputFolder+file
#    inputFile = "/afs/cern.ch/user/s/sthenkel/eos/atlas/user/s/sthenkel/13TeV/DiffXsec/user.sthenkel.DAODmatchedEVNT.mc15_13TeV.410000.evgen.EVNT_StreamEVGEN/user.sthenkel.7832230.StreamEVGEN._000007.pool.root"
    print 'DEBUG :: : '+str(inputFile)
    if os.path.exists(inputFile):
        svcMgr.EventSelector.InputCollections.append(inputFile)

from PyUtils import AthFile
af = AthFile.fopen(svcMgr.EventSelector.InputCollections[0])
nentries = af.fileinfos['nentries']
streamname = af.fileinfos['stream_names']
print 'DEBUG :: all info  >', af.fileinfos
print 'INFO :: nentries   > ', nentries
print 'INFO :: streamname > ', streamname


for w in weight_list:
    rivet = Rivet_i("Rivet_"+str(w.replace(".","_")))
    rivet.AnalysisPath = os.environ['PWD']+"/routines"
    rivet.Analyses += [ 'BoostedDiffXsec' ]
    rivet.Analyses += [ 'Resolved13TeVljets' ]

    rivet.HistoFile = OutputYoda+str(w)+".yoda"
    rivet.DoRootHistos = False
#rivet.MCEventKey
    rivet.WeightName = " "+w+" "
    rivet.CrossSection = 731.11

    job += rivet
#    svcMgr += THistSvc()
#    svcMgr.THistSvc.Output = ["Rivet_"+str(w.replace(".","_"))+" DATAFILE='"+OutputRoot+str(w)+".root"+"' OPT='RECREATE'"]


svcMgr += CfgMgr.AthenaEventLoopMgr(EventPrintoutInterval=1)
