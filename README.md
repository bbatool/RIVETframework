This package contains some instructions on how to use and run RIVET
locally, on the lxbatch and the grid. It started growing from the instructions
given in the following twiki pages:
https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/RivetForAtlas (Using RIVET within the ATLAS framework athena)
https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/RivetTTbarMCValidation (Using RIVET routine with existing top physics related routines)
https://twiki.cern.ch/twiki/bin/view/AtlasProtected/RivetMCValidation (Needs and tips for Rivet validation)


It is far beyond perfect but gets things running. Some scripts are really minimal and
you might ask yourself, why I defined things in such a way, but this is just the workflow
I got used to.
Each folder has a README to sketch how you can setup and run the code.

To get started do:

$> . quickSetup.sh


Please, let me know in case you want improvements etc. and of course always contact me
in case of questions!


steffen.henkelmann@cern.ch


Additional useful information:
A great tutorial by Andy Buckley was part of the ATLAS-CMS MC Workshop 2017
https://indico.cern.ch/event/588781/contributions/2547540/attachments/1454630/2244534/buckley-rivettutorial.pdf