#!/bin/bash

# setup athena
echo setting up athena
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
asetup 20.7.7.7,slc6,64,here

export RIVET_ANALYSIS_PATH=$PWD
export RIVET_DATA_PATH=$PWD

athena.py AddRivet.py