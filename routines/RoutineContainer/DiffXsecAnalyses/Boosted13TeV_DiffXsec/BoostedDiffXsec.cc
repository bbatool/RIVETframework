 /**
 * BOOSTEDDIFFXS
 * Calculation of the boosted ttbar production differential cross-section @ 13 TeV
 * Author: Steffen Henkelmann
 * steffen.henkelmann@cern.ch
 * based on <RIVET routine number> 8 TeV
 **/


// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/FastJets.hh"
#include <bitset>

#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/UnstableFinalState.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Tools/ParticleIdUtils.hh"
#include "Rivet/Particle.hh"
#include "HepMC/GenEvent.h"
#include "fastjet/tools/Filter.hh"
#include <vector>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>

// substructure includes included in fjcontrib-1.021 (http://fastjet.hepforge.org/contrib/)
#include "fastjet/contrib/Njettiness.hh"
#include "fastjet/contrib/Nsubjettiness.hh"
#include "fastjet/contrib/NjettinessPlugin.hh"



namespace Rivet {
 using namespace Cuts;

  class BoostedDiffXsec : public Analysis {
  public:

    /// Constructor
    BoostedDiffXsec()
      : Analysis("BoostedDiffXsec")
    {    }


  public:

    void init() {
      Cut eta_full = etaIn(-5.0, 5.0) & (pT >= 1.0*MeV);
      const FinalState fs(eta_full);
      addProjection(fs, "ALL_FS");


      // Get photons used to dress leptons
      IdentifiedFinalState Photon(fs);
      Photon.acceptId(PID::PHOTON);

       // Muons and electrons must have |eta| < 2.5
      Cut eta_ranges = (eta>-2.5&&eta<2.5);
      // Use all bare electrons as input to the DressedElectrons projection
      IdentifiedFinalState el_id(fs);
      el_id.acceptIdPair(PID::ELECTRON);
      PromptFinalState electrons(el_id);
      electrons.acceptTauDecays(true);//lmassa put true
      addProjection(electrons, "electrons");
      // Get dressed electrons with pT > 15, since those with 15 < pT < 25 are classified as 'veto' electrons
      DressedLeptons dressed_electrons(Photon, electrons, 0.1, eta_ranges, true, true); //last bool: false= no photons from decay, true photons from decay
      addProjection(dressed_electrons, "DRESSED_ELECTRONS");


      // Use all bare muons as input to the DressedMuons projection
      IdentifiedFinalState mu_id(fs);
      mu_id.acceptIdPair(PID::MUON);
      PromptFinalState muons(mu_id);
      muons.acceptTauDecays(true);//lmassa put true
      addProjection(muons, "muons");


      // Get dressed muons with pT > 15, since those with 15 < pT < 25 are classified as 'veto' muons
      DressedLeptons dressed_muons(Photon, muons, 0.1, eta_ranges, true, true);
      addProjection(dressed_muons, "DRESSED_MUONS");


      // Get all neutrinos.
      //      Cut eta_neutrino = eta>-4.5 && eta<4.5;
      //      IdentifiedFinalState nu_id(eta_neutrino);
      IdentifiedFinalState neutrino_fs(fs);
      neutrino_fs.acceptNeutrinos();
      addProjection(neutrino_fs, "NEUTRINO_FS");

     

      /// Histograms:

      h_cutflow_muon = bookHisto1D("h_cutflow_muon", 20, 0.0, 20.0);
      h_cutflow_electron = bookHisto1D("h_cutflow_electron", 20, 0.0, 20.0);

      vector<double> bins;
      bins.push_back(300);
      bins.push_back(350);
      bins.push_back(400);
      bins.push_back(450);
      bins.push_back(500);
      bins.push_back(550);
      bins.push_back(650);
      bins.push_back(750);
      //      bins.push_back(1200);
      bins.push_back(1500);

      std::vector< double > binedges_rap;
      binedges_rap.push_back(-2.); binedges_rap.push_back(-1.8);binedges_rap.push_back(-1.6); binedges_rap.push_back(-1.4); binedges_rap.push_back(-1.2); binedges_rap.push_back(-1.);binedges_rap.push_back(-0.8); binedges_rap.push_back(-0.6); binedges_rap.push_back(-.4); binedges_rap.push_back(-.2);
      binedges_rap.push_back(0.);binedges_rap.push_back(0.2); binedges_rap.push_back(0.4); binedges_rap.push_back(.6); binedges_rap.push_back(.8); binedges_rap.push_back(1.);binedges_rap.push_back(1.2);binedges_rap.push_back(1.4);binedges_rap.push_back(1.6);binedges_rap.push_back(1.8);binedges_rap.push_back(2.0);


      std::vector< double > binedges_absrap;
      binedges_absrap.push_back(0.);binedges_absrap.push_back(0.2); binedges_absrap.push_back(0.4); binedges_absrap.push_back(.6); binedges_absrap.push_back(.8); binedges_absrap.push_back(1.);binedges_absrap.push_back(1.2);binedges_absrap.push_back(1.4);binedges_absrap.push_back(1.6);binedges_absrap.push_back(1.8);binedges_absrap.push_back(2.0);

      h_hadtop_pt_electron = bookHisto1D("h_hadtop_pt_electron", bins);
      h_hadtop_diffXsec_pt_electron = bookHisto1D("h_hadtop_diffXsec_pt_electron", bins);

      h_hadtop_pt_muon = bookHisto1D("h_hadtop_pt_muon", bins);
      h_hadtop_diffXsec_pt_muon = bookHisto1D("h_hadtop_diffXsec_pt_muon", bins);

      h_hadtop_pt_sum = bookHisto1D("h_hadtop_pt_sum", bins);
      h_hadtop_diffXsec_pt_sum = bookHisto1D("h_hadtop_diffXsec_pt_sum", bins);



      h_hadtop_rap_electron = bookHisto1D("h_hadtop_rap_electron", binedges_rap);
      h_hadtop_diffXsec_rap_electron = bookHisto1D("h_hadtop_diffXsec_rap_electron", binedges_rap);

      h_hadtop_rap_muon = bookHisto1D("h_hadtop_rap_muon", binedges_rap);
      h_hadtop_diffXsec_rap_muon = bookHisto1D("h_hadtop_diffXsec_rap_muon", binedges_rap);

      h_hadtop_rap_sum = bookHisto1D("h_hadtop_rap_sum", binedges_rap);
      h_hadtop_diffXsec_rap_sum = bookHisto1D("h_hadtop_diffXsec_rap_sum", binedges_rap);


      h_hadtop_absrap_electron = bookHisto1D("h_hadtop_absrap_electron", binedges_absrap);
      h_hadtop_diffXsec_absrap_electron = bookHisto1D("h_hadtop_diffXsec_absrap_electron", binedges_absrap);

      h_hadtop_absrap_muon = bookHisto1D("h_hadtop_absrap_muon", binedges_absrap);
      h_hadtop_diffXsec_absrap_muon = bookHisto1D("h_hadtop_diffXsec_absrap_muon", binedges_absrap);

      h_hadtop_absrap_sum = bookHisto1D("h_hadtop_absrap_sum", binedges_absrap);
      h_hadtop_diffXsec_absrap_sum = bookHisto1D("h_hadtop_diffXsec_absrap_sum", binedges_absrap);

      std::vector< double > binedges_ttbar;
      binedges_ttbar.push_back(250.0); binedges_ttbar.push_back(275.0); binedges_ttbar.push_back(300.0); binedges_ttbar.push_back(325.0);
      binedges_ttbar.push_back(350.0); binedges_ttbar.push_back(400.0); binedges_ttbar.push_back(450.0); binedges_ttbar.push_back(500.0);
      binedges_ttbar.push_back(575.0); binedges_ttbar.push_back(650.0); binedges_ttbar.push_back(725.0); binedges_ttbar.push_back(900.0);
      _h_ttbar_M    = bookHisto1D("ttbar_M",      binedges_ttbar);

      _h_ttbar_rap  = bookHisto1D("ttbar_rap",    binedges_rap);
      _h_ttbar_phi  = bookHisto1D("ttbar_phi",    100, -6.3, 6.3);

      _h_ttbar_absrap = bookHisto1D("ttbar_absrap",binedges_absrap);



    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const double weight = event.weight();

      // Vetoed particles vector will be filled with the selected leptons and photons which we do NOT want to include in the Jet clustering
      vector<const GenParticle*> vetoed_particles;

      /***********
       *  MUONS  *
       ***********/

      /// Get the dressed muons from the event record (they have pT > 15 and |eta| < 2.5 at this stage)
      const vector<DressedLepton>& clustered_muons = applyProjection<DressedLeptons>(event, "DRESSED_MUONS").dressedLeptons();
      vector<const DressedLepton*> muons_from_top;

      // Remove any muons which come from the decay of a hadron
      vector<const DressedLepton*> real_muons;

      foreach(const DressedLepton& l, clustered_muons) {

        bool keep = true;

	if(l.constituentLepton().fromDecay()&& !l.constituentLepton().fromPromptTau()) keep = false; //remove leptons from hadrons, but takes leptons from taus, which are signal.
        if(keep) real_muons.push_back(&l);
      }

      // Store any muons which have pt>25 as good muons, and 15 < pT < 25 as veto muons
      vector<const DressedLepton*> good_muons;
      vector<const DressedLepton*> veto_muons;
      foreach(const DressedLepton* l, real_muons) {
        double pt = l->momentum().pt();
	
        if(pt > 25.0*GeV) good_muons.push_back(l);
        if(pt <= 25.0*GeV&&pt>15.0*GeV) veto_muons.push_back(l);

        // Add the muons and the dressing photons to the vector of vetoed particles
        const GenParticle* muon = l->constituentLepton().genParticle();
        vetoed_particles.push_back(muon);

        foreach(const Particle& photon, l->constituentPhotons()) {
          const GenParticle* p = photon.genParticle();
          vetoed_particles.push_back(p);
        }

      }

      /**************
       *  ELECTRONS *
       **************/
      /// Get the dressed electrons from the event record (they have pT > 15 and |eta| < 2.5 at this stage)
      const vector<DressedLepton>& clustered_electrons = applyProjection<DressedLeptons>(event, "DRESSED_ELECTRONS").dressedLeptons();

      vector<const DressedLepton*> electrons_from_top;


      // Remove any electrons which come from the decay of a hadron
      vector<const DressedLepton*> real_electrons;

      foreach(const DressedLepton& l, clustered_electrons) {

        bool keep = true;

	if(l.constituentLepton().fromDecay()&& !l.constituentLepton().fromPromptTau()) keep = false; //remove leptons from hadrons, but takes leptons from taus, which are signal.
        if(keep) real_electrons.push_back(&l);
      }

      // Store any electrons which have pt>25 as good electrons, and 15 < pT < 25 as veto electrons
      vector<const DressedLepton*> good_electrons;
      vector<const DressedLepton*> veto_electrons;
      foreach(const DressedLepton* l, real_electrons) {
        double pt = l->momentum().pt();

        if(pt > 25.0*GeV) good_electrons.push_back(l);
        if(pt <= 25.0*GeV&&pt>15.0*GeV) veto_electrons.push_back(l);


        // Add the electrons and the dressing photons to the vector of vetoed particles
        const GenParticle* electron = l->constituentLepton().genParticle();
        vetoed_particles.push_back(electron);

        foreach(const Particle& photon, l->constituentPhotons()) {
          const GenParticle* p = photon.genParticle();
          vetoed_particles.push_back(p);
        }
      }




      // /******************************
      //  *    ELECTRON-MUON OVERLAP   *
      //  ******************************/
      // // Check whether we have any electrons and muons which overlap
      // bool emu_overlap = false;
      // foreach(const DressedLepton* electron, good_electrons) {
      //   foreach(const DressedLepton* muon, good_muons) {
      //     double d_theta = fabs(muon->momentum().theta() - electron->momentum().theta());
      //     double d_phi = fabs(muon->momentum().phi() - electron->momentum().phi());
      //     if(d_theta < 0.005 && d_phi < 0.005) emu_overlap = true;
      //   }
      // }

      /****************
       *  NEUTRINOS   *
       ****************/
      // Get the neutrinos from the event record (they have pT > 0.0 and |eta| < 4.5 at this stage
      const Particles& neutrinos = applyProjection<IdentifiedFinalState>(event, "NEUTRINO_FS").particlesByPt();
      vector<const Particle*> good_neutrinos;
      FourMomentum neutrino_sum;
      foreach(const Particle& n, neutrinos) {
        bool keep = true;
        // If the neutrino comes from a hadron decay, skip it
	if(n.fromDecay()&&!n.fromPromptTau()) keep = false;//remove leptons from hadrons, but takes leptons from taus, which are signal.
        if(keep)
		{
          // Keep the good neutrinos, and add them the to vector of vetoed particles
          good_neutrinos.push_back(&n);
          vetoed_particles.push_back(n.genParticle());
          neutrino_sum += n.momentum();
        }
      }

      /**************
       * B-HADRONS  *
       **************/
      vector<Particle> b_hadrons;
      // Get the b-Hadrons with pT > 5 GeV,to add to the final-state particles used for jet clustering.
      vector<HepMC::GenParticle*> allParticles = particles(event.genEvent());
      for (size_t i = 0; i < allParticles.size(); i++) {
       GenParticle* p = allParticles[i];
        // If the particle is a b-hadron and has pT > 5 GeV, add it to the bhadrons vector
        if(PID::isHadron(p->pdg_id()) && PID::hasBottom(p->pdg_id()) && p->momentum().perp() > 5) {
          Particle b_hadron = Particle(p);
          b_hadrons.push_back(p);
        }
      }

      /********************
       *    JET INPUTS    *
       ********************/
      // Jets need to be built out of the final state particles EXCLUDING the good leptons found so far (electron, muons and neutrinos)
      const Particles& final_state = applyProjection<FinalState>(event, "ALL_FS").particles();
      // Remove the vetoed particles from the input to the jet clustering
      Particles vetoed_final_state = RemoveFromFinalState(final_state, vetoed_particles);

      // Add b-hadrons as ghosts, to allow truth-level b-tagging
      AddGhostBhadrons(&vetoed_final_state, &b_hadrons);

      /**************
       *    JETS    *
       **************/
      // Create the FastJets projection and make the jets
      FastJets fjets(FastJets::ANTIKT, 0.4);
      fjets.calc(vetoed_final_state);


      Cut JetCut = (pT>25*GeV) && (eta>-2.5&&eta<2.5);
      Jets good_jets = fjets.jetsByPt(JetCut);


      /*********************
       *    LargeR JETS    *
       *********************/
      // Create the FastJets projection and make the fat jets (AntiKt 1)
      //Antikt 1, pt=300gev mass=100gev d12=40gev
      double R_FJ = 1.0;
      //Trimming
      double trimmed_FJ_pt_low = 300.;
      double trimmed_FJ_pt_up = 1500.;
      //      double trimmed_FJ_m = 50.;
      //      double trimmed_FJ_d12 = 40.;
      double trimmed_FJ_eta = 2.;



      FastJets fatJets(FastJets::ANTIKT, R_FJ);
      fatJets.calc( vetoed_final_state );
      PseudoJets good_fatJets = fatJets.pseudoJetsByPt(0);

      // // trim the jets
      std::vector<fastjet::PseudoJet> vector_fatJets;
      for (unsigned int i=0; i<good_fatJets.size();i++)	vector_fatJets.push_back((fastjet::PseudoJet)(good_fatJets.at(i)));

      float Rfilt = 0.2;
      float pt_fraction_min = 0.05;
      fastjet::Filter trimmer(fastjet::JetDefinition(fastjet::kt_algorithm, Rfilt),
			      fastjet::SelectorPtFractionMin(pt_fraction_min)
			      );

      std::vector<fastjet::PseudoJet> unsortedTrimmedFatJets;
      for (unsigned int i=0; i<vector_fatJets.size();i++) unsortedTrimmedFatJets.push_back(trimmer(vector_fatJets.at(i)));
      std::vector<fastjet::PseudoJet> trimmed_good_fatJets = fastjet::sorted_by_pt(unsortedTrimmedFatJets);


      std::vector<fastjet::PseudoJet> selected_trimmed_good_fatJets;
      for (unsigned int i=0; i<trimmed_good_fatJets.size();i++)
	{
	  if(//momentum(trimmed_good_fatJets.at(i)).mass() > trimmed_FJ_m &&
	     momentum(trimmed_good_fatJets.at(i)).pt() > trimmed_FJ_pt_low &&
	     momentum(trimmed_good_fatJets.at(i)).pt() < trimmed_FJ_pt_up &&
	     fabs(momentum(trimmed_good_fatJets.at(i)).eta()) < trimmed_FJ_eta )
	    selected_trimmed_good_fatJets.push_back(trimmed_good_fatJets.at(i));
	}

      /****************
       *    B-JETS    *
       ****************/
      // Define b-jets as those which contain a ghost b-hadron
      Jets bjets;
      Jets non_bjets;
      foreach(Jet j, good_jets) {

        bool b_tagged = false;
        foreach(Particle b, b_hadrons){
          if(j.containsParticle(b)) b_tagged = true;
        }
        if(b_tagged) bjets.push_back(j);
        if(!b_tagged) non_bjets.push_back(j);

      }


      unsigned int icut = 0;
      /******************************
       *    ELECTRON CHANNEL CUTS   *
       ******************************/
	{
	  CountEvent(h_cutflow_electron, icut, weight);
	{

        // 1. More than zero good electrons
        if(good_electrons.size() > 0) {
          CountEvent(h_cutflow_electron, icut, weight);
	  std::cout << " DEBUG :: electron -- (" << good_electrons.size() << ")"
		    << " pt :: > " << good_electrons.at(0)->momentum().pt()
		    << " phi :: > "<< good_electrons.at(0)->momentum().phi()
		    << std::endl;
	    // 2. No additional electrons passing the veto selection
          if(good_electrons.size() == 1) {
            CountEvent(h_cutflow_electron, icut, weight);
            // 3. No muons passing the veto selection
            if(good_muons.size() == 0) {
              CountEvent(h_cutflow_electron, icut, weight);

              // 4. Remove events with e-mu overlap
	      //              if(emu_overlap == false) {
		if(neutrino_sum.pt() > 20){
		  CountEvent(h_cutflow_electron, icut, weight);
		  // 5. MTW + total neutrino pT > 60GeV
		  double transmass = TransMass( good_electrons.at(0)->momentum().pt(),
						good_electrons.at(0)->momentum().phi(),
						neutrino_sum.pt(),
						neutrino_sum.phi());
		  if(transmass+neutrino_sum.pt() > 60) {
		    CountEvent(h_cutflow_electron, icut, weight);
		    // 6. At least one good fatjet (pT > 300GeV && |eta| < 2.0) (passLJet)
		    if(selected_trimmed_good_fatJets.size() >= 1 ) {
		      CountEvent(h_cutflow_electron, icut, weight);
		      int fatJetIndex = -1;
		      double FJ_m_cut = 100.;
		      double FJ_pt_cut = 300.;
		      double FJ_eta_cut = 2.;
		      double trimmed_FJ_tau32 = .75; // top-tagging variable
		      bool passTopTag = false;
		      unsigned int j=0;
		      for (;j<selected_trimmed_good_fatJets.size();j++)
			{
			  // 7. good fat jet passing top tagging cut (Nsubjettiness ratio, passTopTag)

      			  if( tau32( selected_trimmed_good_fatJets.at(j), R_FJ ) < trimmed_FJ_tau32 &&
			      momentum(selected_trimmed_good_fatJets.at(j)).mass() > FJ_m_cut &&
			      momentum(selected_trimmed_good_fatJets.at(j)).pt() > FJ_pt_cut &&
			      fabs(momentum(selected_trimmed_good_fatJets.at(j)).eta()) < FJ_eta_cut) {
			    passTopTag = true;
			    fatJetIndex=j;
			  }
			}
		      if( passTopTag && fatJetIndex != -1 ) {
			CountEvent(h_cutflow_electron, icut, weight);
			// 8. dPhi between lepton and fatjet (passDphi)
			bool passDphi = false;
			double dPhi_fatjet = deltaPhi( good_electrons.at(0)->momentum().phi(),
						       momentum(selected_trimmed_good_fatJets.at(fatJetIndex)).phi());
			double dPhi_fatjet_lep_cut  = 1.0; //2.3
			if( dPhi_fatjet > dPhi_fatjet_lep_cut ){
			  passDphi = true;
			}
			if( passDphi ) {
			  CountEvent(h_cutflow_electron, icut, weight);
			  // 9. At least one b-tagged jet (passbtag)
			  if(bjets.size() > 0) {
			    CountEvent(h_cutflow_electron, icut, weight);
			    bool lepbtag=false;
			    bool hadbtag=false;


			    // 10. dR between jet and fatjet (passAddJet)
			    bool passAddJet = false;
			    foreach(const Jet& j, good_jets)
			      {
				double dR = deltaR(j.momentum(), momentum(selected_trimmed_good_fatJets.at(fatJetIndex)));
				double dR_fatjet_jet_cut  = 1.5;
			      if(dR > dR_fatjet_jet_cut) {
				passAddJet = true;
				break;
			      }
			    }
			  if(passAddJet) {
			    CountEvent(h_cutflow_electron, icut, weight);
			    // 11. dR between jet and fatjet && dR between jet and lepton (passLepJet)
			    bool goodLepJet = false;
			    int lepJetIndex = -1;
			    int index = 0;

			    foreach(const Jet& j, good_jets)
			      {
				double dR_jet_lep = deltaR(j.momentum(), good_electrons.at(0)->momentum());
				double dR_jet_fatjet = deltaR(j.momentum(), momentum(selected_trimmed_good_fatJets.at(fatJetIndex)));
				double dR_jet_lep_cut = 2.0;//1.5
				double dR_jet_fatjet_cut = 1.5;//1.5
				if(dR_jet_lep < dR_jet_lep_cut && dR_jet_fatjet > dR_jet_fatjet_cut) {
				  lepJetIndex = index;
				  goodLepJet = true;
				  break;
				}
				++index;
			      }


			    // if (lepJetIndex != -1){
			    //   dR_fatjet = deltaR(good_jets.at(lepJetIndex).momentum(), momentum(selected_trimmed_good_fatJets.at(fatJetIndex)));
			    //   double dR_fatjet_jet_cut  = 1.5;
			    //   if (dR_fatjet > dR_fatjet_jet_cut ){
			    // 	goodLepJet = true;
			    //   }
			    // }

			    if(goodLepJet) {
			      CountEvent(h_cutflow_electron, icut, weight);

			      foreach(Particle b, b_hadrons){
				if(good_jets.at(lepJetIndex).containsParticle(b)){
				  lepbtag = true;
				}
			      }

			      double dR_fatBjet_cut=1.0;
			      for (unsigned int i=0; i<bjets.size(); i++) {
				if (deltaR(momentum(selected_trimmed_good_fatJets.at(fatJetIndex)), bjets.at(i).momentum()) < dR_fatBjet_cut )
				  {
				    hadbtag=true;
				  }
			      }
			      // 12. dR between lep and bjet (goodjet at lepJetIndex is within dR < 2.0 of lepton) ||
			      // dR between fatJet and bjet (passBTagBatch)
			      if(lepbtag || hadbtag) CountEvent(h_cutflow_electron, icut, weight);

			      int btag_category=0;
			      if(hadbtag && lepbtag) btag_category=1;
			      if(hadbtag && !lepbtag) btag_category=2;
			      if(!hadbtag && lepbtag) btag_category=3;

			      if (btag_category==1||btag_category==2||btag_category==3){
				h_cutflow_electron->fill(15+0.5, weight);
				double hadtop_pt= momentum(selected_trimmed_good_fatJets.at(fatJetIndex)).pt();
				double hadtop_rap= momentum(selected_trimmed_good_fatJets.at(fatJetIndex)).eta();
				double hadtop_absrap= fabs(momentum(selected_trimmed_good_fatJets.at(fatJetIndex)).eta());

				h_hadtop_pt_electron->fill(hadtop_pt, weight);
				h_hadtop_diffXsec_pt_electron->fill(hadtop_pt, weight);

				h_hadtop_pt_sum->fill(hadtop_pt, weight);
				h_hadtop_diffXsec_pt_sum->fill(hadtop_pt, weight);

				h_hadtop_rap_electron->fill(hadtop_rap, weight);
				h_hadtop_diffXsec_rap_electron->fill(hadtop_rap, weight);

				h_hadtop_rap_sum->fill(hadtop_rap, weight);
				h_hadtop_diffXsec_rap_sum->fill(hadtop_rap, weight);

				h_hadtop_absrap_electron->fill(hadtop_absrap, weight);
				h_hadtop_diffXsec_absrap_electron->fill(hadtop_absrap, weight);

				h_hadtop_absrap_sum->fill(hadtop_absrap, weight);
				h_hadtop_diffXsec_absrap_sum->fill(hadtop_absrap, weight);
			      }
			      if (btag_category==1){
				h_cutflow_electron->fill(16+0.5, weight);
			      }
			      if (btag_category==2){
				h_cutflow_electron->fill(17+0.5, weight);
			      }
			      if (btag_category==3){
				h_cutflow_electron->fill(18+0.5, weight);
			      }
			    }//lepbtag || hadbtag
			  } // passLepJet
			} //passAddJet
		      } // bjets >= 1
		      }//passDphi
		    } // passTopTag
		  } //Large R Jet >=1
		}//MTW+MET
	      }//MET
	    //	    } // emu_overlao
	  } // ither leptons == 0
	} //lepton == 1
	} // leptons >= 1
	} // START

	icut = 0;
	/******************************
       *    MUON CHANNEL CUTS   *
       ******************************/
	{
	  CountEvent(h_cutflow_muon, icut, weight);
	{

        // 1. More than zero good muons
        if(good_muons.size() > 0) {
          CountEvent(h_cutflow_muon, icut, weight);
          // 2. No additional muons passing the veto selection
          if(good_muons.size() == 1) {
            CountEvent(h_cutflow_muon, icut, weight);
            // 3. No electrons passing the veto selection
            if(good_electrons.size() == 0) {
              CountEvent(h_cutflow_muon, icut, weight);

              // 4. Remove events with e-mu overlap
	      //              if(emu_overlap == false) {
		if(neutrino_sum.pt() > 20){
		  CountEvent(h_cutflow_muon, icut, weight);
		  // 5. MTW + total neutrino pT > 60GeV
		  double transmass = TransMass( good_muons.at(0)->momentum().pt(),
						good_muons.at(0)->momentum().phi(),
						neutrino_sum.pt(),
						neutrino_sum.phi()
						);
		  if(transmass+neutrino_sum.pt() > 60) {
		    CountEvent(h_cutflow_muon, icut, weight);
		    // 6. At least one good fatjet (pT > 300GeV && |eta| < 2.0) (passLJet)
		    if(selected_trimmed_good_fatJets.size() >= 1 ) {
		      CountEvent(h_cutflow_muon, icut, weight);
		      int fatJetIndex = -1;
		      double FJ_m_cut = 100.;
		      bool passTopTag = false;
		      unsigned int j=0;
		      for (;j<selected_trimmed_good_fatJets.size();j++)
			{
			  // 7. good fat jet passing top tagging cut (Nsubjettiness ratio, passTopTag)
			  double trimmed_FJ_tau32 = .75; // top-tagging variable
      			  if( tau32( selected_trimmed_good_fatJets.at(j), R_FJ ) < trimmed_FJ_tau32 &&
			      momentum(selected_trimmed_good_fatJets.at(j)).mass() > FJ_m_cut ) {
			    passTopTag = true;
			    fatJetIndex=j;
			  }
			}
		      if( passTopTag ) {
			CountEvent(h_cutflow_muon, icut, weight);
			// 8. dPhi between lepton and fatjet (passDphi)
			bool passDphi = false;
			double dPhi_fatjet = deltaPhi( good_muons.at(0)->momentum().phi(),
						       momentum(selected_trimmed_good_fatJets.at(fatJetIndex)).phi());
			double dPhi_fatjet_lep_cut  = 1.0; //2.3
			if( dPhi_fatjet > dPhi_fatjet_lep_cut ){
			  passDphi = true;
			}
			if( passDphi ) {
			  CountEvent(h_cutflow_muon, icut, weight);
			  // 9. At least one b-tagged jet (passbtag)
			  if(bjets.size() > 0) {
			    CountEvent(h_cutflow_muon, icut, weight);
			    bool lepbtag=false;
			    bool hadbtag=false;


			    // 10. dR between jet and fatjet (passAddJet)
			    bool passAddJet = false;
			    foreach(const Jet& j, good_jets)
			      {
				double dR = deltaR(j.momentum(), momentum(selected_trimmed_good_fatJets.at(fatJetIndex)));
				double dR_fatjet_jet_cut  = 1.5;
			      if(dR > dR_fatjet_jet_cut) {
				passAddJet = true;
				break;
			      }
			    }
			  if(passAddJet) {
			    CountEvent(h_cutflow_muon, icut, weight);
			    // 11. dR between jet and fatjet && dR between jet and lepton (passLepJet)
			    bool goodLepJet = false;
			    int lepJetIndex = -1;
			    int index = 0;

			    foreach(const Jet& j, good_jets)
			      {
				double dR_jet_lep = deltaR(j.momentum(), good_muons.at(0)->momentum());
				double dR_jet_fatjet = deltaR(j.momentum(), momentum(selected_trimmed_good_fatJets.at(fatJetIndex)));
				double dR_jet_lep_cut = 2.0;//1.5
				double dR_jet_fatjet_cut = 1.5;//1.5
				if(dR_jet_lep < dR_jet_lep_cut && dR_jet_fatjet > dR_jet_fatjet_cut) {
				  lepJetIndex = index;
				  goodLepJet = true;
				  break;
				}
				++index;
			      }


			    // if (lepJetIndex != -1){
			    //   dR_fatjet = deltaR(good_jets.at(lepJetIndex).momentum(), momentum(selected_trimmed_good_fatJets.at(fatJetIndex)));
			    //   double dR_fatjet_jet_cut  = 1.5;
			    //   if (dR_fatjet > dR_fatjet_jet_cut ){
			    // 	goodLepJet = true;
			    //   }
			    // }

			    if(goodLepJet) {
			      CountEvent(h_cutflow_muon, icut, weight);

			      foreach(Particle b, b_hadrons){
				if(good_jets.at(lepJetIndex).containsParticle(b)){
				  lepbtag = true;
				}
			      }

			      double dR_fatBjet_cut=1.0;
			      for (unsigned int i=0; i<bjets.size(); i++) {
				if (deltaR(momentum(selected_trimmed_good_fatJets.at(fatJetIndex)), bjets.at(i).momentum()) < dR_fatBjet_cut )
				  {
				    hadbtag=true;
				  }
			      }
			      // 12. dR between lep and bjet (goodjet at lepJetIndex is within dR < 2.0 of lepton) ||
			      // dR between fatJet and bjet (passBTagBatch)
			      if(lepbtag || hadbtag) CountEvent(h_cutflow_muon, icut, weight);

			      int btag_category=0;
			      if(hadbtag && lepbtag) btag_category=1;
			      if(hadbtag && !lepbtag) btag_category=2;
			      if(!hadbtag && lepbtag) btag_category=3;

			      if (btag_category==1||btag_category==2||btag_category==3){
				h_cutflow_muon->fill(15+0.5, weight);
				double hadtop_pt= momentum(selected_trimmed_good_fatJets.at(fatJetIndex)).pt();
				double hadtop_rap= momentum(selected_trimmed_good_fatJets.at(fatJetIndex)).eta();
				double hadtop_absrap= fabs(momentum(selected_trimmed_good_fatJets.at(fatJetIndex)).eta());

				h_hadtop_pt_muon->fill(hadtop_pt, weight);
				h_hadtop_diffXsec_pt_muon->fill(hadtop_pt, weight);

				h_hadtop_pt_sum->fill(hadtop_pt, weight);
				h_hadtop_diffXsec_pt_sum->fill(hadtop_pt, weight);

				h_hadtop_rap_muon->fill(hadtop_rap, weight);
				h_hadtop_diffXsec_rap_muon->fill(hadtop_rap, weight);

				h_hadtop_rap_sum->fill(hadtop_rap, weight);
				h_hadtop_diffXsec_rap_sum->fill(hadtop_rap, weight);

				h_hadtop_absrap_muon->fill(hadtop_absrap, weight);
				h_hadtop_diffXsec_absrap_muon->fill(hadtop_absrap, weight);

				h_hadtop_absrap_sum->fill(hadtop_absrap, weight);
				h_hadtop_diffXsec_absrap_sum->fill(hadtop_absrap, weight);
			      }
			      if (btag_category==1){
				h_cutflow_muon->fill(16+0.5, weight);
			      }
			      if (btag_category==2){
				h_cutflow_muon->fill(17+0.5, weight);
			      }
			      if (btag_category==3){
				h_cutflow_muon->fill(18+0.5, weight);
			      }
			    }//lepbtag || hadbtag
			  } // passLepJet
			} //passAddJet
		      } // bjets >= 1
		      }//passDphi
		    } // passTopTag
		  } //Large R Jet >=1
		}//MTW+MET
	      }//MET
	    //	    } // emu_overlao
	  } // ither leptons == 0
	} //lepton == 1
	} // leptons >= 1
	} // START

    }


    template <class type> void Print(type a) {
      MSG_INFO("object pt = " << a.momentum().pt() << "  eta = " << a.momentum().eta() << "  phi " << a.momentum().phi());
    }

    double TransMass(double ptLep, double phiLep, double met, double phiMet) {
      return std::sqrt(2.0*ptLep*met*( 1 - std::cos( phiLep-phiMet ) ) );
    }

    void AddGhostBhadrons(Particles* parts, Particles* bhad) {
      foreach(Particle p, *bhad) {
        double normfactor = 1.0E-3/p.E();
        p.setMomentum(FourMomentum(p.E()*normfactor, p.momentum().px()*normfactor, p.momentum().py()*normfactor, p.momentum().pz()*normfactor));
        parts->push_back(p);
      }
    }



 double tau3(const fastjet::PseudoJet &jet, double jet_rad) const{
   double alpha = 1.0;
    fastjet::contrib::KT_Axes kt_axes;
    fastjet::contrib::NormalizedCutoffMeasure normalized_measure(alpha, jet_rad, 1000000);
     fastjet::contrib::Nsubjettiness tau3_kt(3, kt_axes, normalized_measure);
     double tau3 = tau3_kt.result(jet);
     return tau3;
 }
   double tau2(const fastjet::PseudoJet &jet, double jet_rad) const{
     double alpha = 1.0;
     fastjet::contrib::KT_Axes kt_axes;
     fastjet::contrib::NormalizedCutoffMeasure normalized_measure(alpha, jet_rad, 1000000);
     fastjet::contrib::Nsubjettiness tau2_kt(2, kt_axes, normalized_measure);
     double tau2 = tau2_kt.result(jet);
     return tau2;
   }

 double tau32(const fastjet::PseudoJet &jet, double jet_rad) const{

      double alpha = 1.0;
      //fastjet::contrib::UnnormalizedMeasure measureSpec1(beta1);
      fastjet::contrib::NormalizedCutoffMeasure normalized_measure(alpha, jet_rad, 1000000);
      // WTA definition
      //      fastjet::contrib::OnePass_WTA_KT_Axes wta_kt_axes;
      // as in official JetSubStructure recommendations (http://acode-browser.usatlas.bnl.gov/lxr/source/atlas/Reconstruction/Jet/JetSubStructureMomentTools/Root/NSubjettinessTool.cxx)
      fastjet::contrib::KT_Axes kt_axes;

      /// NsubjettinessRatio uses the results from Nsubjettiness to calculate the ratio
      /// tau_N/tau_M, where N and M are specified by the user. The ratio of different tau values
      /// is often used in analyses, so this class is helpful to streamline code.
      fastjet::contrib::NsubjettinessRatio tau32_kt( 3, 2,  kt_axes, normalized_measure );

      double tau32 = tau32_kt.result(jet);
      //std::cout << "DEBUG :: nsubjettiness output > " << tau32 << std::endl;

      return tau32;

    }

    double tau32wta(const fastjet::PseudoJet &jet, double jet_rad) const{

      double alpha = 1.0;
      //fastjet::contrib::UnnormalizedMeasure measureSpec1(beta1);
      fastjet::contrib::NormalizedCutoffMeasure normalized_measure(alpha, jet_rad, 1000000);
      // WTA definition
      //      fastjet::contrib::OnePass_WTA_KT_Axes wta_kt_axes;
      // as in official JetSubStructure recommendations (http://acode-browser.usatlas.bnl.gov/lxr/source/atlas/Reconstruction/Jet/JetSubStructureMomentTools/Root/NSubjettinessTool.cxx)
      fastjet::contrib::WTA_KT_Axes wta_kt_axes;

      /// NsubjettinessRatio uses the results from Nsubjettiness to calculate the ratio                                                                          /// tau_N/tau_M, where N and M are specified by the user. The ratio of different tau values                                                                /// is often used in analyses, so this class is helpful to streamline code.
      fastjet::contrib::NsubjettinessRatio tau32_WTA( 3, 2,  wta_kt_axes, normalized_measure );

      double tau32_wta = tau32_WTA.result(jet);
      //std::cout << "DEBUG :: nsubjettiness output > " << tau32 << std::endl;

      return tau32_wta;

    }


    Particles RemoveFromFinalState(const Particles& fs, vector<const GenParticle*> vetoed_particles) {
      Particles vetoed_final_state;
      foreach(const Particle& p, fs) {
        bool keep = true;
        foreach(const GenParticle* g, vetoed_particles) {
          // Loop over each final state particle.
          // If it's also in the vector of vetoed particles, we don't want to keep it.
          if(p.genParticle() == g) keep = false;
        }
        if(keep) vetoed_final_state.push_back(Particle(p));
      }

      return vetoed_final_state;
    }

    Particles* GetChargedParticles(Particles& parts) {
      Particles* charged = new Particles();
      foreach(Particle& p, parts) {
        if(p.charge() != 0) {
          charged->push_back(p);
        }
      }
      return charged;
    }

    void CountEvent(Histo1DPtr h, unsigned int& icut, double weight) {
      h->fill(icut+0.5, weight);
      ++icut;
    }




    /// Normalise histograms etc., after the run
    void finalize() {
	double cross_section=crossSection();
	double normfac = cross_section/sumOfWeights();

	scale(h_hadtop_diffXsec_pt_electron, normfac);
	scale(h_hadtop_diffXsec_pt_muon, normfac);
	scale(h_hadtop_diffXsec_pt_sum, normfac);

	scale(h_hadtop_diffXsec_rap_electron, normfac);
	scale(h_hadtop_diffXsec_rap_muon, normfac);
	scale(h_hadtop_diffXsec_rap_sum, normfac);

	scale(h_hadtop_diffXsec_absrap_electron, normfac);
	scale(h_hadtop_diffXsec_absrap_muon, normfac);
	scale(h_hadtop_diffXsec_absrap_sum, normfac);


	//	scale(_h_ttbar_M,   normfac);
	//	scale(_h_ttbar_rap,   normfac);
	//	scale(_h_ttbar_absrap,   normfac);
	//	scale(_h_ttbar_phi,   normfac);
    }



  private:
    Histo1DPtr h_cutflow_muon;
    Histo1DPtr h_cutflow_electron;

    Histo1DPtr h_hadtop_pt_electron;
    Histo1DPtr h_hadtop_diffXsec_pt_electron;

    Histo1DPtr h_hadtop_pt_muon;
    Histo1DPtr h_hadtop_diffXsec_pt_muon;

    Histo1DPtr h_hadtop_pt_sum;
    Histo1DPtr h_hadtop_diffXsec_pt_sum;

    Histo1DPtr h_hadtop_rap_electron;
    Histo1DPtr h_hadtop_diffXsec_rap_electron;

    Histo1DPtr h_hadtop_rap_muon;
    Histo1DPtr h_hadtop_diffXsec_rap_muon;

    Histo1DPtr h_hadtop_rap_sum;
    Histo1DPtr h_hadtop_diffXsec_rap_sum;



     Histo1DPtr h_hadtop_absrap_electron;
    Histo1DPtr h_hadtop_diffXsec_absrap_electron;

    Histo1DPtr h_hadtop_absrap_muon;
    Histo1DPtr h_hadtop_diffXsec_absrap_muon;

    Histo1DPtr h_hadtop_absrap_sum;
    Histo1DPtr h_hadtop_diffXsec_absrap_sum;

    Histo1DPtr _h_ttbar_M;
    Histo1DPtr _h_ttbar_rap;
    Histo1DPtr _h_ttbar_absrap;
    Histo1DPtr _h_ttbar_phi;

  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(BoostedDiffXsec);

}
