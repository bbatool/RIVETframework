# BEGIN PLOT /BoostedDiffXsec/h_cutflow_electron
Title="Electron Channel Cutflow"
XLabel=Cutsd#sigma_{t#bar{t}}/dp_{T}^{t_{had}} [pb GeV^{-1}]
YLabel= Entries
# END PLOT

# BEGIN PLOT /BoostedDiffXsec/h_cutflow_muon
Title="Electron Channel Cutflow"
XLabel=Cuts
YLabel= Entries
# END PLOT

# BEGIN PLOT /BoostedDiffXsec/h_hadtop_pt_electron
Title="Hadronic Top $p_{T}$ distribution in electron channel"
XLabel=$p_{T} \rm [GeV/c]$
YLabel= Entries
LegendXPos=0.5
LegendYPos=1.0
# END PLOT

# BEGIN PLOT /BoostedDiffXsec/h_hadtop_pt_muon
Title="Hadronic Top $p_T$ distribution in muon channel"
XLabel=$p_{T} \rm \rm [GeV/c] $
YLabel= Entries
LegendXPos=0.5
LegendYPos=1.0
# END PLOT

# BEGIN PLOT /BoostedDiffXsec/h_hadtop_diffXsec_pt_electron
Title="$t\bar{t}$ differential cross section with respect to the hadronic top $p_T$ in electron channel"
XLabel=$p_{T} \rm [GeV/c] $
YLabel= $ d\sigma_{t\bar{t}}/dp_{T}^{t_{\rm had}}\rm [pb \ GeV^{-1}]$
LegendXPos=0.5
LegendYPos=1.0
# END PLOT

# BEGIN PLOT /BoostedDiffXsec/h_hadtop_diffXsec_pt_muon
Title="$t\bar{t}$ differential cross section with respect to the hadronic top $p_T$ in muon channel"
XLabel=$p_{T} \rm [GeV/c] $
YLabel= $ d\sigma_{t\bar{t}}/dp_{T}^{t_{\rm had}}\rm [pb \ GeV^{-1}]$
LegendXPos=0.5
LegendYPos=1.0
# END PLOT

# BEGIN PLOT /BoostedDiffXsec/h_hadtop_pt_sum
Title="Hadronic Top $p_{T}$ distribution in combined channel"
XLabel=$p_{T} \rm [GeV/c] $
YLabel= Entries
LegendXPos=0.5
LegendYPos=1.0
# END PLOT

# BEGIN PLOT /BoostedDiffXsec/h_hadtop_diffXsec_pt_sum
Title="$t\bar{t}$ differential cross section with respect to the hadronic top $p_{T}$ in combined channel"
XLabel=$p_{T} \rm [GeV/c] $
YLabel= $ d\sigma_{t\bar{t}}/dp_{T}^{t_{\rm had}}\rm [pb \ GeV^{-1}]$
LegendXPos=0.5
LegendYPos=1.0
RatioPlotYMin=0.
RatioPlotYMax=2.
RatioPlotErrorBandColor=lightgray
# END PLOT

# BEGIN PLOT /BoostedDiffXsec_closureTest/h_hadtop_diffXsec_pt_sum
Path=/BoostedDiffXsec_closureTest/h_hadtop_diffXsec_pt_sum
Title="$t\bar{t}$ differential cross section with respect to the hadronic top $p_{T}$ in combined channel"
XLabel=$p_{T} \rm [GeV/c] $
YLabel= $ d\sigma_{t\bar{t}}/dp_{T}^{t_{\rm had}}\rm [pb \ GeV^{-1}]$
LegendXPos=0.5
LegendYPos=1.0
RatioPlotYMin=.6
RatioPlotYMax=1.4
RatioPlotErrorBandColor=lightgray
# END PLOT

# BEGIN PLOT /BoostedDiffXsec/h_hadtop_rap_muon
Title="Hadronic Top $y$ distribution in muon channel"
XLabel=$y_{t^{\rm had}} $
YLabel= Entries
# END PLOT

# BEGIN PLOT /BoostedDiffXsec/h_hadtop_diffXsec_rap_electron
Title="$t\bar{t}$ differential cross section with respect to the hadronic top $y$ in electron channel"
XLabel=$y_{t^{\rm had}}$
YLabel= $ d\sigma/dy_{t^{\rm had}}$
# END PLOT

# BEGIN PLOT /BoostedDiffXsec/h_hadtop_diffXsec_rap_muon
Title="$t\bar{t}$ differential cross section with respect to the hadronic top $y$ in muon channel"
XLabel=$y_{t^{\rm had}} $
YLabel= $ d\sigma/dy_{t^{\rm had}}$
# END PLOT

# BEGIN PLOT /BoostedDiffXsec/h_hadtop_rap_sum
Title="Hadronic Top $y$ distribution in combined channel"
XLabel=$y_{t^{\rm had}}$
YLabel= Entries
# END PLOT

# BEGIN PLOT /BoostedDiffXsec/h_hadtop_diffXsec_rap_sum
Title="$t\bar{t}$ differential cross section with respect to the hadronic top $y$ in combined channel"
XLabel=$y_{t^{\rm had}}$
YLabel= $ d\sigma/dy_{t^{\rm had}}$
# END PLOT



# BEGIN PLOT /BoostedDiffXsec/h_hadtop_absrap_muon
Title="Hadronic Top $|y|$ distribution in muon channel"
XLabel=$|y_{t^{\rm had}}|$
YLabel= Entries
# END PLOT

# BEGIN PLOT /BoostedDiffXsec/h_hadtop_diffXsec_absrap_electron
Title="$t\bar{t}$ differential cross section with respect to the hadronic top $|y|$ in electron channel"
XLabel=$|y_{t^{\rm had}}|$
YLabel= $ d\sigma/d|y_{t^{\rm had}}|$
# END PLOT

# BEGIN PLOT /BoostedDiffXsec/h_hadtop_diffXsec_absrap_muon
Title="$t\bar{t}$ differential cross section with respect to the hadronic top $|y|$ in muon channel"
XLabel=$|y_{t^{\rm had}}|$
YLabel= $ d\sigma/d|y_{t^{\rm had}}|$
# END PLOT

# BEGIN PLOT /BoostedDiffXsec/h_hadtop_absrap_sum
Title="Hadronic Top $|y|$ distribution in combined channel"
XLabel=$|y_{t^{\rm had}}|$
YLabel= Entries
# END PLOT

# BEGIN PLOT /BoostedDiffXsec/h_hadtop_diffXsec_absrap_sum
Title="$t\bar{t}$ differential cross section with respect to the hadronic top $|y|$ in combined channel"
XLabel=$|y_{t^{\rm had}}|$
YLabel= $ d\sigma/d|y_{t^{\rm had}}|$
# END PLOT
