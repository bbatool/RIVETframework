// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/AnalysisLoader.hh"

namespace Rivet {

  using namespace Cuts;

  /// @brief ATLAS 7 TeV pseudo-top analysis
  ///
  /// @author K .Finelli <kevin.finelli@cern.ch>
  /// @author A. Saavedra <a.saavedra@physics.usyd.edu.au>
  /// @author L. Lan <llan@physics.usyd.edu.au>
  class ATLAS_2015_I1345452 : public Analysis {
    public:

      /// Constructor
      ATLAS_2015_I1345452()
        : Analysis("ATLAS_2015_I1345452")
      {    }


      void init() {
        // Eta ranges
        Cut eta_full = etaIn(-5.0, 5.0) & (pT >= 1.0*MeV);
        Cut eta_lep = etaIn(-2.5, 2.5);

        // All final state particles
        FinalState fs(eta_full);

        // Get photons to dress leptons
        IdentifiedFinalState photons(fs);
        photons.acceptIdPair(PID::PHOTON);

        // Projection to find the electrons
        IdentifiedFinalState el_id(fs);
        el_id.acceptIdPair(PID::ELECTRON);
        PromptFinalState electrons(el_id);
        electrons.acceptTauDecays(true);
        addProjection(electrons, "electrons");
        DressedLeptons dressedelectrons(photons, electrons, 0.1, true, eta_lep & (pT >= 25.0*GeV), true);
        addProjection(dressedelectrons, "dressedelectrons");
        DressedLeptons ewdressedelectrons(photons, electrons, 0.1, true, eta_full, true);
        addProjection(ewdressedelectrons, "ewdressedelectrons");
        DressedLeptons vetodressedelectrons(photons, electrons, 0.1, true, eta_lep & (pT >= 15.0*GeV), true);
        addProjection(vetodressedelectrons, "vetodressedelectrons");

        // Projection to find the muons
        IdentifiedFinalState mu_id(fs);
        mu_id.acceptIdPair(PID::MUON);
        PromptFinalState muons(mu_id);
        muons.acceptTauDecays(true);
        addProjection(muons, "muons");
        vector<pair<double, double> > eta_muon;
        DressedLeptons dressedmuons(photons, muons, 0.1, true, eta_lep & (pT >= 25.0*GeV), true);
        addProjection(dressedmuons, "dressedmuons");
        DressedLeptons ewdressedmuons(photons, muons, 0.1, true, eta_full, true);
        addProjection(ewdressedmuons, "ewdressedmuons");
        DressedLeptons vetodressedmuons(photons, muons, 0.1, true, eta_lep & (pT >= 15.0*GeV), true);
        addProjection(vetodressedmuons, "vetodressedmuons");

        // Projection to find neutrinos and produce MET
        IdentifiedFinalState nu_id;
        nu_id.acceptNeutrinos();
        PromptFinalState neutrinos(nu_id);
        neutrinos.acceptTauDecays(true);
        addProjection(neutrinos, "neutrinos");

        // Jet clustering.
        VetoedFinalState vfs;
        vfs.addVetoOnThisFinalState(ewdressedelectrons);
        vfs.addVetoOnThisFinalState(ewdressedmuons);
        vfs.addVetoOnThisFinalState(neutrinos);
        FastJets jets(vfs, FastJets::ANTIKT, 0.4);
        jets.useInvisibles();
        addProjection(jets, "jets");

        //Make variable bins for each type of data needed
        //***TODO: get these from plaintext data, not source code
        std::vector<double> bins_pT_pseudo, bins_pT_tt, bins_absrap, bins_absrap_tt, bins_mass;
	bins_pT_pseudo.push_back(0);
	bins_pT_pseudo.push_back(45);
	bins_pT_pseudo.push_back(90);
	bins_pT_pseudo.push_back(120);
	bins_pT_pseudo.push_back(150);
	bins_pT_pseudo.push_back(180);
	bins_pT_pseudo.push_back(220);
	bins_pT_pseudo.push_back(260);
	bins_pT_pseudo.push_back(300);
	bins_pT_pseudo.push_back(350);
	bins_pT_pseudo.push_back(800);
	bins_pT_tt.push_back(0);
	bins_pT_tt.push_back(25);
	bins_pT_tt.push_back(50);
	bins_pT_tt.push_back(85);
	bins_pT_tt.push_back(125);
	bins_pT_tt.push_back(500);
	bins_absrap.push_back(0.0);
	bins_absrap.push_back(.4);
	bins_absrap.push_back(.8);
	bins_absrap.push_back(1.1);
	bins_absrap.push_back(1.4);
	bins_absrap.push_back(1.8);
	bins_absrap.push_back(2.5);
	bins_absrap_tt.push_back(0.0);
	bins_absrap_tt.push_back(0.1);
	bins_absrap_tt.push_back(0.25);
	bins_absrap_tt.push_back(0.4);
	bins_absrap_tt.push_back(0.55);
	bins_absrap_tt.push_back(0.7);
	bins_absrap_tt.push_back(0.85);
	bins_absrap_tt.push_back(1.0);
	bins_absrap_tt.push_back(1.2);
	bins_absrap_tt.push_back(1.6);
	bins_absrap_tt.push_back(2.5);
	bins_mass.push_back(225);
	bins_mass.push_back(400);
	bins_mass.push_back(500);
	bins_mass.push_back(600);
	bins_mass.push_back(700);
	bins_mass.push_back(850);
	bins_mass.push_back(1000);
	bins_mass.push_back(1250);
	bins_mass.push_back(2500);
        //std::vector<double> bins_pT_pseudo = {0, 45, 90, 120, 150, 180, 220, 260, 300, 350, 800};
	//        std::vector<double> bins_pT_tt = {0, 25, 50, 85, 125, 500};
	//        std::vector<double> bins_absrap = {0.0, 0.4, 0.8, 1.1, 1.4, 1.8, 2.5};
	//        std::vector<double> bins_absrap_tt = {0.0, 0.1, 0.25, 0.4, 0.55, 0.7, 0.85, 1.0, 1.2, 1.6, 2.5};
	//        std::vector<double> bins_mass = {225, 400, 500, 600, 700, 850, 1000, 1250, 2500};

        //pseudotop leptons and hadrons
        _h_ptpseudotophadron_mu = bookHisto1D(1, 1, 2);
        _h_ptpseudotophadron_el = bookHisto1D(2, 1, 2);
        _h_absrappseudotophadron_mu = bookHisto1D(3, 1, 2);
        _h_absrappseudotophadron_el = bookHisto1D(4, 1, 2);
        _h_ptpseudotoplepton_mu = bookHisto1D(5, 1, 2);
        _h_ptpseudotoplepton_el = bookHisto1D(6, 1, 2);
        _h_absrappseudotoplepton_mu = bookHisto1D(7, 1, 2);
        _h_absrappseudotoplepton_el = bookHisto1D(8, 1, 2);
        _h_ptttbar_mu = bookHisto1D(9, 1, 2);
        _h_ptttbar_el = bookHisto1D(10, 1, 2);
        _h_absrapttbar_mu= bookHisto1D(11, 1, 2);
        _h_absrapttbar_el = bookHisto1D(12, 1, 2);
        _h_ttbarmass_mu = bookHisto1D(13, 1, 2);
        _h_ttbarmass_el = bookHisto1D(14, 1, 2);

        _h_ptpseudotophadron = bookHisto1D(15, 1, 2);
        _h_absrappseudotophadron = bookHisto1D(16, 1, 2);
        _h_ptpseudotoplepton = bookHisto1D(17, 1, 2);
        _h_absrappseudotoplepton = bookHisto1D(18, 1, 2);
        _h_ptttbar = bookHisto1D(19, 1, 2);
        _h_absrapttbar = bookHisto1D(20, 1, 2);
        _h_ttbarmass = bookHisto1D(21, 1, 2);

        //pseudotop with electron channel


        //pseudotop with muon channel


        //***diagnostic plots****
        //dR values
        _h_dR_jet_el = bookHisto1D("dR_values_jets_el", 10, 0.0, 1.0);
        _h_dR_jet_mu = bookHisto1D("dR_values_jets_mu", 10, 0.0, 1.0);

        _h_nbjet = bookHisto1D("nbjets", 7, -0.5, 6.5);
        _h_njet = bookHisto1D("njets", 12, -0.5, 11.5);

        _h_metet = bookHisto1D("E_T^Miss neutrino pT", 100, 0.0, 250);

        _h_elecpt = bookHisto1D("elecpt", 100, 0.0, 500);
        _h_eleceta = bookHisto1D("eleceta", 50, -2.8, 2.8);
        _h_muonpt = bookHisto1D("muonpt", 100, 0.0, 500);
        _h_muoneta = bookHisto1D("muoneta", 50, -2.8, 2.8);

        _h_jet1pt = bookHisto1D("jet1pt", 100, 0.0, 500);
        _h_jet1eta = bookHisto1D("jet1eta", 50, -2.8, 2.8);
        _h_jet2pt = bookHisto1D("jet2pt", 100, 0.0, 500);
        _h_jet2eta = bookHisto1D("jet2eta", 50, -2.8, 2.8);

        _h_bjet1pt = bookHisto1D("bjet1pt", 13, 0.0, 400);
        _h_bjet1eta = bookHisto1D("bjet1eta", 50, -2.8, 2.8);
        _h_bjet2pt = bookHisto1D("bjet2pt", 13, 0.0, 400);
        _h_bjet2eta = bookHisto1D("bjet2eta", 50, -2.8, 2.8);


        _h_nneutrino = bookHisto1D("nneutrino", 7, 0.5, 7.5);

        //***Plots for comparison with Aldo's truth files***//
        //electron channel
        _h_lpt_el = bookHisto1D("lpt_el", 100, 0.0, 500);
        _h_leta_el = bookHisto1D("leta_el", 50, -2.8, 2.8);
        _h_lphi_el = bookHisto1D("lphi_el", 100, -3.2, 3.2);
        _h_j1pt_el = bookHisto1D("j1pt_el", 100, 0.0, 500);
        _h_j1eta_el = bookHisto1D("j1eta_el", 50, -2.8, 2.8);
        _h_j1phi_el = bookHisto1D("j1phi_el", 100, -3.2, 3.2);
        _h_tty_el = bookHisto1D("tty_el", 100, -2.5, 2.5);
        _h_pttt_el = bookHisto1D("pttt_el", 500, 0.0, 400);
        _h_mtt_el = bookHisto1D("mtt_el", 500, 0.0, 2500);
        _h_tlpt_el = bookHisto1D("tlpt_el", 100, 0.0, 450);
        _h_tly_el = bookHisto1D("tly_el", 100, -2.5, 2.5);
        _h_tlm_el = bookHisto1D("tlm_el", 50, 0.0, 600);
        _h_thpt_el = bookHisto1D("thpt_el", 100, 0.0, 450);
        _h_thy_el = bookHisto1D("thy_el", 100, -2.5, 2.5);
        _h_thm_el = bookHisto1D("thm_el", 50, 0.0, 600);
        _h_met_el = bookHisto1D("met_el", 100, 0.0, 250);
        _h_met_pT_el = bookHisto1D("met_pT_el", 100, 0.0, 250);

        //muon channel
        _h_lpt_mu = bookHisto1D("lpt_mu", 100, 0.0, 500);
        _h_leta_mu = bookHisto1D("leta_mu", 50, -2.8, 2.8);
        _h_lphi_mu = bookHisto1D("lphi_mu", 100, -3.2, 3.2);
        _h_j1pt_mu = bookHisto1D("j1pt_mu", 100, 0.0, 500);
        _h_j1eta_mu = bookHisto1D("j1eta_mu", 50, -2.8, 2.8);
        _h_j1phi_mu = bookHisto1D("j1phi_mu", 100, -3.2, 3.2);
        _h_tty_mu = bookHisto1D("tty_mu", 100, -2.5, 2.5);
        _h_pttt_mu = bookHisto1D("pttt_mu", 500, 0.0, 400);
        _h_mtt_mu = bookHisto1D("mtt_mu", 500, 0.0, 2500);
        _h_tlpt_mu = bookHisto1D("tlpt_mu", 100, 0.0, 450);
        _h_tly_mu = bookHisto1D("tly_mu", 100, -2.5, 2.5);
        _h_tlm_mu = bookHisto1D("tlm_mu", 50, 0.0, 600);
        _h_thpt_mu = bookHisto1D("thpt_mu", 100, 0.0, 450);
        _h_thy_mu = bookHisto1D("thy_mu", 100, -2.5, 2.5);
        _h_thm_mu = bookHisto1D("thm_mu", 50, 0.0, 600);
        _h_met_mu = bookHisto1D("met_mu", 100, 0.0, 250);
        _h_met_pT_mu = bookHisto1D("met_pT_mu", 100, 0.0, 250);
      }

      void analyze(const Event& event) {

        // Get the selected objects, using the projections.
        _dressedelectrons = sortByPt(applyProjection<DressedLeptons>(event, "dressedelectrons").dressedLeptons());
        _vetodressedelectrons = applyProjection<DressedLeptons>(event, "vetodressedelectrons").dressedLeptons();
        _dressedmuons = sortByPt(applyProjection<DressedLeptons>(event, "dressedmuons").dressedLeptons());
        _vetodressedmuons = applyProjection<DressedLeptons>(event, "vetodressedmuons").dressedLeptons();
        _neutrinos = applyProjection<PromptFinalState>(event, "neutrinos").particlesByPt();
        _jets = sortByPt(applyProjection<FastJets>(event, "jets").jetsByPt(25.0, MAXDOUBLE, -2.5, 2.5));

        //get true l+jets events by removing events with more than 1 electron||muon neutrino
        unsigned int n_elmu_neutrinos = 0;
        foreach (const Particle p, _neutrinos) {
          if (abs(p.genParticle()->pdg_id()) == 12 || abs(p.genParticle()->pdg_id() ) == 14) n_elmu_neutrinos++;
        }

        if (n_elmu_neutrinos != 1) vetoEvent;

        DressedLepton *lepton;
        if (_dressedelectrons.size() != 0) {
          lepton = &_dressedelectrons[0];
        }
        else if (_dressedmuons.size() != 0){
          lepton =  &_dressedmuons[0];
        }
        else vetoEvent;

        // Calculate the missing ET, using the prompt neutrinos only (really?)
        /// @todo Why not use MissingMomentum?
        FourMomentum pmet;
        foreach (const Particle& p, _neutrinos) pmet += p.momentum();
        _met_et = pmet.pT();
        _met_phi = pmet.phi();

        //Momentum of lepton
        FourMomentum plepton = lepton->momentum();

        _bjets.clear(); //clears each list of b jets when analysing each event
        _lightjets.clear(); //clears each list of non b jets when analysing each event

        //Calculate dR
        double _minimum_dR_el = 0.999;
        double _minimum_dR_mu = 0.999;

        // Check overlap of jets/leptons.
        unsigned int i,j;
        FourMomentum pbjet1; //Momentum of bjet1
        FourMomentum pbjet2; //Momentum of bjet2
        _jet_ntag = 0;
        _overlap = false;

        std::vector<unsigned int> _overlap_jets;


        //remove jets if they are within dR<0.2 of lepton
        for (i = 0; i < _jets.size(); i++) {
          double _minimum_dR_jet_lep = 0.999;
          //looping over all jets
          const Jet& jet = _jets[i];
          //looping over all veto electrons
          for (unsigned int j = 0; j < _vetodressedelectrons.size(); j++) {
            const FourMomentum& electron = _vetodressedelectrons.at(j);
            //remove jet if dR < 0.2
            if (deltaR(jet, electron) < _minimum_dR_jet_lep) {
              _minimum_dR_jet_lep = deltaR(jet, electron);
            }
          }
          if (_minimum_dR_jet_lep < 0.2) _overlap_jets.push_back(i);
        }

        remove_jet(_overlap_jets, _jets);


        for (i = 0; i < _jets.size(); i++) {
          const Jet& jet = _jets[i];
          // If dR(el,jet) < 0.4 skip the event
          foreach (const DressedLepton& el, _dressedelectrons) {
            if (deltaR(jet, el) < 0.4) _overlap = true;
            if (deltaR(jet, el) < _minimum_dR_el) _minimum_dR_el = deltaR(jet, el); //270115
          }
          // If dR(mu,jet) < 0.4 skip the event
          foreach (const DressedLepton& mu, _dressedmuons) {
            if (deltaR(jet, mu) < 0.4) _overlap = true;
            if (deltaR(jet, mu) < _minimum_dR_mu) _minimum_dR_mu = deltaR(jet, mu); //270115
          }
          // If dR(jet,jet) < 0.5 skip the event
          for (j = 0; j < _jets.size(); j++) {
            const Jet& jet2 = _jets[j];
            if (i == j) continue; // skip the diagonal
            if (deltaR(jet, jet2) < 0.5) _overlap = true;
          }
          // Count the number of b-tags
          if (!jet.bTags().empty()){
            _jet_ntag += 1 ;
            _bjets.push_back(jet); //if b-jet is found, add to this vector
          }
          else _lightjets.push_back(jet); //if a non b-jet or light jet is found, add to this vector
        }

        _bjetsdR.clear();

        if (_bjets.size() < 2) vetoEvent;
        if (_lightjets.size() < 2) vetoEvent;

        for (i = 0; i < 2; i++) {
          const Jet& bjet = _bjets[i];
          _bjetsdR.push_back(deltaR(bjet.momentum(), lepton->momentum()));
        }


        if (_bjetsdR.at(0) <= _bjetsdR.at(1) ) {
          const Jet& bjet1 = _bjets.at(0);
          pbjet1 = bjet1.momentum();
          const Jet& bjet2 = _bjets.at(1);
          pbjet2 = bjet2.momentum();
        }
        else { //(_bjetsdR.at(0)>  _bjetsdR.at(1) )
          const Jet& bjet1 = _bjets.at(1);
          pbjet1 = bjet1.momentum();
          const Jet& bjet2 = _bjets.at(0);
          pbjet2 = bjet2.momentum();
        }

        FourMomentum pjet1; // Momentum of jet1
        if (_lightjets.size()>0){
          pjet1 = _lightjets[0].momentum();
        }

        FourMomentum pjet2; // Momentum of jet 2
        if (_lightjets.size()>1){
          pjet2 = _lightjets[1].momentum();
        }

        //	std::cout << _bjetsdR.at(1) << " " << pbjet1.pT() << " " << pbjet2.pT() << std::endl;
        //	std::cout << _bjets.size() << std::endl;

        FourMomentum ppseudoneutrino; //4-Momentum of pseudo  neutrino to compute that of the psuedotops 070115
        ppseudoneutrino.setPx (pmet.px() );
        ppseudoneutrino.setPy (pmet.py() );
        ppseudoneutrino.setPz (computeneutrinoz(plepton, pmet));
        ppseudoneutrino.setE ( sqrt( sqr((ppseudoneutrino.px())) + sqr((ppseudoneutrino.py())) + sqr((ppseudoneutrino.pz()))) ); //assume that the
        // neutrino has zero mass, therefore equate the energy to its 3 momentum

        //compute leptonic, hadronic, combined pseudo-top
        FourMomentum ppseudotoplepton = computepseudotop(plepton, ppseudoneutrino, pbjet1);
        FourMomentum ppseudotophadron = computepseudotop(pbjet2, pjet1, pjet2);
        FourMomentum pttbar = ppseudotoplepton + ppseudotophadron;

        // Evaluate basic event selection
        unsigned int ejets_bits = 0, mujets_bits = 0;
        bool pass_ejets = _ejets(ejets_bits);
        bool pass_mujets = _mujets(mujets_bits);

        //fill diagnostic plots of jet-lep dR
        _h_dR_jet_el->fill(_minimum_dR_el); //jet electron dR
        _h_dR_jet_mu->fill(_minimum_dR_mu); //jet mu dR

        // Remove events with object overlap
        if (_overlap) vetoEvent;
        // basic event selection requirements
        if (!pass_ejets && !pass_mujets) vetoEvent;

        // Fill histograms
        const double weight = event.weight();

        //diagnostic hists fill
        _h_nbjet->fill(_jet_ntag, weight);
        _h_njet->fill(_jets.size(), weight);
        _h_metet->fill(_met_et , weight);
        if (_dressedelectrons.size() != 0){
          _h_elecpt->fill(lepton->pt(), weight);
          _h_eleceta->fill(lepton->eta(), weight);
        } else {
          _h_muonpt->fill(lepton->pt(), weight);
          _h_muoneta->fill(lepton->eta(), weight);
        }
        _h_nneutrino->fill(_neutrinos.size(),weight);

        _h_jet1pt->fill(pjet1.pt(), weight);
        _h_jet1eta->fill(pjet1.eta(), weight);
        _h_jet2pt->fill(pjet2.pt(), weight);
        _h_jet2eta->fill(pjet2.eta(), weight);
        _h_bjet1pt->fill(pbjet1.pt(), weight);
        _h_bjet1eta->fill(pbjet1.eta(), weight);
        _h_bjet2pt->fill(pbjet2.pt(), weight);
        _h_bjet2eta->fill(pbjet2.eta(), weight);

        //truth hists fill
        if (_dressedelectrons.size() != 0) {
          _h_lpt_el->fill(lepton->pt(), weight);
          _h_leta_el->fill(lepton->eta(), weight);
          _h_lphi_el->fill(lepton->phi(MINUSPI_PLUSPI), weight);
          _h_j1pt_el->fill(pjet1.pt(), weight);
          _h_j1eta_el->fill(pjet1.eta(), weight);
          _h_j1phi_el->fill(pjet1.phi(MINUSPI_PLUSPI), weight);
          _h_tty_el->fill(pttbar.rapidity(), weight);
          _h_pttt_el->fill(pttbar.pt(), weight);
          _h_mtt_el->fill(pttbar.mass(),weight);
          _h_tlpt_el->fill(ppseudotoplepton.pt(), weight);
          _h_tly_el->fill(ppseudotoplepton.rapidity(), weight);
          _h_tlm_el->fill(ppseudotoplepton.mass(), weight);
          _h_thpt_el->fill(ppseudotophadron.pt(), weight);
          _h_thy_el->fill(ppseudotophadron.rapidity(), weight);
          _h_thm_el->fill(ppseudotophadron.mass(), weight);
          _h_met_el->fill(pmet.Et(), weight);
          _h_met_pT_el->fill(pmet.pT(), weight);
        }
        else {
          _h_lpt_mu->fill(lepton->pt(), weight);
          _h_leta_mu->fill(lepton->eta(), weight);
          _h_lphi_mu->fill(lepton->phi(MINUSPI_PLUSPI), weight);
          _h_j1pt_mu->fill(pjet1.pt(), weight);
          _h_j1eta_mu->fill(pjet1.eta(), weight);
          _h_j1phi_mu->fill(pjet1.phi(MINUSPI_PLUSPI), weight);
          _h_tty_mu->fill(pttbar.rapidity(), weight);
          _h_pttt_mu->fill(pttbar.pt(), weight);
          _h_mtt_mu->fill(pttbar.mass(),weight);
          _h_tlpt_mu->fill(ppseudotoplepton.pt(), weight);
          _h_tly_mu->fill(ppseudotoplepton.rapidity(), weight);
          _h_tlm_mu->fill(ppseudotoplepton.mass(), weight);
          _h_thpt_mu->fill(ppseudotophadron.pt(), weight);
          _h_thy_mu->fill(ppseudotophadron.rapidity(), weight);
          _h_thm_mu->fill(ppseudotophadron.mass(), weight);
          _h_met_mu->fill(pmet.Et(), weight);
          _h_met_pT_mu->fill(pmet.pT(), weight);
        }

        //pseudotop hadrons and leptons fill histogram
        _h_ptpseudotoplepton->fill(ppseudotoplepton.pt(), weight); //pT of pseudo top lepton
        _h_absrappseudotoplepton->fill(ppseudotoplepton.absrap(), weight); //absolute rapidity of pseudo top lepton
        _h_ptpseudotophadron->fill(ppseudotophadron.pt(), weight); //pT of pseudo top hadron
        _h_absrappseudotophadron->fill(ppseudotophadron.absrap(), weight); //absolute rapidity of pseudo top hadron
        _h_absrapttbar->fill(pttbar.absrap(), weight); //absolute rapidity of tt bar
        _h_ttbarmass->fill(pttbar.mass(), weight); //mass of tt bar
        _h_ptttbar->fill(pttbar.pt(), weight); //fill pT of tt bar in combined channel

        //electron channel fill histogram
        if (pass_ejets) {
          _h_ptpseudotoplepton_el->fill(ppseudotoplepton.pt(), weight); //pT of pseudo top lepton
          _h_absrappseudotoplepton_el->fill(ppseudotoplepton.absrap(), weight); //absolute rapidity of pseudo top lepton
          _h_ptpseudotophadron_el->fill(ppseudotophadron.pt(), weight); //pT of pseudo top hadron
          _h_absrappseudotophadron_el->fill(ppseudotophadron.absrap(), weight); //absolute rapidity of pseudo top hadron
          _h_absrapttbar_el->fill(pttbar.absrap(), weight); //absolute rapidity of tt bar
          _h_ttbarmass_el->fill(pttbar.mass(), weight); // fill electron channel ttbar mass
          _h_ptttbar_el->fill(pttbar.pt(), weight); //fill pT of tt bar in electron channel
        }
        //muon channel fill histogram
        else {
          _h_ptpseudotoplepton_mu->fill(ppseudotoplepton.pt(), weight); //pT of pseudo top lepton
          _h_absrappseudotoplepton_mu->fill(ppseudotoplepton.absrap(), weight); //absolute rapidity of pseudo top lepton
          _h_ptpseudotophadron_mu->fill(ppseudotophadron.pt(), weight); //pT of pseudo top hadron
          _h_absrappseudotophadron_mu->fill(ppseudotophadron.absrap(), weight); //absolute rapidity of pseudo top hadron
          _h_absrapttbar_mu->fill(pttbar.absrap(), weight); //absolute rapidity of tt bar
          _h_ttbarmass_mu->fill(pttbar.mass(), weight); //fill muon channel histograms
          _h_ptttbar_mu->fill(pttbar.pt(), weight); //fill pT of tt bar in electron channel
        }
      }

      void finalize() {
        // Normalize to cross-section
        const double norm = crossSection()/sumOfWeights()/picobarn;

        scale(_h_ptpseudotoplepton,     0.5*norm); scale(_h_ptpseudotoplepton_el,     norm); scale(_h_ptpseudotoplepton_mu,     norm);
        scale(_h_absrappseudotoplepton, 0.5*norm); scale(_h_absrappseudotoplepton_el, norm); scale(_h_absrappseudotoplepton_mu, norm);
        scale(_h_ptpseudotophadron,     0.5*norm); scale(_h_ptpseudotophadron_el,     norm); scale(_h_ptpseudotophadron_mu,     norm);
        scale(_h_absrappseudotophadron, 0.5*norm); scale(_h_absrappseudotophadron_el, norm); scale(_h_absrappseudotophadron_mu, norm);
        scale(_h_absrapttbar,           0.5*norm); scale(_h_absrapttbar_el,           norm); scale(_h_absrapttbar_mu,           norm);
        scale(_h_ttbarmass,             0.5*norm); scale(_h_ttbarmass_el,             norm); scale(_h_ttbarmass_mu,             norm);
        scale(_h_ptttbar,               0.5*norm); scale(_h_ptttbar_el,               norm); scale(_h_ptttbar_mu,               norm);

        //truth hists scaling
        //electron channel
        scale(_h_lpt_el,   norm); scale(_h_leta_el,  norm); scale(_h_lphi_el,  norm); scale(_h_j1pt_el, norm);
        scale(_h_j1eta_el, norm); scale(_h_j1phi_el, norm); scale( _h_tty_el,  norm); scale(_h_pttt_el, norm);
        scale(_h_mtt_el,   norm); scale(_h_tlpt_el,  norm); scale(_h_tly_el,   norm); scale(_h_tlm_el,  norm);
        scale(_h_thpt_el,  norm); scale(_h_thy_el,   norm); scale(_h_thm_el,   norm); scale(_h_met_el,  norm);

        //muon channel
        scale(_h_lpt_mu,   norm); scale(_h_leta_mu,  norm); scale(_h_lphi_mu,  norm); scale(_h_j1pt_mu, norm);
        scale(_h_j1eta_mu, norm); scale(_h_j1phi_mu, norm); scale( _h_tty_mu,  norm); scale(_h_pttt_mu, norm);
        scale(_h_mtt_mu,   norm); scale(_h_tlpt_mu,  norm); scale(_h_tly_mu,   norm); scale(_h_tlm_mu,  norm);
        scale(_h_thpt_mu,  norm); scale(_h_thy_mu,   norm); scale(_h_thm_mu,   norm); scale(_h_met_el,  norm);
      }

    private:

      double computeneutrinoz(const FourMomentum& lepton, const FourMomentum& met) const {
        //computing z component of neutrino momentum given lepton and met
        double pzneutrino;
        double a, b, c, k, m_W; //components of quadratric formula involved in computeneutrinoz
        m_W = 80.399; // in GeV, given in the paper
        k = (( sqr ( m_W ) - sqr( lepton.mass() )) / 2 )+ (lepton.px()*met.px() + lepton.py()*met.py());
        a = sqr ( lepton.E() )- sqr ( lepton.pz() );
        b = -2*k*lepton.pz();
        c = sqr( lepton.E() ) * sqr( met.pT() ) - sqr( k );

        double discriminant = sqr(b)-4*a*c;
        double quad[2] = {(-b - sqrt(discriminant)) / (2*a) , (-b + sqrt(discriminant)) / (2*a)}; //two possible quadratic solns
        if (discriminant < 0){
          pzneutrino = -b/(2*a); //if the discriminant is negative
        }
        else { //if the discriminant is greater than or equal to zero, take the soln with smallest absolute value
          double absquad[2];
          for (int n=0; n<2; n++) {
            absquad[n] = fabs (quad[n]);
          }
          if (absquad[0] < absquad[1]) {
            pzneutrino = quad[0];
          }
          else
            pzneutrino = quad[1];
        }
        if(!std::isfinite(pzneutrino)){
          std::cout << "Found non-finite value" << std::endl;}
        return pzneutrino;
      }

      FourMomentum computepseudotop(const FourMomentum& one, const FourMomentum& two, const FourMomentum& three) const{
        //computing pseudotop four-momenta (just add the 4-vectors)
        FourMomentum ppseudotop;
        ppseudotop = one + two + three;
        return ppseudotop;
      }

      // Event selection functions
      bool _ejets(unsigned int& cutBits) {
        // 1. Exactly one good electron
        cutBits += 1; if (_dressedelectrons.size() != 1) return false;
        // 1.5 no additional veto electrons
        cutBits += 1 << 1; if (_vetodressedelectrons.size() > 1) return false;
        // 2. No muons passing the veto selection
        cutBits += 1 << 2; if (_vetodressedmuons.size() > 0) return false;
        // 3. total neutrino pT > 30 GeV
        cutBits += 1 << 3; if (_met_et <= 30.0*GeV) return false;
        // 4. MTW > 35 GeV
        cutBits += 1 << 4;
        if (_transMass(_dressedelectrons[0].pT(), _dressedelectrons[0].phi(), _met_et, _met_phi) <= 35*GeV) return false;
        // 5. At least two b-tagged jets
        cutBits += 1 << 5; if (_jet_ntag < 2) return false;
        // 6. At least four good jets
        cutBits += 1 << 6; if (_jets.size() < 4) return false;
        return true;
      }

      bool _mujets(unsigned int& cutBits) {
        // 1. Exactly one good muon
        cutBits += 1; if (_dressedmuons.size() != 1) return false;
        // 1.5 no additional veto muons
        cutBits += 1 << 1; if (_vetodressedmuons.size() > 1) return false;
        // 2. No electrons passing the veto selection
        cutBits += 1 << 2; if (_vetodressedelectrons.size() > 0) return false;
        // 3. total neutrino pT > 30 GeV
        cutBits += 1 << 3; if (_met_et <= 30*GeV) return false;
        // 4. MTW > 35 GeV
        cutBits += 1 << 4;
        if (_transMass(_dressedmuons[0].pT(), _dressedmuons[0].phi(), _met_et, _met_phi) <= 35*GeV) return false;
        // 5. At least two b-tagged jets
        cutBits += 1 << 5; if (_jet_ntag < 2) return false;
        // 6. At least four good jets
        cutBits += 1 << 6; if (_jets.size() < 4) return false;
        return true;
      }

      void remove_jet(std::vector<unsigned int> veto_list, Jets& jets) {
        //Remove jet of index iveto from the list jets
        Jets new_jets;
        for (unsigned int i = 0; i < jets.size(); i++){
          if (std::find(veto_list.begin(), veto_list.end(), i) == veto_list.end()) {
            new_jets.push_back(jets.at(i));
          }
        }
        jets = new_jets;
      }

      double _transMass(double ptLep, double phiLep, double met, double phiMet) {
        return sqrt(2.0*ptLep*met*(1 - cos(phiLep-phiMet)));
      }

      /// @name Objects that are used by the event selection decisions
      //@{
      vector<DressedLepton> _dressedelectrons;
      vector<DressedLepton> _vetodressedelectrons;
      vector<DressedLepton> _dressedmuons;
      vector<DressedLepton> _vetodressedmuons;
      vector<double> _bjetsdR;
      Particles _neutrinos;
      Jets _jets, _bjets, _lightjets;
      unsigned int _jet_ntag;
      /// @todo Why not store the whole MET FourMomentum?
      double _met_et, _met_phi;
      bool _overlap;
      //@}

      Histo1DPtr _h_ptpseudotoplepton, _h_absrappseudotoplepton, _h_ptpseudotophadron, _h_absrappseudotophadron;
      Histo1DPtr _h_absrapttbar, _h_ttbarmass, _h_ptttbar;
      Histo1DPtr _h_ptpseudotoplepton_el, _h_absrappseudotoplepton_el, _h_ptpseudotophadron_el, _h_absrappseudotophadron_el;
      Histo1DPtr _h_absrapttbar_el, _h_ttbarmass_el, _h_ptttbar_el;
      Histo1DPtr _h_ptpseudotoplepton_mu, _h_absrappseudotoplepton_mu, _h_ptpseudotophadron_mu, _h_absrappseudotophadron_mu;
      Histo1DPtr _h_absrapttbar_mu, _h_ttbarmass_mu, _h_ptttbar_mu;
      //diagnostic hists
      Histo1DPtr _h_nbjet, _h_njet, _h_metet, _h_elecpt, _h_eleceta, _h_muonpt, _h_muoneta,
                 _h_jet1pt, _h_jet1eta, _h_jet2pt, _h_jet2eta,
                 _h_bjet1pt, _h_bjet1eta, _h_bjet2pt, _h_bjet2eta,
                 _h_nneutrino;
      Histo1DPtr _h_dR_jet_el, _h_dR_jet_mu;
      //truth hists
      Histo1DPtr  _h_lpt_el, _h_leta_el, _h_lphi_el, _h_j1pt_el, _h_j1eta_el, _h_j1phi_el, _h_tty_el, _h_pttt_el,
                  _h_mtt_el, _h_tlpt_el, _h_tly_el, _h_tlm_el, _h_thpt_el, _h_thy_el, _h_thm_el, _h_met_el, _h_met_pT_el;
      Histo1DPtr  _h_lpt_mu, _h_leta_mu, _h_lphi_mu, _h_j1pt_mu, _h_j1eta_mu, _h_j1phi_mu, _h_tty_mu, _h_pttt_mu,
                  _h_mtt_mu, _h_tlpt_mu, _h_tly_mu, _h_tlm_mu, _h_thpt_mu, _h_thy_mu, _h_thm_mu, _h_met_mu, _h_met_pT_mu;
  };
  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(ATLAS_2015_I1345452);

}
