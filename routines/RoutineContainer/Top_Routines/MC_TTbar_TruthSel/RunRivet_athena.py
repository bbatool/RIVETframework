#include("GeneratorUtils/StdEvgenSetup.py")
theApp.EvtMax = -1
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
import AthenaPoolCnvSvc.ReadAthenaPool
from AthenaCommon.AlgSequence import AlgSequence
from Rivet_i.Rivet_iConf import Rivet_i
from GaudiSvc.GaudiSvcConf import THistSvc

svcMgr.EventSelector.InputCollections =[]
job = AlgSequence()


InputFolder = "/nfs/atlas/top-modelling01/PowhegModelling_7TeV/Summary_withHdamp_ljets_PowPy8_pTHard0/SummaryFolderPool/"
OutputRoot  = "Output_test.root"
OutputYoda  = "Output_test.yoda"
FileList = os.listdir(InputFolder)
for file in FileList:
    if not "1.pool.root" in file:
        continue
    inputFile = InputFolder+file
    if os.path.exists(inputFile):
        svcMgr.EventSelector.InputCollections.append(inputFile)

rivet = Rivet_i("Rivet")
rivet.AnalysisPath = os.environ['PWD']
rivet.Analyses += [ 'MC_TTbar_TruthSel' ]
#7TeV
rivet.Analyses += [ 'ATLAS_2014_I1304688']
rivet.Analyses += [ 'ATLAS_2013_I1243871']
rivet.Analyses += [ 'ATLAS_2014_I1304289']
#Top-fiducial
rivet.Analyses += [ 'ATLAS_2015_I1345452']
rivet.HistoFile = OutputYoda
rivet.CrossSection = 1
job += rivet

svcMgr += THistSvc()
svcMgr.THistSvc.Output = ["Rivet DATAFILE='"+OutputRoot+"' OPT='RECREATE'"]
