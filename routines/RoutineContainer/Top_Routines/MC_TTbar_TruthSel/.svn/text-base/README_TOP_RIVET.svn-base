# This document provides instructions on how to 
# retrieve Rivet -- and its glue packages -- from svn
# and compile on an lxplus slc6 machine

# cd into your testarea and setup an 
# Athena release (this example uses nightly r2)

   asetup devval,r2,slc6,64,here

# Get the packages

   cmt co External/YODA
   cmt co External/Rivet
   cmt co Generators/Rivet_i

# Build the packages (in this order)  

   cd External/YODA/cmt
   cmt config; make clean; make

   cd External/Rivet/cmt
   cmt config; make clean; make

   cd Generators/Rivet_i/cmt
   make clean; make

# Rivet2.x should now be set-up and ready for use
# You can check this by executing the following command:

   rivet --list-analyses

# Now let's perform a local test run with Pythia8 using
# one of our TOP rivet analyses (e.g. MC_TTbar_TruthSel)
# If you don't already have the routines locally, get them 
# from svn ($SVNPHYS/Physics/Top/Software/MCvalidation/Rivet/Rivet1.X/trunk/routines)
# In the directory where the routine *.cc, *.info, and *.plot are, execute the command

    rivet-buildplugin RivetMC_TTbar_TruthSel.so MC_TTbar_TruthSel.cc

# The analysis should now be built into the Rivet analysis plugin library.
# In order to use the analysis, set the environment variable
# to the directory where the *.so file is located

    export RIVET_ANALYSIS_PATH=$PWD

# You can check that the analysis is ready for use with:

    rivet --show-analysis MC_TTbar_TruthSel

# Now we are ready for a test run. Go into the 
# Generators/Rivet_i/examples directory and open 
# the file jobOptions.rivet.py. Add the line

    topAlg.Rivet.Analyses = ["MC_TTbar_TruthSel"]

# to this file. Now execute

    athena.py jobOptions.rivet.py

# If you'd like to plot the .yoda file, do

    rivet-mkhtml Rivet.yoda
