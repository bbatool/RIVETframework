#! /bin/bash

source /afs/cern.ch/sw/lcg/external/MCGenerators_lcgcmt67c/rivet/2.2.0/x86_64-slc6-gcc47-opt/rivetenv-genser.sh

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

setupATLAS
localSetupGcc gcc472_x86_64_slc6
localSetupPython 2.7.3-x86_64-slc6-gcc47
source /afs/cern.ch/sw/lcg/external/MCGenerators_lcgcmt67c/rivet/2.2.0/x86_64-slc6-gcc47-opt/rivetenv.sh

rivet-mkhtml Output_test.yoda:'Title=TestPlot':'LineWidth=0.06':'LineStyle=solid'