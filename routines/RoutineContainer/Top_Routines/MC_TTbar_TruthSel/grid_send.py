#!/bin/python
import os,re,sys

Samples = [
    'mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.evgen.EVNT.e3698/'
#    '',
#    '',
#    '',
#    '',
#    ''
    ]
path = os.environ['PWD']
RegEx = re.compile( '(\w*)\.(\d*)\.(\w*)\.(\w*)\.(\w*)\.(\w*)/' )

UserName = 'sthenkel'
Version = 'rivetTest4'
JOs = path+'/JO_rivet.py'
OwnRivetAnalysis = path+'/RivetMC_TTbar_TruthSel.so'

athenaRel = '19.2.1.3,slc6'
cmtConf = "x86_64-slc5-gcc43-opt"
nJobs = 50
FilesPerJob = 5
GBperJob = 'MAX'
nFiles = 5

for sample in Samples:
    Match = RegEx.match( sample )
    outSample = 'user.' + UserName + '.' + Version + '.' + Match.group( 2 ) + '.' + Match.group( 3 ) +'.' + Match.group( 5 ) + '.' + Match.group( 6 )
    #    output_yoda = Match.group(2) + '_' +Match.group(6) +'.yoda' +
#needs to match the rivet.HistoFile in JOs
    output_yoda = 'Output_test.yoda'
    print '###############################################################################'
    print '### >>> sending job for / preparing command : '+outSample
    print '###############################################################################\n'
    command = 'pathena %s --athenaTag=%s --cmtConfig=%s --nJobs=%d --long --nFilesPerJob=%d --nGBPerJob=%s --extOutFile %s --inDS=%s --outDS=%s --extFile=%s' % (JOs, athenaRel, cmtConf, nJobs, FilesPerJob, GBperJob, output_yoda, sample, outSample, OwnRivetAnalysis)
    print '###############################################################################'
    print '### >>> command : '+command
    print '###############################################################################\n'
    os.system( command )
