
#include("GeneratorUtils/StdEvgenSetup.py")

theApp.EvtMax = -1
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
import AthenaPoolCnvSvc.ReadAthenaPool
from AthenaCommon.AlgSequence import AlgSequence
from Rivet_i.Rivet_iConf import Rivet_i
from GaudiSvc.GaudiSvcConf import THistSvc

svcMgr.EventSelector.InputCollections =[]
svcMgr.EventSelector.SkipBadFiles = True
svcMgr.EventSelector.BackNavigation = True
job = AlgSequence()


InputFolder = "/afs/cern.ch/user/s/sthenkel/eos/atlas/user/s/sthenkel/13TeV/MC/TOP_EVNT/mc15_13TeV.410004.PowhegHerwigppEvtGen_UEEE5_ttbar_hdamp172p5_nonallhad.evgen.EVNT.e3836/"
OutputRoot  = "Output_test.root"
OutputYoda  = "Output_test.yoda"
FileList = os.listdir(InputFolder)
NumberOfFiles = 10
for file in FileList[:NumberOfFiles]:
    if not "1.pool.root" in file:
        continue
    inputFile = InputFolder+file
    if os.path.exists(inputFile):
        svcMgr.EventSelector.InputCollections.append(inputFile)

rivet = Rivet_i("Rivet")
rivet.AnalysisPath = os.environ['PWD']
rivet.Analyses += [ 'MC_TTbar_TruthSel' ]
#7TeV
rivet.Analyses += [ 'ATLAS_2014_I1304688']
rivet.Analyses += [ 'ATLAS_2013_I1243871']
rivet.Analyses += [ 'ATLAS_2014_I1304289']
#Top-fiducial
#rivet.Analyses += [ 'ATLAS_2015_I1345452']
rivet.HistoFile = OutputYoda
#rivet.MCEventKey
#rivet.CrossSection = 1
job += rivet

svcMgr += THistSvc()
svcMgr.THistSvc.Output = ["Rivet DATAFILE='"+OutputRoot+"' OPT='RECREATE'"]
