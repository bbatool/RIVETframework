// -*- C++ -*-
// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Tools/Logging.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/LeadingParticlesFinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Cuts.hh"

namespace Rivet {


  class ATLAS_nosel_MCval : public Analysis {
  public:

    /// @name Constructors etc.
    //@{

    /// Constructor
    ATLAS_nosel_MCval()
      : Analysis("ATLAS_nosel_MCval")
    {
      setNeedsCrossSection(false);
    }

    //@}


  public:

    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      Cut eta_full   = Rivet::Cuts::etaIn(-5.0, 5.0) & (Rivet::Cuts::pT >= 10.0*GeV);
      Cut eta_gamma  = Rivet::Cuts::etaIn(-5.0, 5.0) & (Rivet::Cuts::pT >=  5.0*GeV);

      // All final state particles                                                                                                                                     
      FinalState fs1(eta_full);
      FinalState fs2(eta_gamma);

      /// Initialise and register projections (selections on the final state)
      // projection to find the electrons
      // std::vector<std::pair<double, double> > eta_e;
      // eta_e.push_back(make_pair(-5.00,+5.00));
      IdentifiedFinalState elecs(fs1);
      elecs.acceptIdPair(PID::ELECTRON);
      addProjection(elecs, "elecs");

      // projection to find the muons
      // std::vector<std::pair<double, double> > eta_m;
      // eta_m.push_back(fs1);
      IdentifiedFinalState muons(fs1);
      muons.acceptIdPair(PID::MUON);
      addProjection(muons, "muons");

      // projection to find the taus
      // std::vector<std::pair<double, double> > eta_tau;
      //  eta_tau.push_back(fs1);
      IdentifiedFinalState taus(fs1);
      muons.acceptIdPair(PID::TAU);
      addProjection(taus, "taus");

      // projection to find FS gamma-s
      // std::vector<std::pair<double, double> > eta_gamma; 
      // eta_gamma.push_back(fs2); 
      IdentifiedFinalState gammas(fs2); 
      gammas.acceptId(PID::PHOTON); 
      addProjection(gammas, "gammas"); 


      // Leading neutrinos for Etmiss
      FinalState fs;
      LeadingParticlesFinalState muon_neutrino(fs);
      muon_neutrino.addParticleIdPair(PID::NU_MU);
      addProjection(muon_neutrino, "muon_neutrino");
      LeadingParticlesFinalState elec_neutrino(fs);
      elec_neutrino.addParticleIdPair(PID::NU_E);
      addProjection(elec_neutrino, "elec_neutrino");
      LeadingParticlesFinalState tau_neutrino(fs);
      tau_neutrino.addParticleIdPair(PID::NU_TAU);
      addProjection(tau_neutrino, "tau_neutrino");


      // Input for the jets: No neutrinos, no muons, and no electron which
      // passed the electron cuts ("elecs" finalstate from above)
      //VetoedFinalState veto;
      //veto.addVetoOnThisFinalState(elecs);
      //veto.addVetoPairId(PID::MUON);
      //veto.vetoNeutrinos();
      //FastJets jets(veto, FastJets::ANTIKT, 0.4);
      FastJets jets(fs, FastJets::ANTIKT, 0.4);
      addProjection(jets, "jets");

      // charged particles 
      ChargedFinalState cfs250(-5.0, 5.0, 0.250*GeV);
      addProjection(cfs250, "CFS250");

      /// book histograms

      // jets:
      _h_jet_1_pT = bookHisto1D("jet_1_pT", 500, 0, 500);
      _h_jet_1_eta = bookHisto1D("jet_1_eta", 50, -5.0, 5.0);
      _h_jet_2_pT = bookHisto1D("jet_2_pT", 500, 0, 500);
      _h_jet_2_eta = bookHisto1D("jet_2_eta", 50, -5.0, 5.0);
      _h_jet_3_pT = bookHisto1D("jet_3_pT", 500, 0, 500);
      _h_jet_3_eta = bookHisto1D("jet_3_eta", 50, -5.0, 5.0);
      _h_jet_4_pT = bookHisto1D("jet_4_pT", 500, 0, 500);
      _h_jet_4_eta = bookHisto1D("jet_4_eta", 50, -5.0, 5.0);
      _h_jet_5_pT = bookHisto1D("jet_5_pT", 500, 0, 500);
      _h_jet_5_eta = bookHisto1D("jet_5_eta", 50, -5.0, 5.0);

      _h_njet_10 = bookHisto1D("njet_10", 30, 0, 30);
      _h_njet_25 = bookHisto1D("njet_25", 20, 0, 20);
      _h_njet_40 = bookHisto1D("njet_40", 20, 0, 20);
      _h_njet_60 = bookHisto1D("njet_60", 20, 0, 20);      
      _h_njet_80 = bookHisto1D("njet_80", 20, 0, 20);
      _h_njet_100 = bookHisto1D("njet_100", 20, 0, 20); 
      _h_njet_160 = bookHisto1D("njet_160", 20, 0, 20); 

     //gammas: 
      _h_gamma_1_pT = bookHisto1D("gamma_1_pT", 1000, 0, 1000); 
      _h_gamma_1_eta = bookHisto1D("gamma_1_eta", 100, -5.0, 5.0); 
      _h_gamma_1_phi = bookHisto1D("gamma_1_phi", 100, -6.3, 6.3); 
      _h_gamma_N = bookHisto1D("gamma_N", 20, 0, 20);

      //leptons:
      _h_elec_1_pT = bookHisto1D("elec_1_pT", 1000, 0, 1000);
      _h_elec_1_eta = bookHisto1D("elec_1_eta", 100, -5.0, 5.0);
      _h_elec_1_phi = bookHisto1D("elec_1_phi", 100, -6.3, 6.3);
      _h_elec_N = bookHisto1D("elec_N", 20, 0, 20);

      _h_mu_1_pT = bookHisto1D("mu_1_pT", 1000, 0, 1000);
      _h_mu_1_eta = bookHisto1D("mu_1_eta", 100, -5.0, 5.0);
      _h_mu_1_phi = bookHisto1D("mu_1_phi", 100, -6.3, 6.3);
      _h_mu_N = bookHisto1D("mu_N", 20, 0, 20);

      _h_tau_1_pT = bookHisto1D("tau_1_pT", 1000, 0, 1000);
      _h_tau_1_eta = bookHisto1D("tau_1_eta", 100, -5.0, 5.0);
      _h_tau_1_phi = bookHisto1D("tau_1_phi", 100, -6.3, 6.3);
      _h_tau_N = bookHisto1D("tau_N", 20, 0, 20);

      // MET
      _h_MET=bookHisto1D("MET", 1000, 0, 1000);

      // charged particle properties
      _h_charged_N = bookHisto1D("charged_N", 1000, 0, 10000);
      _h_charged_pT = bookHisto1D("charged_pT", 500, 0, 500);
      _h_charged_eta = bookHisto1D("charged_eta", 500, -5.0, 5.0 );
      _h_charged_phi = bookHisto1D("charged_phi", 500, -6.3, 6.3 );

    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const double weight = event.weight();

      FourMomentum lepton, gamma;
      FourMomentum p_miss(0.,0.,0.,0.);

      
      const ChargedFinalState& charged250 = applyProjection<ChargedFinalState>(event, "CFS250");

      const FinalState& elecs = applyProjection<FinalState>(event, "elecs");
      const FinalState& muons = applyProjection<FinalState>(event, "muons");
      const FinalState& taus = applyProjection<FinalState>(event, "taus");

      ParticleVector elec_neutrino=applyProjection<FinalState>(event, "elec_neutrino").particles();
      ParticleVector muon_neutrino=applyProjection<FinalState>(event, "muon_neutrino").particles();
      ParticleVector tau_neutrino=applyProjection<FinalState>(event, "tau_neutrino").particles();

      ParticleVector gammas = applyProjection<FinalState>(event, "gammas").particles();

      //temp. dileptons hack
      // if ( (elecs.size()+muons.size()+taus.size())<2 ) vetoEvent;

      // fill lepton histograms

      //pt>10 GeV lepton #
      _h_elec_N->fill(elecs.size(),weight);
      _h_mu_N->fill(muons.size(),weight);      
      _h_tau_N->fill(taus.size(),weight);

      //pt>5 GeV gamma # 
      _h_gamma_N->fill(gammas.size(),weight);

      // gammas : 
      if (gammas.size()>0)  {
	  gamma=gammas[0].momentum();
          _h_gamma_1_pT->fill(gamma.pT()/GeV, weight);  
          _h_gamma_1_eta->fill(gamma.eta(), weight); 
          _h_gamma_1_phi->fill(gamma.phi(), weight); 	
      }

      // leading lepton
      if (elecs.size()>0)
	{
	  lepton=elecs.particles()[0].momentum();
	  _h_elec_1_pT->fill(lepton.pT()/GeV, weight); 
	  _h_elec_1_eta->fill(lepton.eta(), weight);
	  _h_elec_1_phi->fill(lepton.phi(), weight);
	}
      if (muons.size()>0)
	{
	  lepton=muons.particles()[0].momentum();
	  _h_mu_1_pT->fill(lepton.pT()/GeV, weight); 
	  _h_mu_1_eta->fill(lepton.eta(), weight);
	  _h_mu_1_phi->fill(lepton.phi(), weight);
	}
      if (taus.size()>0)
	{
	  lepton=taus.particles()[0].momentum();
	  _h_tau_1_pT->fill(lepton.pT()/GeV, weight); 
	  _h_tau_1_eta->fill(lepton.eta(), weight);
	  _h_tau_1_phi->fill(lepton.phi(), weight);
	}

      if (elec_neutrino.size()>0) p_miss+=elec_neutrino[0].momentum();
      if (muon_neutrino.size()>0) p_miss+=muon_neutrino[0].momentum();
      if (tau_neutrino.size()>0) p_miss+=tau_neutrino[0].momentum();
      
      Jets jets;
      foreach (const Jet& jet, applyProjection<FastJets>(event, "jets").jetsByPt(10.0*GeV)) {
	if (fabs(jet.eta())<5.0){
	  jets.push_back(jet);
	}
      }

      _h_charged_N->fill(charged250.particles().size(), weight);

      foreach (const Particle& p, charged250.particles()) {
	_h_charged_pT->fill(p.momentum().pT()/GeV,weight);
	_h_charged_eta->fill(p.momentum().eta(),weight);
	_h_charged_phi->fill(p.momentum().phi(),weight);
      }	  
      
      int n10=0; int n25=0; int n40=0; int n60=0; int n80=0; int n100=0; int n160=0; 
      foreach (const Jet& jet, jets) {
	if ((jet.momentum().pT()/GeV)>10.) n10++;
	if ((jet.momentum().pT()/GeV)>25.) n25++;
	if ((jet.momentum().pT()/GeV)>40.) n40++;
	if ((jet.momentum().pT()/GeV)>60.) n60++;
	if ((jet.momentum().pT()/GeV)>80.) n80++;
	if ((jet.momentum().pT()/GeV)>100.) n100++;
	if ((jet.momentum().pT()/GeV)>160.) n160++;
	//MSG_INFO("Jet pT = " << jet.momentum().pT()/GeV << " GeV");
      }

      _h_njet_10->fill(n10,weight);
      _h_njet_25->fill(n25,weight);
      _h_njet_40->fill(n40,weight);
      _h_njet_60->fill(n60,weight);      
      _h_njet_80->fill(n80,weight);
      _h_njet_100->fill(n100,weight); 
      _h_njet_160->fill(n160,weight);	  

      if (jets.size()>0) 
	{
	  _h_jet_1_pT->fill(jets[0].momentum().pT()/GeV, weight);
	  _h_jet_1_eta->fill(jets[0].momentum().eta(), weight);
	}
      if (jets.size()>1) 
	{
	  _h_jet_2_pT->fill(jets[1].momentum().pT()/GeV, weight);
	  _h_jet_2_eta->fill(jets[1].momentum().eta(), weight);
	}	  
      if (jets.size()>2) 
	{
	  _h_jet_3_pT->fill(jets[2].momentum().pT()/GeV, weight);
	  _h_jet_3_eta->fill(jets[2].momentum().eta(), weight);
	}
      if (jets.size()>3) 
	{
	  _h_jet_4_pT->fill(jets[3].momentum().pT()/GeV, weight);
	  _h_jet_4_eta->fill(jets[3].momentum().eta(), weight);
	}
      if (jets.size()>4) 
	{
	  _h_jet_5_pT->fill(jets[4].momentum().pT()/GeV, weight);
	  _h_jet_5_eta->fill(jets[4].momentum().eta(), weight);
	}
      
      _h_MET->fill(p_miss.Et()/GeV, weight);   

    }


    /// Normalise histograms etc., after the run
    void finalize() {
      //double normfac=crossSection()/sumOfWeights();
      double normfac=1;
      scale(_h_jet_1_pT, normfac);
      scale(_h_jet_1_eta, normfac);
      scale(_h_jet_2_pT, normfac);
      scale(_h_jet_2_eta, normfac);
      scale(_h_jet_3_pT, normfac);
      scale(_h_jet_3_eta, normfac);
      scale(_h_jet_4_pT, normfac);
      scale(_h_jet_4_eta, normfac);
      scale(_h_jet_5_pT, normfac);
      scale(_h_jet_5_eta, normfac);

      scale(_h_njet_10, normfac);
      scale(_h_njet_25, normfac);
      scale(_h_njet_40, normfac);
      scale(_h_njet_60, normfac);
      scale(_h_njet_80, normfac);
      scale(_h_njet_100, normfac);
      scale(_h_njet_160, normfac);

      scale(_h_elec_1_pT, normfac);
      scale(_h_elec_1_eta, normfac);
      scale(_h_elec_1_phi, normfac);
      scale(_h_elec_N, normfac);
      scale(_h_mu_1_pT, normfac);
      scale(_h_mu_1_eta, normfac);
      scale(_h_mu_1_phi, normfac);
      scale(_h_mu_N, normfac);
      scale(_h_tau_1_pT, normfac);
      scale(_h_tau_1_eta, normfac);
      scale(_h_tau_1_phi, normfac);
      scale(_h_tau_N, normfac);

      scale(_h_MET, normfac);

      scale(_h_charged_N, normfac);
      scale(_h_charged_pT, normfac);
      scale(_h_charged_eta, normfac);
      scale(_h_charged_phi, normfac);
    }

    //@}


  private:

    /// @name Histograms
    //@{

    Histo1DPtr _h_jet_1_pT;
    Histo1DPtr _h_jet_1_eta;
    Histo1DPtr _h_jet_2_pT;
    Histo1DPtr _h_jet_2_eta;
    Histo1DPtr _h_jet_3_pT;
    Histo1DPtr _h_jet_3_eta;
    Histo1DPtr _h_jet_4_pT;
    Histo1DPtr _h_jet_4_eta;
    Histo1DPtr _h_jet_5_pT;
    Histo1DPtr _h_jet_5_eta;
    Histo1DPtr _h_njet_10;
    Histo1DPtr _h_njet_25;
    Histo1DPtr _h_njet_40;
    Histo1DPtr _h_njet_60;
    Histo1DPtr _h_njet_80;
    Histo1DPtr _h_njet_100;
    Histo1DPtr _h_njet_160;
    Histo1DPtr _h_elec_1_pT;
    Histo1DPtr _h_elec_1_eta;
    Histo1DPtr _h_elec_1_phi;
    Histo1DPtr _h_elec_N;
    Histo1DPtr _h_gamma_1_pT; 
    Histo1DPtr _h_gamma_1_eta; 
    Histo1DPtr _h_gamma_1_phi; 
    Histo1DPtr _h_gamma_N; 
    Histo1DPtr _h_mu_1_pT;
    Histo1DPtr _h_mu_1_eta;
    Histo1DPtr _h_mu_1_phi;
    Histo1DPtr _h_mu_N;
    Histo1DPtr _h_tau_1_pT;
    Histo1DPtr _h_tau_1_eta;
    Histo1DPtr _h_tau_1_phi;
    Histo1DPtr _h_tau_N;
    Histo1DPtr _h_MET;
    Histo1DPtr _h_charged_N;
    Histo1DPtr _h_charged_pT;
    Histo1DPtr _h_charged_eta;
    Histo1DPtr _h_charged_phi;    
    //@}

  };



  // This global object acts as a hook for the plugin system
  AnalysisBuilder<ATLAS_nosel_MCval> plugin_ATLAS_nosel_MCval;


}

