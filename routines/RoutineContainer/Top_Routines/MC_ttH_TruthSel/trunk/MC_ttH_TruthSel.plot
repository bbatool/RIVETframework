## BEGIN PLOT /MC_ttH_TruthSel/*
NormalizeToSum=1
#RatioPlotErrorBandColor={[rgb]{0.2,0.6,0.8}}
RatioPlotErrorBandColor=yellow
LegendXPos=0.67
LegendYPos=0.93
## END PLOT

## BEGIN PLOT /MC_ttH_TruthSel/m_bb
XLabel=inv. mass of b-jets not ass. with top
YLabel=bin sum.
Title=m(b,b)
LogY=0
Rebin=2
RatioPlotYMin=0.73
RatioPlotYMax=1.27
LegendXPos=0.30
LegendYPos=0.94
## END PLOT

## BEGIN PLOT /MC_ttH_TruthSel/M_hadT
XLabel=m_{top} (hadr.)
YLabel=bin sum.
Title= mass of reco. hadronic top
LogY=0
RatioPlotYMin=0.73
RatioPlotYMax=1.27
LegendXPos=0.60
LegendYPos=0.94

## END PLOT

## BEGIN PLOT /MC_ttH_TruthSel/M_lepT
XLabel=m_{top} (lept.)
YLabel=bin sum.
Title= mass of reco. leptonic top
LogY=0
RatioPlotYMin=0.73
RatioPlotYMax=1.27
LegendXPos=0.60
LegendYPos=0.94
## END PLOT

## BEGIN PLOT /MC_ttH_TruthSel/N_bj
XLabel=Number of b-jets
YLabel=bin sum.
Title=Number of b-jets
LogY=0
RatioPlotYMin=0.73
RatioPlotYMax=1.27
LegendXPos=0.30
LegendYPos=0.94
## END PLOT

## BEGIN PLOT /MC_ttH_TruthSel/pt_ttH
XLabel=pT of ttH system [GeV]
YLabel=bin sum.
Title=pT of ttH system [GeV]
LogY=0
RatioPlotYMin=0.73
RatioPlotYMax=1.27
LegendXPos=0.30
LegendYPos=0.94
Rebin=4
## END PLOT
