## job option script to run Rivet inside Athena

from AthenaCommon.AppMgr import ServiceMgr as svcMgr
import glob
import os
import AthenaPoolCnvSvc.ReadAthenaPool

svcMgr.EventSelector.InputCollections = glob.glob("/nfs/atlas/top-modelling01/RivetRoutinesUnderConstruction/SamplesTTH/mc12_8TeV.169888.PowhegPythia8_AU2CT10_PowHelttH125inc_ljets.evgen.EVNT.e2053*/*.pool.root*")

# limit number of events
from AthenaCommon.AppMgr import theApp
theApp.EvtMax = -1 #1000000

## Now set up Rivet
from Rivet_i.Rivet_iConf import Rivet_i
rivet=Rivet_i("Rivet")

# pick analysis
rivet.AnalysisPath = os.getcwd()
rivet.Analyses    += ["MC_ttH_TruthSel"] 
rivet.CrossSection = 1

rivet.HistoFile = "RIVET_Test_ttH_Selection.yoda"

from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()
topSequence += rivet
