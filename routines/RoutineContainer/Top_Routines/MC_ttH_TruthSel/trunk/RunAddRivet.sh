#!/bin/bash

# setup athena
echo setting up athena
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
asetup 19.2.1.3,slc6,64,here

athena.py AddRivet2.py