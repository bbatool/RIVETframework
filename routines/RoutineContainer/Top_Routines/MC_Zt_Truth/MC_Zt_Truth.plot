## BEGIN PLOT /MC_Zt_Truth/*
NormalizeToIntegral=1
#NormalizeToSum=1
LegendXPos=0.75
LegendYPos=0.94
RatioPlotYMin=0.75
RatioPlotYMax=1.25
XTwosidedTicks=1
YTwosidedTicks=1
RatioPlotYLabel=NLO/LO
YLabel=Unity
LogY=0

## END PLOT
	
## ==========================================
# BEGIN PLOT /MC_Zt_Truth/weight
Title=MC generator weight
LogY=0
XLabel=weight
XMajorTickMarks=1
XMinorTickMarks=1
# END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/electrons_N
Title= Number of electrons
XLabel=Number of electrons
XMajorTickMarks=10
XMinorTickMarks=0
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/muons_N
Title=Number of muons
XLabel=Number of muons
XMajorTickMarks=10
XMinorTickMarks=0
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/leptons_N
Title=Number of leptons
XLabel=Number of leptons
XMajorTickMarks=10
XMinorTickMarks=0
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/electron_1_pt
Title=Highest electron $P_{T}$
XLabel=$P_{T}^{ele} [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/electron_1_eta
Title=Highest electron $\eta$
XLabel=$\eta^{ele}$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/electron_1_phi
Title=Highest electron $\phi$
XLabel=$\phi^{ele}$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/muon_1_pt
Title=Highest muon $P_{T}$
XLabel=$P_{T}^{\mu} [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/muon_1_eta
Title=Highest muon $\eta$
XLabel=$\eta^{\mu}$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/muon_1_phi
Title=Highest muon $\phi$
XLabel=$\phi^{\mu}$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/lepton_1_pt
Title=Highest lepton $P_{T}$
XLabel=$P_{T}^{lep} [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/lepton_1_eta
Title=Highest lepton $\eta$
XLabel=$\eta^{lep}$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/lepton_1_phi
Title=Highest lepton $\phi$
XLabel=$\phi^{lep}$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/electron_2_pt
Title=The second electron $P_{T}$
XLabel=$P_{T}^{ele} [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/electron_2_eta
Title=The second electron $\eta$
XLabel=$\eta^{ele}$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/electron_2_phi
Title=The second electron $\phi$
XLabel=$\phi^{ele}$
## END PLOT
## ==========================================
## BEGIN PLOT /MC_Zt_Truth/muon_2_pt
Title=The second muon $P_{T}$
XLabel=$P_{T}^{\mu} [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/muon_2_eta
Title=The second muon $\eta$
XLabel=$\eta^{\mu}$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/muon_2_phi
Title=The second muon $\phi$
XLabel=$\phi^{\mu}$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/lepton_2_pt
Title=The second lepton $P_{T}$
XLabel=$P_{T}^{lep} [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/lepton_2_eta
Title=The second lepton $\eta$
XLabel=$\eta^{lep}$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/lepton_2_phi
Title=The second lepton $\phi$
XLabel=$\phi^{lep}$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/electron_3_pt
Title=The third electron $P_{T}$
XLabel=$P_{T}^{ele} [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/electron_3_eta
Title=The third electron $\eta$
XLabel=$\eta^{ele}$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/electron_3_phi
Title=The third electron $\phi$
XLabel=$\phi^{ele}$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/muon_3_pt
Title=The third muon $P_{T}$
XLabel=$P_{T}^{\mu} [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/muon_3_eta
Title=The third muon $\eta$
XLabel=$\eta^{\mu}$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/muon_3_phi
Title=The third muon $\phi$
XLabel=$\phi^{\mu}$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/lepton_3_pt
Title=The third lepton $P_{T}$
XLabel=$P_{T}^{lep} [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/lepton_3_eta
Title=The third lepton $\eta$
XLabel=$\eta^{lep}$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/lepton_32_phi
Title=The third lepton $\phi$
XLabel=$\phi^{lep}$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/CosThetaStar
Title=W helicity
XLabel=$Cos(\theta^{*})$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/MET
Title=Missing transverse momentum
XLabel=Missing $E_{T} [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/MET_phi
Title=Neutrino $\phi$
XLabel=$\Phi^{\nu}$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/jets_N
Title=Number of jets
XLabel=Number of jets
XMajorTickMarks=10
XMinorTickMarks=0
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/jet_1_pt
Title=The first jet $P_{T}$
XLabel=$P_{T}^{jet1} [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/jet_1_eta
Title=The first jet \eta$
XLabel=$\eta^{jet1}$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/jet_1_phi
Title=The first jet $\phi$
XLabel=$\phi^{jet1}$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/jet_1_m
Title=The first jet mass
XLabel=$M_{jet1} [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/jet_2_pt
Title=The second jet $P_{T}$
XLabel=$P_{T}^{jet2} [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/jet_2_eta
Title=The second jet $\eta$
XLabel=$\eta^{jet2}$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/jet_2_phi
Title=The second jet $\phi$
XLabel=$\phi^{jet2}$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/jet_2_m
Title=The second jet mass
XLabel=$M_{jet2} [GeV]$
## END PLOT
## ==========================================
## BEGIN PLOT /MC_Zt_Truth/jet_3_pt
Title=The third jet $P_{T}$
XLabel=$P_{T}^{jet3} [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/jet_3_eta
Title=The third jet $\eta$
XLabel=$\eta^{jet3}$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/jet_3_phi
Title=The third jet $\phi$
XLabel=$\phi^{jet3}$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/jet_3_m
Title=The third jet mass
XLabel=$M_{jet3 [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/jet_4_pt
## BEGIN 
Title=The fourth jet $P_{T}$
XLabel=$P_{T}^{jet4} [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/jet_4_eta
Title=The fourth jet $\eta$
XLabel=$\eta^{jet4}$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/jet_4_phi
Title=The fourth jet $\phi$
XLabel=$\phi^{jet4}$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/jet_4_m
Title=The fourth jet mass
XLabel=$M_{jet4 [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/bjets_N
Title=Number of b-jets
XLabel=Number of b-jets
XMajorTickMarks=10
XMinorTickMarks=0
## END PLOT

## ==========================================

## BEGIN PLOT /MC_Zt_Truth/bjet_1_pt
Title=The first bjet $P_{T}$
XLabel=$P_{T}^{bjet1} [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/bjet_1_eta
Title=The first bjet $\eta$
XLabel=$\eta^{bjet1}$
## END PLOT
## ==========================================
## BEGIN PLOT /MC_Zt_Truth/bjet_1_phi
Title=The first bjet $\phi$
XLabel=$\phi^{bjet1}$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/bjet_1_m
Title=The first bjet mass
XLabel=$M_{bjet1} [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/bjet_2_pt
Title=The second bjet $P_{T}$
XLabel=$P_{T}^{bjet2} [GeV]$
## END PLOT
## ==========================================
## BEGIN PLOT /MC_Zt_Truth/bjet_2_eta
Title=The second bjet $\eta$
XLabel=$\eta^{bjet2}$
## END PLOT
## ==========================================
## BEGIN PLOT /MC_Zt_Truth/bjet_2_phi
Title=The second bjet $\phi$
XLabel=$\phi^{bjet2}$
## END PLOT

## ==========================================

## BEGIN PLOT /MC_Zt_Truth/bjet_2_m
Title=The second bjet mass
XLabel=$M_{bjet2} [GeV]$
## END PLOT
## ==========================================
## BEGIN PLOT /MC_Zt_Truth/bjet_3_pt
## BEGIN third bjet $P_{T}$
XLabel=$P_{T}^{bjet3} [GeV]$
## END PLOT
## ==========================================
## BEGIN PLOT /MC_Zt_Truth/bjet_3_eta
Title=The third bjet $\eta$
XLabel=$\eta^{bjet3}$
## END PLOT
## ==========================================
## BEGIN PLOT /MC_Zt_Truth/jet_3_phi
Title=The third jet $\phi$
XLabel=$\phi^{jet3}$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/bjet_3_m
Title=The third bjet mass
XLabel=$M_{bjet3 [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/bjet_4_pt
Title=The fourth bjet $P_{T}$
XLabel=$P_{T}^{bjet4} [GeV]$
## END PLOT
## ==========================================
## BEGIN PLOT /MC_Zt_Truth/bjet_4_eta
Title=The fourth bjet $\eta$
XLabel=$\eta^{jet4}$
## END PLOT
## ==========================================
## BEGIN PLOT /MC_Zt_Truth/bjet_4_phi
Title=The fourth bjet $\phi$
XLabel=$\phi^{bjet4}$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/bjet_4_m
Title=The fourth bjet mass
XLabel=$M_{bjet4} [GeV]$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/W_pt
Title=W boson $P_{T}$
XLabel=$P_{T}^{W} [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/W_eta
Title=W boson $\eta$
XLabel=$\eta^{W}$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/W_phi
Title=W boson $\phi$
XLabel=$\phi^{W}$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/W_m
Title=W boson mass
XLabel=$M_{W} [GeV]$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/W_mt
Title=W boson transverse mass
XLabel=$MT_{W} [GeV]$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/top_pt
Title=top $P_{T}$
XLabel=$P_{T}^{top} [GeV]$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/top_eta
Title=top $\eta$
XLabel=$\eta^{top}$

## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/top_phi
Title=top $\phi$
XLabel=$\phi^{top}$
## END PLOT
## ==========================================

## BEGIN PLOT /MC_Zt_Truth/top_m
Title=top mass
XLabel=$M_{top} [GeV]$
## END PLOT

## ==========================================

## BEGIN PLOT /MC_Zt_Truth/Z_pt
Title=Z boson $P_{T}$
XLabel=$P_{T}^{Z} [GeV]$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/Z_eta
Title=Z boson $\eta$
XLabel=$\eta^{Z}$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/Z_phi
Title=Z boson $\phi$
XLabel=$\phi^{Z}$
## END PLOT

## ==========================================
## BEGIN PLOT /MC_Zt_Truth/Z_m
Title=Z boson mass
XLabel=$M_{Z} [GeV]$
## END PLOT
## ==========================================
## BEGIN PLOT /MC_Zt_Truth/Z_m_allcombination
Title=Z boson mass from all jet paris
XLabel=$M_{Z} [GeV]$
## END PLOT
