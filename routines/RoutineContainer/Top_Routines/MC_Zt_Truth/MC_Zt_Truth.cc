#include "Rivet/Analysis.hh"
#include "Rivet/Tools/Logging.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/VisibleFinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/LeadingParticlesFinalState.hh"
#include "Rivet/Projections/UnstableFinalState.hh"
#include "Rivet/Math/LorentzTrans.hh"
// Vetoed Jet inputs
#include "Rivet/Projections/VetoedFinalState.hh"
// event record specific
#include "Rivet/Particle.hh"
#include "Rivet/Tools/ParticleIdUtils.hh"

#include "HepMC/GenEvent.h"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/TauFinder.hh"
#include "Rivet/Projections/Beam.hh"

#include <math.h>
#include <string>
#include <sstream>
#include <iostream>
#include <vector>

using namespace std;

namespace patch {

    template < typename T > std::string to_string(const T& n) {
        std::ostringstream stm;
        stm << n;
        return stm.str();
    }
}

namespace Rivet {

    class MC_Zt_Truth : public Analysis {
    public:

        MC_Zt_Truth()
        : Analysis("MC_Zt_Truth") {
            setNeedsCrossSection(false);
        }
    public:

        //=====================================================================
        // Set up projections and book histograms
        //=====================================================================

        void init() {

            ReadSteeringFile("Steering_ZTop_Truth");

            cout << "You chosen to analyze ZChannel<" << ZChannel << "> and TopQuarkChannel<" << TopChannel << ">" << endl;

            vector<string> ZBosonChannel;
            ZBosonChannel.push_back("Hadronic");
            ZBosonChannel.push_back("Leptonic");
            ZBosonChannel.push_back("Invesible");

            vector<string> TopQuarkChannel;
            TopQuarkChannel.push_back("Hadronic");
            TopQuarkChannel.push_back("Leptonic");

            std::vector<string>::iterator it;
            it = find(ZBosonChannel.begin(), ZBosonChannel.end(), ZChannel);
            if (it == ZBosonChannel.end()) {
                std::cout << "The ZChannel: " << ZChannel << " is unknown, please check Steering_ZTop_Truth file \n";
                exit(0);
            }

            it = find(TopQuarkChannel.begin(), TopQuarkChannel.end(), TopChannel);
            if (it == TopQuarkChannel.end()) {
                std::cout << "The TopQuarkChannel: " << TopChannel << " is unknown, please check Steering_ZTop_Truth file \n";
                exit(0);
            }

            //Check the non allowed combinations
            if ((ZChannel == "Invesible" && TopChannel == "Hadronic") || (ZChannel == "Hadronic" && TopChannel == "Hadronic")) {
                std::cout << "The combination of the top quark and Z boson decays is not recognized\n";
                exit(0);
            }

            // Eta ranges
            //Cut eta_full = Cuts::etaIn(-5.0, 5.0) & (Cuts::pT >= 1.0 * MeV);
            Cut eta_lep = Cuts::etaIn(-4.5, 4.5);

            // FinalState projector to select all particles in |eta| < 4.5
            const FinalState fs(-4.5, 4.5);
            addProjection(fs, "fs");

            // Get photons to dress leptons
            IdentifiedFinalState photons(fs);
            photons.acceptIdPair(PID::PHOTON);

            // projection to find the electrons (selection on the final state projector)
            IdentifiedFinalState el_id(-4.5, +4.5, 25.0 * GeV);
            el_id.acceptIdPair(PID::ELECTRON);
            PromptFinalState electrons(el_id);
            electrons.acceptTauDecays(true);
            addProjection(electrons, "electrons");
            //it is needed later
            DressedLeptons dressedelectrons(photons, electrons, 0.1, true, eta_lep & (Cuts::pT >= 25.0 * GeV), true);
            addProjection(dressedelectrons, "dressedelectrons");


            // projection to find the muons (selection on the final state projector)
            IdentifiedFinalState mu_id(-4.5, +4.5, 25.0 * GeV);
            mu_id.acceptIdPair(PID::MUON);
            PromptFinalState muons(mu_id);
            muons.acceptTauDecays(true);
            addProjection(muons, "muons");
            //it is needed later
            DressedLeptons dressedmuons(photons, muons, 0.1, true, eta_lep & (Cuts::pT >= 25.0 * GeV), true);
            addProjection(dressedmuons, "dressedmuons");


            // projection to find the taus (selection on the final state projector)
            UnstableFinalState taus(-4.5, +2.5, 25.0 * GeV);
            addProjection(taus, "taus");

            // projection to find the neutrinos (selection on the final state projector)
            //      IdentifiedFinalState neutrinos(-4.5, 4.5, 25.0*GeV);

            IdentifiedFinalState nu_id(-4.5, 4.5, 0.0 * GeV);
            nu_id.acceptNeutrinos();
            PromptFinalState neutrinos(nu_id);
            neutrinos.acceptTauDecays(true);
            addProjection(neutrinos, "neutrinos");

            // FinalState used as input for jet-finding (including everything except muons and neutrinos)
            VetoedFinalState jet_input(fs);
            jet_input.vetoNeutrinos();
            jet_input.addVetoPairId(PID::MUON);
            addProjection(jet_input, "jet_input");
            FastJets jets(jet_input, FastJets::ANTIKT, 0.4);
            addProjection(jets, "jets");

            // Jet clustering.
            //Ask Dominic
            //            VetoedFinalState vfs;
            //            vfs.addVetoOnThisFinalState(ewdressedelectrons);
            //            vfs.addVetoOnThisFinalState(ewdressedmuons);
            //            vfs.addVetoOnThisFinalState(neutrinos);
            //            FastJets jets(vfs, FastJets::ANTIKT, 0.4);
            //            jets.useInvisibles();
            //            addProjection(jets, "jets");

            // book histograms

            BookHistograms();
        }

        //=====================================================================
        // Perform the per-event analysis
        //=====================================================================

        void analyze(const Event& event) {

            // bool _debug=false;
            _weight = event.weight();
            _h_weight->fill(_weight);
            MSG_DEBUG("mc weight: " << _weight);

            //Sort and select Good Particle

            //_dressedelectrons = sortByPt(applyProjection<DressedLeptons>(event, "dressedelectrons").dressedLeptons());
            const FinalState& ElectronCollection = applyProjection<FinalState>(event, "electrons");
            vector<Particle> GoodElectrons; //= fillPropertiesFromCollection(ElectronCollection);

            foreach(const Particle& _p, ElectronCollection.particlesByPt()) {
                if (_p.momentum().pT() > 0 && fabs(_p.momentum().eta()) < 4.5) {
                    GoodElectrons.push_back(_p);
                }
            }

            const FinalState& MuonCollection = applyProjection<FinalState>(event, "muons");
            vector<Particle> GoodMuons; // = fillPropertiesFromCollection(MuonCollection, _h_muons);

            foreach(const Particle& _p, MuonCollection.particlesByPt()) {
                if (_p.momentum().pT() > 0 && fabs(_p.momentum().eta()) < 4.5) {
                    GoodMuons.push_back(_p);
                }
            }

            vector<Particle> GoodLeptons = GoodElectrons + GoodMuons;
            Particle temp;
            for (size_t kk = 0; kk < GoodLeptons.size(); ++kk) {
                for (size_t mm = kk + 1; mm < GoodLeptons.size(); ++mm) {
                    if (GoodLeptons[mm].momentum().pT() > GoodLeptons[kk].momentum().pT()) {
                        temp = GoodLeptons[kk];
                        GoodLeptons[kk] = GoodLeptons[mm];
                        GoodLeptons[mm] = temp;
                    }
                }
            }

            const FinalState& NeutrinoCollection = applyProjection<FinalState>(event, "neutrinos");
            vector<Particle> GoodNeutrinos; //= fillPropertiesFromCollection(NeutrinoCollection, _h_neutrinos);

            foreach(const Particle& _p, NeutrinoCollection.particlesByPt()) {
                if (_p.momentum().pT() > 0 && fabs(_p.momentum().eta()) < 4.5) {
                    GoodNeutrinos.push_back(_p);
                }
            }

            // fill MET
            FourMomentum PMissEt(0., 0., 0., 0.);

            foreach(const Particle& _p, NeutrinoCollection.particlesByPt()) {
                PMissEt = PMissEt + _p.momentum();
            }

            // fill jets collection (eta cut, pt cut)
            const FastJets& JetCollection = applyProjection<FastJets>(event, "jets");

            // fill all jets with pT > 7 GeV (ATLAS standard jet collection)
            Jets AllJets = fillPropertiesFromCollection(JetCollection, 4.5, 7.0);
            Jets GoodJets;

            foreach(const Jet& _jet, AllJets) {
                if (_jet.momentum().pT() > 25) {
                    GoodJets.push_back(_jet);
                }
            }

            // fill B-hadrons collection (pt cut)
            vector<HepMC::GenParticle*> B_hadrons = fillBHadrons(event, 4.5, 5.0);

            // fill ligh-jets (select b-jets as those containing a b-hadron) (deltaR cut)
            Jets GoodBJets; //= fillJetsFlavours(good_jets, all_jets, B_hadrons, 0.3);
            //Jets GoodLightJets;

            foreach(const Jet& _j, GoodJets) {
                bool isbJet = false;

                foreach(HepMC::GenParticle* _b, B_hadrons) {
                    const FourMomentum hadron = _b->momentum();
                    const double hadron_jet_dR = deltaR(_j.momentum(), hadron);
                    if (hadron_jet_dR < 0.3) {
                        isbJet = true;
                        break;
                    }
                }

                // Check if the given jet is overlapped to any other jet
                bool isOverlapped = false;

                foreach(const Jet& _k, AllJets) {
                    if (_j.momentum() == _k.momentum()) continue;
                    double dRjj = deltaR(_j.momentum(), _k.momentum());
                    if (dRjj < 0.8) {
                        isOverlapped = true;
                        break;
                    }
                }
                if (isbJet && !isOverlapped) {
                    GoodBJets.push_back(_j);
                } //else if (!isbJet && !isOverlapped) {
                // GoodLightJets.push_back(_j);
                //}
            }

            _h_electron_N->fill(GoodElectrons.size(), _weight);
            _h_muon_N->fill(GoodMuons.size(), _weight);
            _h_lepton_N->fill(GoodLeptons.size(), _weight);
            _h_jet_N->fill(GoodJets.size(), _weight);
            _h_bjet_N->fill(GoodBJets.size(), _weight);
            if (TopChannel == "Leptonic") {
                _h_MET->fill(PMissEt.pT(), _weight);
                _h_MET_phi->fill(PMissEt.phi(), _weight);
            }
            //preselection
            if (GoodLeptons.empty())vetoEvent; //we have always at least 1 lepton
            if (GoodJets.size() < 3)vetoEvent; //we have always at least 3 jets
            if (GoodBJets.empty())vetoEvent; //we have always 1 bjets or more

            //After this point, we need to select the right channel

            if (ZChannel == "Hadronic" && TopChannel == "Leptonic") {
                //extra selection
                if (GoodLeptons.size() != 1)vetoEvent;
                if (GoodNeutrinos.empty())vetoEvent;
                if (GoodJets.size() < 4)vetoEvent;

                _h_lepton_1_pt->fill(GoodLeptons[0].momentum().pT(), _weight);
                _h_lepton_1_eta->fill(GoodLeptons[0].momentum().eta(), _weight);
                _h_lepton_1_phi->fill(GoodLeptons[0].momentum().phi(), _weight);

                if (GoodElectrons.size() == 1) {
                    _h_electron_1_pt->fill(GoodElectrons[0].momentum().pT(), _weight);
                    _h_electron_1_eta->fill(GoodElectrons[0].momentum().eta(), _weight);
                    _h_electron_1_phi->fill(GoodElectrons[0].momentum().phi(), _weight);
                } else {
                    _h_muon_1_pt->fill(GoodMuons[0].momentum().pT(), _weight);
                    _h_muon_1_eta->fill(GoodMuons[0].momentum().eta(), _weight);
                    _h_muon_1_phi->fill(GoodMuons[0].momentum().phi(), _weight);
                }

                _h_jet_1_pt->fill(GoodJets[0].momentum().pT(), _weight);
                _h_jet_1_eta->fill(GoodJets[0].momentum().eta(), _weight);
                _h_jet_1_phi->fill(GoodJets[0].momentum().phi(), _weight);
                _h_jet_1_mass ->fill(GoodJets[0].momentum().mass(), _weight);

                _h_jet_2_pt->fill(GoodJets[1].momentum().pT(), _weight);
                _h_jet_2_eta->fill(GoodJets[1].momentum().eta(), _weight);
                _h_jet_2_phi->fill(GoodJets[1].momentum().phi(), _weight);
                _h_jet_2_mass ->fill(GoodJets[1].momentum().mass(), _weight);

                _h_jet_3_pt->fill(GoodJets[2].momentum().pT(), _weight);
                _h_jet_3_eta->fill(GoodJets[2].momentum().eta(), _weight);
                _h_jet_3_phi->fill(GoodJets[2].momentum().phi(), _weight);
                _h_jet_3_mass ->fill(GoodJets[2].momentum().mass(), _weight);

                _h_jet_4_pt->fill(GoodJets[3].momentum().pT(), _weight);
                _h_jet_4_eta->fill(GoodJets[3].momentum().eta(), _weight);
                _h_jet_4_phi->fill(GoodJets[3].momentum().phi(), _weight);
                _h_jet_4_mass ->fill(GoodJets[3].momentum().mass(), _weight);

                _h_bjet_1_pt->fill(GoodBJets[0].momentum().pT(), _weight);
                _h_bjet_1_eta->fill(GoodBJets[0].momentum().eta(), _weight);
                _h_bjet_1_phi->fill(GoodBJets[0].momentum().phi(), _weight);
                _h_bjet_1_mass->fill(GoodBJets[0].momentum().mass(), _weight);

                if (GoodBJets.size() > 1) {
                    _h_bjet_2_pt->fill(GoodBJets[1].momentum().pT(), _weight);
                    _h_bjet_2_eta->fill(GoodBJets[1].momentum().eta(), _weight);
                    _h_bjet_2_phi->fill(GoodBJets[1].momentum().phi(), _weight);
                    _h_bjet_2_mass->fill(GoodBJets[1].momentum().mass(), _weight);
                }
                if (GoodBJets.size() > 2) {
                    _h_bjet_3_pt->fill(GoodBJets[2].momentum().pT(), _weight);
                    _h_bjet_3_eta->fill(GoodBJets[2].momentum().eta(), _weight);
                    _h_bjet_3_phi->fill(GoodBJets[2].momentum().phi(), _weight);
                    _h_bjet_3_mass->fill(GoodBJets[2].momentum().mass(), _weight);
                }
                if (GoodBJets.size() > 3) {
                    _h_bjet_4_pt->fill(GoodBJets[3].momentum().pT(), _weight);
                    _h_bjet_4_eta->fill(GoodBJets[3].momentum().eta(), _weight);
                    _h_bjet_4_phi->fill(GoodBJets[3].momentum().phi(), _weight);
                    _h_bjet_4_mass->fill(GoodBJets[3].momentum().mass(), _weight);
                }

                vector<Particle> WComponent = fillLeptonicWboson(GoodLeptons, GoodNeutrinos);
                FourMomentum _lepton_selected = WComponent[0].momentum();
                FourMomentum _neutrino_selected = WComponent[1].momentum();
                _lepton_from_W_boson = _lepton_selected;

                FourMomentum _Wboson_selected = _lepton_selected + _neutrino_selected;
                Particle _Wboson(24, _Wboson_selected);

                _h_W_pt->fill(_Wboson_selected.pT(), _weight);
                _h_W_eta->fill(_Wboson_selected.eta(), _weight);
                _h_W_phi->fill(_Wboson_selected.phi(), _weight);
                _h_W_m->fill(_Wboson_selected.mass(), _weight);
                float Wmet = MWT(_lepton_selected, _neutrino_selected);
                _h_W_mt->fill(Wmet, _weight);

                Particle Top_b_Component = fillTopQuark(_Wboson, GoodBJets);
                FourMomentum _Topquark_selected = Top_b_Component.momentum() + _Wboson_selected;
                Particle TopQuark(6, _Topquark_selected);

                _h_top_pt->fill(_Topquark_selected.pT(), _weight);
                _h_top_eta->fill(_Topquark_selected.eta(), _weight);
                _h_top_phi->fill(_Topquark_selected.phi(), _weight);
                _h_top_m->fill(_Topquark_selected.mass(), _weight);

                float Helocity = FillCosThetaStar(_Topquark_selected, _Wboson_selected, _lepton_from_W_boson);

                _h_cosTheta->fill(Helocity, _weight);

                Jets GoodJetsExcludingjetsfromtop;

                foreach(const Jet& _jet, AllJets) {
                    if (_jet.momentum() == Top_b_Component.momentum()) continue;
                    GoodJetsExcludingjetsfromtop.push_back(_jet);
                }

                for (size_t k = 0; k < GoodJetsExcludingjetsfromtop.size(); k++) {
                    for (size_t l = k + 1; l < GoodJetsExcludingjetsfromtop.size(); l++) {
                        FourMomentum Zboson = GoodJetsExcludingjetsfromtop[k].momentum() + GoodJetsExcludingjetsfromtop[l].momentum();
                        _h_Z_m_allcombination->fill(Zboson.mass(), _weight);
                    }
                }

                Particle ZWboson = fillHadronicZboson(GoodJets);
                FourMomentum _Zboson_selected = ZWboson.momentum();
                _h_Z_pt->fill(_Zboson_selected.pT(), _weight);
                _h_Z_eta->fill(_Zboson_selected.eta(), _weight);
                _h_Z_phi->fill(_Zboson_selected.phi(), _weight);
                _h_Z_m->fill(_Zboson_selected.mass(), _weight);

            } else if (ZChannel == "Leptonic" && TopChannel == "Leptonic") {
                if (GoodLeptons.size() != 3)vetoEvent;
                if (GoodNeutrinos.empty())vetoEvent;
                if (GoodJets.size() != 3)vetoEvent;

                _h_lepton_1_pt->fill(GoodLeptons[0].momentum().pT(), _weight);
                _h_lepton_1_eta->fill(GoodLeptons[0].momentum().eta(), _weight);
                _h_lepton_1_phi->fill(GoodLeptons[0].momentum().phi(), _weight);

                _h_lepton_2_pt->fill(GoodLeptons[1].momentum().pT(), _weight);
                _h_lepton_2_eta->fill(GoodLeptons[1].momentum().eta(), _weight);
                _h_lepton_2_phi->fill(GoodLeptons[1].momentum().phi(), _weight);

                _h_lepton_3_pt->fill(GoodLeptons[2].momentum().pT(), _weight);
                _h_lepton_3_eta->fill(GoodLeptons[2].momentum().eta(), _weight);
                _h_lepton_3_phi->fill(GoodLeptons[2].momentum().phi(), _weight);

                if (!GoodElectrons.empty()) {
                    _h_electron_1_pt->fill(GoodElectrons[0].momentum().pT(), _weight);
                    _h_electron_1_eta->fill(GoodElectrons[0].momentum().eta(), _weight);
                    _h_electron_1_phi->fill(GoodElectrons[0].momentum().phi(), _weight);

                }
                if (GoodElectrons.size() > 1) {
                    _h_electron_2_pt->fill(GoodElectrons[1].momentum().pT(), _weight);
                    _h_electron_2_eta->fill(GoodElectrons[1].momentum().eta(), _weight);
                    _h_electron_2_phi->fill(GoodElectrons[1].momentum().phi(), _weight);
                }
                if (GoodElectrons.size() > 2) {
                    _h_electron_3_pt->fill(GoodElectrons[2].momentum().pT(), _weight);
                    _h_electron_3_eta->fill(GoodElectrons[2].momentum().eta(), _weight);
                    _h_electron_3_phi->fill(GoodElectrons[2].momentum().phi(), _weight);
                }
                if (!GoodMuons.empty()) {
                    _h_muon_1_pt->fill(GoodMuons[0].momentum().pT(), _weight);
                    _h_muon_1_eta->fill(GoodMuons[0].momentum().eta(), _weight);
                    _h_muon_1_phi->fill(GoodMuons[0].momentum().phi(), _weight);

                }
                if (GoodMuons.size() > 1) {
                    _h_muon_2_pt->fill(GoodMuons[1].momentum().pT(), _weight);
                    _h_muon_2_eta->fill(GoodMuons[1].momentum().eta(), _weight);
                    _h_muon_2_phi->fill(GoodMuons[1].momentum().phi(), _weight);
                }
                if (GoodMuons.size() > 2) {
                    _h_muon_3_pt->fill(GoodMuons[2].momentum().pT(), _weight);
                    _h_muon_3_eta->fill(GoodMuons[2].momentum().eta(), _weight);
                    _h_muon_3_phi->fill(GoodMuons[2].momentum().phi(), _weight);
                }

                _h_jet_1_pt->fill(GoodJets[0].momentum().pT(), _weight);
                _h_jet_1_eta->fill(GoodJets[0].momentum().eta(), _weight);
                _h_jet_1_phi->fill(GoodJets[0].momentum().phi(), _weight);
                _h_jet_1_mass ->fill(GoodJets[0].momentum().mass(), _weight);

                _h_jet_2_pt->fill(GoodJets[1].momentum().pT(), _weight);
                _h_jet_2_eta->fill(GoodJets[1].momentum().eta(), _weight);
                _h_jet_2_phi->fill(GoodJets[1].momentum().phi(), _weight);
                _h_jet_2_mass ->fill(GoodJets[1].momentum().mass(), _weight);

                _h_jet_3_pt->fill(GoodJets[2].momentum().pT(), _weight);
                _h_jet_3_eta->fill(GoodJets[2].momentum().eta(), _weight);
                _h_jet_3_phi->fill(GoodJets[2].momentum().phi(), _weight);
                _h_jet_3_mass ->fill(GoodJets[2].momentum().mass(), _weight);

                _h_bjet_1_pt->fill(GoodBJets[0].momentum().pT(), _weight);
                _h_bjet_1_eta->fill(GoodBJets[0].momentum().eta(), _weight);
                _h_bjet_1_phi->fill(GoodBJets[0].momentum().phi(), _weight);
                _h_bjet_1_mass->fill(GoodBJets[0].momentum().mass(), _weight);

                if (GoodBJets.size() > 1) {
                    _h_bjet_2_pt->fill(GoodBJets[1].momentum().pT(), _weight);
                    _h_bjet_2_eta->fill(GoodBJets[1].momentum().eta(), _weight);
                    _h_bjet_2_phi->fill(GoodBJets[1].momentum().phi(), _weight);
                    _h_bjet_2_mass->fill(GoodBJets[1].momentum().mass(), _weight);

                }
                if (GoodBJets.size() > 2) {
                    _h_bjet_3_pt->fill(GoodBJets[2].momentum().pT(), _weight);
                    _h_bjet_3_eta->fill(GoodBJets[2].momentum().eta(), _weight);
                    _h_bjet_3_phi->fill(GoodBJets[2].momentum().phi(), _weight);
                    _h_bjet_3_mass->fill(GoodBJets[2].momentum().mass(), _weight);
                }

                vector<Particle> WComponent = fillLeptonicWboson(GoodLeptons, GoodNeutrinos);
                FourMomentum _lepton_selected = WComponent[0].momentum();
                FourMomentum _neutrino_selected = WComponent[1].momentum();
                _lepton_from_W_boson = _lepton_selected;

                FourMomentum _Wboson_selected = _lepton_selected + _neutrino_selected;
                Particle _Wboson(24, _Wboson_selected);

                _h_W_pt->fill(_Wboson_selected.pT(), _weight);
                _h_W_eta->fill(_Wboson_selected.eta(), _weight);
                _h_W_phi->fill(_Wboson_selected.phi(), _weight);
                _h_W_m->fill(_Wboson_selected.mass(), _weight);
                float Wmet = MWT(_lepton_selected, _neutrino_selected);
                _h_W_mt->fill(Wmet, _weight);

                Particle Top_b_Component = fillTopQuark(_Wboson, GoodBJets);
                FourMomentum _Topquark_selected = Top_b_Component.momentum() + _Wboson_selected;
                Particle TopQuark(6, _Topquark_selected);

                _h_top_pt->fill(_Topquark_selected.pT(), _weight);
                _h_top_eta->fill(_Topquark_selected.eta(), _weight);
                _h_top_phi->fill(_Topquark_selected.phi(), _weight);
                _h_top_m->fill(_Topquark_selected.mass(), _weight);

                float Helocity = FillCosThetaStar(_Topquark_selected, _Wboson_selected, _lepton_from_W_boson);
                _h_cosTheta->fill(Helocity, _weight);

                Particle ZWboson = fillLeptonicZboson(GoodLeptons);
                FourMomentum _Zboson_selected = ZWboson.momentum();
                _h_Z_pt->fill(_Zboson_selected.pT(), _weight);
                _h_Z_eta->fill(_Zboson_selected.eta(), _weight);
                _h_Z_phi->fill(_Zboson_selected.phi(), _weight);
                _h_Z_m->fill(_Zboson_selected.mass(), _weight);

            } else if (ZChannel == "Leptonic" && TopChannel == "Hadronic") {

                if (GoodMuons.size() != 2 && GoodElectrons.size() != 2)vetoEvent;
                if (GoodJets.size() != 5)vetoEvent;

                _h_lepton_1_pt->fill(GoodLeptons[0].momentum().pT(), _weight);
                _h_lepton_1_eta->fill(GoodLeptons[0].momentum().eta(), _weight);
                _h_lepton_1_phi->fill(GoodLeptons[0].momentum().phi(), _weight);

                _h_lepton_2_pt->fill(GoodLeptons[1].momentum().pT(), _weight);
                _h_lepton_2_eta->fill(GoodLeptons[1].momentum().eta(), _weight);
                _h_lepton_2_phi->fill(GoodLeptons[1].momentum().phi(), _weight);

                //this because I am sure that we have either two electrons or two muons, this comes from the event selection
                if (!GoodElectrons.empty()) {
                    _h_electron_1_pt->fill(GoodElectrons[0].momentum().pT(), _weight);
                    _h_electron_1_eta->fill(GoodElectrons[0].momentum().eta(), _weight);
                    _h_electron_1_phi->fill(GoodElectrons[0].momentum().phi(), _weight);

                    _h_electron_2_pt->fill(GoodElectrons[1].momentum().pT(), _weight);
                    _h_electron_2_eta->fill(GoodElectrons[1].momentum().eta(), _weight);
                    _h_electron_2_phi->fill(GoodElectrons[1].momentum().phi(), _weight);
                } else {
                    _h_muon_1_pt->fill(GoodMuons[0].momentum().pT(), _weight);
                    _h_muon_1_eta->fill(GoodMuons[0].momentum().eta(), _weight);
                    _h_muon_1_phi->fill(GoodMuons[0].momentum().phi(), _weight);

                    _h_muon_2_pt->fill(GoodMuons[1].momentum().pT(), _weight);
                    _h_muon_2_eta->fill(GoodMuons[1].momentum().eta(), _weight);
                    _h_muon_2_phi->fill(GoodMuons[1].momentum().phi(), _weight);
                }

                _h_jet_1_pt->fill(GoodJets[0].momentum().pT(), _weight);
                _h_jet_1_eta->fill(GoodJets[0].momentum().eta(), _weight);
                _h_jet_1_phi->fill(GoodJets[0].momentum().phi(), _weight);
                _h_jet_1_mass ->fill(GoodJets[0].momentum().mass(), _weight);

                _h_jet_2_pt->fill(GoodJets[1].momentum().pT(), _weight);
                _h_jet_2_eta->fill(GoodJets[1].momentum().eta(), _weight);
                _h_jet_2_phi->fill(GoodJets[1].momentum().phi(), _weight);
                _h_jet_2_mass ->fill(GoodJets[1].momentum().mass(), _weight);

                _h_jet_3_pt->fill(GoodJets[2].momentum().pT(), _weight);
                _h_jet_3_eta->fill(GoodJets[2].momentum().eta(), _weight);
                _h_jet_3_phi->fill(GoodJets[2].momentum().phi(), _weight);
                _h_jet_3_mass ->fill(GoodJets[2].momentum().mass(), _weight);

                _h_jet_4_pt->fill(GoodJets[3].momentum().pT(), _weight);
                _h_jet_4_eta->fill(GoodJets[3].momentum().eta(), _weight);
                _h_jet_4_phi->fill(GoodJets[3].momentum().phi(), _weight);
                _h_jet_4_mass ->fill(GoodJets[3].momentum().mass(), _weight);

                _h_bjet_1_pt->fill(GoodBJets[0].momentum().pT(), _weight);
                _h_bjet_1_eta->fill(GoodBJets[0].momentum().eta(), _weight);
                _h_bjet_1_phi->fill(GoodBJets[0].momentum().phi(), _weight);
                _h_bjet_1_mass->fill(GoodBJets[0].momentum().mass(), _weight);

                if (GoodBJets.size() > 1) {
                    _h_bjet_2_pt->fill(GoodBJets[1].momentum().pT(), _weight);
                    _h_bjet_2_eta->fill(GoodBJets[1].momentum().eta(), _weight);
                    _h_bjet_2_phi->fill(GoodBJets[1].momentum().phi(), _weight);
                    _h_bjet_2_mass->fill(GoodBJets[1].momentum().mass(), _weight);
                }

                Particle Wboson = fillHadronicWboson(GoodJets);
                FourMomentum _Wboson_selected = Wboson.momentum();

                _h_W_pt->fill(_Wboson_selected.pT(), _weight);
                _h_W_eta->fill(_Wboson_selected.eta(), _weight);
                _h_W_phi->fill(_Wboson_selected.phi(), _weight);
                _h_W_m->fill(_Wboson_selected.mass(), _weight);

                Particle Top_b_Component = fillTopQuark(Wboson, GoodBJets);
                FourMomentum _Topquark_selected = Top_b_Component.momentum() + _Wboson_selected;
                cout << _Topquark_selected.pT() << endl;
                Particle TopQuark(6, _Topquark_selected);

                _h_top_pt->fill(_Topquark_selected.pT(), _weight);
                _h_top_eta->fill(_Topquark_selected.eta(), _weight);
                _h_top_phi->fill(_Topquark_selected.phi(), _weight);
                _h_top_m->fill(_Topquark_selected.mass(), _weight);

                Particle ZWboson = fillLeptonicZboson(GoodLeptons);
                FourMomentum _Zboson_selected = ZWboson.momentum();
                _h_Z_pt->fill(_Zboson_selected.pT(), _weight);
                _h_Z_eta->fill(_Zboson_selected.eta(), _weight);
                _h_Z_phi->fill(_Zboson_selected.phi(), _weight);
                _h_Z_m->fill(_Zboson_selected.mass(), _weight);

            } else if (ZChannel == "Invesible" && TopChannel == "Leptonic") {
                if (GoodLeptons.size() != 1)vetoEvent;
                if (GoodNeutrinos.size() != 3)vetoEvent;
                if (GoodJets.size() != 3)vetoEvent;

                _h_lepton_1_pt->fill(GoodLeptons[0].momentum().pT(), _weight);
                _h_lepton_1_eta->fill(GoodLeptons[0].momentum().eta(), _weight);
                _h_lepton_1_phi->fill(GoodLeptons[0].momentum().phi(), _weight);

                if (GoodElectrons.size() == 1) {
                    _h_electron_1_pt->fill(GoodElectrons[0].momentum().pT(), _weight);
                    _h_electron_1_eta->fill(GoodElectrons[0].momentum().eta(), _weight);
                    _h_electron_1_phi->fill(GoodElectrons[0].momentum().phi(), _weight);
                } else {
                    _h_muon_1_pt->fill(GoodMuons[0].momentum().pT(), _weight);
                    _h_muon_1_eta->fill(GoodMuons[0].momentum().eta(), _weight);
                    _h_muon_1_phi->fill(GoodMuons[0].momentum().phi(), _weight);
                }

                _h_jet_1_pt->fill(GoodJets[0].momentum().pT(), _weight);
                _h_jet_1_eta->fill(GoodJets[0].momentum().eta(), _weight);
                _h_jet_1_phi->fill(GoodJets[0].momentum().phi(), _weight);
                _h_jet_1_mass ->fill(GoodJets[0].momentum().mass(), _weight);

                _h_jet_2_pt->fill(GoodJets[1].momentum().pT(), _weight);
                _h_jet_2_eta->fill(GoodJets[1].momentum().eta(), _weight);
                _h_jet_2_phi->fill(GoodJets[1].momentum().phi(), _weight);
                _h_jet_2_mass ->fill(GoodJets[1].momentum().mass(), _weight);

                _h_jet_3_pt->fill(GoodJets[2].momentum().pT(), _weight);
                _h_jet_3_eta->fill(GoodJets[2].momentum().eta(), _weight);
                _h_jet_3_phi->fill(GoodJets[2].momentum().phi(), _weight);
                _h_jet_3_mass ->fill(GoodJets[2].momentum().mass(), _weight);

                _h_bjet_1_pt->fill(GoodBJets[0].momentum().pT(), _weight);
                _h_bjet_1_eta->fill(GoodBJets[0].momentum().eta(), _weight);
                _h_bjet_1_phi->fill(GoodBJets[0].momentum().phi(), _weight);
                _h_bjet_1_mass->fill(GoodBJets[0].momentum().mass(), _weight);

                if (GoodBJets.size() > 1) {
                    _h_bjet_2_pt->fill(GoodBJets[1].momentum().pT(), _weight);
                    _h_bjet_2_eta->fill(GoodBJets[1].momentum().eta(), _weight);
                    _h_bjet_2_phi->fill(GoodBJets[1].momentum().phi(), _weight);
                    _h_bjet_2_mass->fill(GoodBJets[1].momentum().mass(), _weight);
                }
                if (GoodBJets.size() > 2) {
                    _h_bjet_3_pt->fill(GoodBJets[2].momentum().pT(), _weight);
                    _h_bjet_3_eta->fill(GoodBJets[2].momentum().eta(), _weight);
                    _h_bjet_3_phi->fill(GoodBJets[2].momentum().phi(), _weight);
                    _h_bjet_3_mass->fill(GoodBJets[2].momentum().mass(), _weight);
                }

                vector<Particle> WComponent = fillLeptonicWboson(GoodLeptons, GoodNeutrinos);
                FourMomentum _lepton_selected = WComponent[0].momentum();
                FourMomentum _neutrino_selected = WComponent[1].momentum();
                _lepton_from_W_boson = _lepton_selected;

                FourMomentum _Wboson_selected = _lepton_selected + _neutrino_selected;
                Particle _Wboson(24, _Wboson_selected);

                _h_W_pt->fill(_Wboson_selected.pT(), _weight);
                _h_W_eta->fill(_Wboson_selected.eta(), _weight);
                _h_W_phi->fill(_Wboson_selected.phi(), _weight);
                _h_W_m->fill(_Wboson_selected.mass(), _weight);
                float Wmet = MWT(_lepton_selected, _neutrino_selected);
                _h_W_mt->fill(Wmet, _weight);

                Particle Top_b_Component = fillTopQuark(_Wboson, GoodBJets);
                FourMomentum _Topquark_selected = Top_b_Component.momentum() + _Wboson_selected;
                Particle TopQuark(6, _Topquark_selected);

                _h_top_pt->fill(_Topquark_selected.pT(), _weight);
                _h_top_eta->fill(_Topquark_selected.eta(), _weight);
                _h_top_phi->fill(_Topquark_selected.phi(), _weight);
                _h_top_m->fill(_Topquark_selected.mass(), _weight);

                float Helocity = FillCosThetaStar(_Topquark_selected, _Wboson_selected, _lepton_from_W_boson);
                _h_cosTheta->fill(Helocity, _weight);
            }
            //MSG_INFO("");
        } // end of analyze()

        //=====================================================================

        float MWT(FourMomentum lepton, FourMomentum neutrino) {
            return sqrt(2 * lepton.pT() * neutrino.pT() * (1 - cos(lepton.phi() - neutrino.phi())));
        }

        float FillCosThetaStar(FourMomentum top, FourMomentum W, FourMomentum lep) {
            // need to go into the W rest frame here

            LorentzTransform boostTrafo;
            boostTrafo.setBoost(-W.boostVector());

            // const FourMomentum boostW = boostTrafo.transform(W);
            //PrintFourMomentum(0,   "boosteW",   boostW);
            const FourMomentum boostTop = boostTrafo.transform(top);
            //PrintFourMomentum(0,  "BoostedTop", boostTop);
            const FourMomentum boostLep = boostTrafo.transform(lep);

            return cos(boostLep.angle(-boostTop));
        }

        //=====================================================================
        // fill properties from Collection - FastJets
        //=====================================================================

        Jets fillPropertiesFromCollection(const FastJets& Collection, float etaCut, float ptCut) {
            Jets _jets;

            foreach(const Jet& _jet, Collection.jetsByPt(ptCut * GeV)) {
                if (fabs(_jet.eta()) >= etaCut) {
                    continue;
                }
                _jets.push_back(_jet);
            }
            return _jets;
        }

        //=====================================================================
        // fill B-Hadrons
        //=====================================================================

        vector<HepMC::GenParticle*> fillBHadrons(const Event& event, float etaCut = 4.5, float ptCut = 5.0) {
            vector<HepMC::GenParticle*> B_hadrons;
            // Get the B-Hadrons with pT > ptCut GeV, to add to the final-state particles used for jet clustering.
            vector<HepMC::GenParticle*> allParticles = particles(event.genEvent());
            for (size_t i = 0; i < allParticles.size(); i++) {
                HepMC::GenParticle* _p = allParticles.at(i);
                // If the particle is a B-hadron and has pT > ptCut GeV, add it to the B-hadrons vector
                if (PID::isHadron(_p->pdg_id()) && PID::hasBottom(_p->pdg_id()) && fabs(_p->momentum().eta()) < etaCut && _p->momentum().perp() > ptCut) {
                    B_hadrons.push_back(_p);
                }
            }
            return B_hadrons;
        }

        //=====================================================================
        // fill jets flavours
        //=====================================================================

        std::vector<Jets> fillJetsFlavours(Jets _good_jets, Jets _all_jets, vector<HepMC::GenParticle*> _B_hadrons, float dRCut) {
            Jets _l_jets, _b_jets;

            int indexLJet = 0;
            int indexBJet = 0;

            foreach(const Jet& _j, _good_jets) {
                bool isbJet = false;

                foreach(HepMC::GenParticle* _b, _B_hadrons) {
                    /// @todo Use direct momentum accessor / delta functions
                    const FourMomentum hadron = _b->momentum();
                    const double hadron_jet_dR = deltaR(_j.momentum(), hadron);
                    if (hadron_jet_dR < dRCut) {
                        isbJet = true;
                        break;
                    }
                }

                // Check if the given jet is overlapped to any other jet
                bool isOverlapped = false;

                foreach(const Jet& _k, _all_jets) {
                    if (_j.momentum() == _k.momentum()) continue;
                    double dRjj = deltaR(_j.momentum(), _k.momentum());
                    if (dRjj < 0.8) {
                        isOverlapped = true;
                        break;
                    }
                }
                if (isbJet && !isOverlapped) {
                    _b_jets.push_back(_j);
                    //FillFourMomentum(_j, _h_bjets, 1, true);
                    // PrintFourMomentum(indexBJet, "b-jet", _j.momentum());
                    indexBJet++;
                } else if (!isbJet && !isOverlapped) {
                    _l_jets.push_back(_j);
                    //FillFourMomentum(_j, _h_ljets, 1, true);
                    // PrintFourMomentum(indexLJet, "light jet", _j.momentum());
                    indexLJet++;
                }
            }

            std::vector<Jets> _jetsVector;
            _jetsVector.clear();
            _jetsVector.push_back(_l_jets);
            _jetsVector.push_back(_b_jets);

            return _jetsVector;
        }

        //=====================================================================
        // fill W boson
        //=====================================================================

        vector<Particle> fillLeptonicWboson(vector<Particle> Particle1, vector<Particle> Particle2) {

            float Wboson_mass = 80.399 * GeV; // in GeV
            float massdif = 1e9;

            int indx[2] = {0};
            for (size_t k = 0; k < Particle1.size(); k++) {
                for (size_t l = 0; l < Particle2.size(); l++) {
                    FourMomentum _Wboson_candidate = Particle1[k].momentum() + Particle2[l].momentum();
                    float dif = fabs(_Wboson_candidate.mass() - Wboson_mass);
                    if (dif < massdif) {
                        indx[0] = k;
                        indx[1] = l;
                        massdif = dif;
                    }
                }
            }

            vector<Particle> WComponent(2);
            WComponent.at(0) = Particle1[indx[0]];
            WComponent.at(1) = Particle2[indx[1]];
            return WComponent;
        }

        Particle fillHadronicWboson(Jets _jets) {
            FourMomentum _Wboson_selected(0, 0, 0, 0);
            float Wboson_mass = 80.399 * GeV; // in GeV
            float massdif = 1e9;
            int indx[2] = {0};
            for (size_t k = 0; k < _jets.size(); k++) {
                for (size_t l = k + 1; l < _jets.size(); l++) {
                    FourMomentum _Wboson_candidate = _jets[k].momentum() + _jets[l].momentum();
                    float dif = fabs(_Wboson_candidate.mass() - Wboson_mass);
                    if (dif < massdif) {
                        indx[0] = k;
                        indx[1] = l;
                        massdif = dif;
                    }
                }
            }

            FourMomentum _jet_selected_1 = _jets[indx[0]].momentum();
            FourMomentum _jet_selected_2 = _jets[indx[1]].momentum();

            _Wboson_selected = _jet_selected_1 + _jet_selected_2;
            Particle _Wboson(24, _Wboson_selected);
            return _Wboson;
        }

        Particle fillHadronicZboson(Jets _jets) {
            FourMomentum _Zboson_selected(0, 0, 0, 0);
            //float Zboson_mass = 91.1876 * GeV; // in GeV
            //float massdif = 1e9;
            float deltardif = 1e9;
            int indx[2] = {0};
            //            for (size_t k = 0; k < _jets.size(); k++) {
            //                if (_jets[k].momentum() == bjet.momentum())continue;
            //                for (size_t l = k + 1; l < _jets.size(); l++) {
            //                    if (_jets[l].momentum() == bjet.momentum())continue;
            //                    FourMomentum _Zboson_candidate = _jets[k].momentum() + _jets[l].momentum();
            //                    float dif = fabs(_Zboson_candidate.mass() - Zboson_mass);
            //                    if (dif < massdif) {
            //                        indx[0] = k;
            //                        indx[1] = l;
            //                        massdif = dif;
            //                    }
            //                }
            //            }

            for (size_t k = 0; k < _jets.size(); k++) {
                for (size_t l = k + 1; l < _jets.size(); l++) {
                    float dif = deltaR(_jets[k], _jets[l]);
                    if (dif < deltardif) {
                        indx[0] = k;
                        indx[1] = l;
                        deltardif = dif;
                    }
                }
            }

            FourMomentum _jet_selected_1 = _jets[indx[0]].momentum();
            FourMomentum _jet_selected_2 = _jets[indx[1]].momentum();

            _Zboson_selected = _jet_selected_1 + _jet_selected_2;
            Particle _Zboson(23, _Zboson_selected);
            return _Zboson;
        }

        Particle fillLeptonicZboson(vector<Particle> Leptons) {
            FourMomentum _Zboson_selected(0, 0, 0, 0);
            float Zboson_mass = 91.1876 * GeV; // in GeV
            float massdif = 1e9;

            int indx[2] = {0};
            for (size_t k = 0; k < Leptons.size(); k++) {
                for (size_t l = k + 1; l < Leptons.size(); l++) {
                    FourMomentum _Zboson_candidate = Leptons[k].momentum() + Leptons[l].momentum();
                    float dif = fabs(_Zboson_candidate.mass() - Zboson_mass);
                    if (dif < massdif) {
                        indx[0] = k;
                        indx[1] = l;
                        massdif = dif;
                    }
                }
            }

            FourMomentum _lepton_selected_1 = Leptons[indx[0]].momentum();
            FourMomentum _lepton_selected_2 = Leptons[indx[1]].momentum();

            _Zboson_selected = _lepton_selected_1 + _lepton_selected_2;
            Particle _Zboson(23, _Zboson_selected);
            return _Zboson;
        }

        //=====================================================================
        // fill top quark
        //=====================================================================

        Particle fillTopQuark(Particle _Wboson, Jets _bjets) {
            FourMomentum _bjet_selected(0, 0, 0, 0);
            FourMomentum _topquark_selected(0, 0, 0, 0);

            float topquark_mass = 172.5 * GeV;
            float massdif = 1e9;

            foreach(const Jet& _bjet, _bjets) {
                FourMomentum _topquark_candidate = _Wboson.momentum() + _bjet.momentum();
                float dif = fabs(_topquark_candidate.mass() - topquark_mass);
                if (dif < massdif) {
                    _bjet_selected = _bjet.momentum();
                    _topquark_selected = _topquark_candidate;
                    massdif = dif;
                }
            }

            Particle _bquark(5, _bjet_selected);

            return _bquark;
        }


        //=====================================================================
        // BookHistograms
        //=====================================================================

        void BookHistograms() {
            // mc weight
            _h_weight = bookHisto1D("weight", 11, -5.5, 5.5);

            _h_electron_N = bookHisto1D("electrons_N", 6, -0.5, 5.5);
            _h_muon_N = bookHisto1D("muons_N", 6, -0.5, 5.5);
            _h_lepton_N = bookHisto1D("leptons_N", 6, -0.5, 5.5);
            //In all cases we will have at least 1 lepton, so electron_1, muon_1 and lepton_1 will be declared always

            _h_electron_1_pt = bookHisto1D("electron_1_pt", 50, 0, 250);
            _h_electron_1_eta = bookHisto1D("electron_1_eta", 40, -5.0, 5.0);
            _h_electron_1_phi = bookHisto1D("electron_1_phi", 32, 0.0, twopi);

            _h_muon_1_pt = bookHisto1D("muon_1_pt", 50, 0, 250);
            _h_muon_1_eta = bookHisto1D("muon_1_eta", 40, -5.0, 5.0);
            _h_muon_1_phi = bookHisto1D("muon_1_phi", 32, 0.0, twopi);

            _h_lepton_1_pt = bookHisto1D("lepton_1_pt", 50, 0, 250);
            _h_lepton_1_eta = bookHisto1D("lepton_1_eta", 40, -5.0, 5.0);
            _h_lepton_1_phi = bookHisto1D("lepton_1_phi", 32, 0.0, twopi);

            //the following is valid only because when Z boson decays into leptons then no matter which top decay modes we have,
            //we will have at least 2 leptons

            if (ZChannel == "Leptonic") {
                _h_electron_2_pt = bookHisto1D("electron_2_pt", 50, 0, 250);
                _h_electron_2_eta = bookHisto1D("electron_2_eta", 40, -5.0, 5.0);
                _h_electron_2_phi = bookHisto1D("electron_2_phi", 32, 0.0, twopi);

                _h_muon_2_pt = bookHisto1D("muon_2_pt", 50, 0, 250);
                _h_muon_2_eta = bookHisto1D("muon_2_eta", 40, -5.0, 5.0);
                _h_muon_2_phi = bookHisto1D("muon_2_phi", 32, 0.0, twopi);

                _h_lepton_2_pt = bookHisto1D("lepton_2_pt", 50, 0, 250);
                _h_lepton_2_eta = bookHisto1D("lepton_2_eta", 40, -5.0, 5.0);
                _h_lepton_2_phi = bookHisto1D("lepton_2_phi", 32, 0.0, twopi);

                if (TopChannel == "Leptonic") {

                    _h_electron_3_pt = bookHisto1D("electron_3_pt", 50, 0, 250);
                    _h_electron_3_eta = bookHisto1D("electron_3_eta", 40, -5.0, 5.0);
                    _h_electron_3_phi = bookHisto1D("electron_3_phi", 32, 0.0, twopi);

                    _h_muon_3_pt = bookHisto1D("muon_3_pt", 50, 0, 250);
                    _h_muon_3_eta = bookHisto1D("muon_3_eta", 40, -5.0, 5.0);
                    _h_muon_3_phi = bookHisto1D("muon_3_phi", 32, 0.0, twopi);

                    _h_lepton_3_pt = bookHisto1D("lepton_3_pt", 50, 0, 250);
                    _h_lepton_3_eta = bookHisto1D("lepton_3_eta", 40, -5.0, 5.0);
                    _h_lepton_3_phi = bookHisto1D("lepton_3_phi", 32, 0.0, twopi);

                }
            }

            if (TopChannel == "Leptonic") {
                _h_cosTheta = bookHisto1D("CosThetaStar", 15, -1.0, 1.0);
                // MET
                _h_MET = bookHisto1D("MET", 50, 0, 250);
                _h_MET_phi = bookHisto1D("MET_phi", 32, 0.0, twopi);
            }

            _h_jet_N = bookHisto1D("jets_N", 9, -0.5, 8.5);

            _h_jet_1_pt = bookHisto1D("jet_1_pt", 50, 0, 250);
            _h_jet_1_eta = bookHisto1D("jet_1_eta", 40, -5.0, 5.0);
            _h_jet_1_phi = bookHisto1D("jet_1_phi", 32, 0.0, twopi);
            _h_jet_1_mass = bookHisto1D("jet_1_m", 25, 0, 50);

            _h_jet_2_pt = bookHisto1D("jet_2_pt", 50, 0, 250);
            _h_jet_2_eta = bookHisto1D("jet_2_eta", 40, -5.0, 5.0);
            _h_jet_2_phi = bookHisto1D("jet_2_phi", 32, 0.0, twopi);
            _h_jet_2_mass = bookHisto1D("jet_2_m", 25, 0, 50);

            _h_jet_3_pt = bookHisto1D("jet_3_pt", 50, 0, 250);
            _h_jet_3_eta = bookHisto1D("jet_3_eta", 40, -5.0, 5.0);
            _h_jet_3_phi = bookHisto1D("jet_3_phi", 32, 0.0, twopi);
            _h_jet_3_mass = bookHisto1D("jet_3_m", 25, 0, 50);

            if (ZChannel == "Hadronic" || TopChannel == "Hadronic") {
                _h_jet_4_pt = bookHisto1D("jet_4_pt", 50, 0, 250);
                _h_jet_4_eta = bookHisto1D("jet_4_eta", 40, -5.0, 5.0);
                _h_jet_4_phi = bookHisto1D("jet_4_phi", 32, 0.0, twopi);
                _h_jet_4_mass = bookHisto1D("jet_4_m", 25, 0, 50);
            }
            _h_bjet_N = bookHisto1D("bjets_N", 6, -0.5, 5.5);

            _h_bjet_1_pt = bookHisto1D("bjet_1_pt", 50, 0, 250);
            _h_bjet_1_eta = bookHisto1D("bjet_1_eta", 40, -5.0, 5.0);
            _h_bjet_1_phi = bookHisto1D("bjet_1_phi", 32, 0.0, twopi);
            _h_bjet_1_mass = bookHisto1D("bjet_1_m", 25, 0, 50);

            _h_bjet_2_pt = bookHisto1D("bjet_2_pt", 50, 0, 250);
            _h_bjet_2_eta = bookHisto1D("bjet_2_eta", 40, -5.0, 5.0);
            _h_bjet_2_phi = bookHisto1D("bjet_2_phi", 32, 0.0, twopi);
            _h_bjet_2_mass = bookHisto1D("bjet_2_m", 25, 0, 50);

            if (ZChannel == "Hadronic") {
                _h_bjet_3_pt = bookHisto1D("bjet_3_pt", 50, 0, 250);
                _h_bjet_3_eta = bookHisto1D("bjet_3_eta", 40, -5.0, 5.0);
                _h_bjet_3_phi = bookHisto1D("bjet_3_phi", 32, 0.0, twopi);
                _h_bjet_3_mass = bookHisto1D("bjet_3_m", 25, 0, 50);

                _h_bjet_4_pt = bookHisto1D("bjet_4_pt", 50, 0, 250);
                _h_bjet_4_eta = bookHisto1D("bjet_4_eta", 40, -5.0, 5.0);
                _h_bjet_4_phi = bookHisto1D("bjet_4_phi", 32, 0.0, twopi);
                _h_bjet_4_mass = bookHisto1D("bjet_4_m", 25, 0, 50);
            }

            _h_W_pt = bookHisto1D("W_pt", 50, 0, 250);
            _h_W_eta = bookHisto1D("W_eta", 40, -5.0, 5.0);
            _h_W_phi = bookHisto1D("W_phi", 32, 0.0, twopi);
            _h_W_m = bookHisto1D("W_m", 75, 0, 150);
            if (TopChannel == "Leptonic") {
                _h_W_mt = bookHisto1D("W_mt", 75, 0, 150);
            }

            _h_top_pt = bookHisto1D("top_pt", 50, 0, 250);
            _h_top_eta = bookHisto1D("top_eta", 40, -5.0, 5.0);
            _h_top_phi = bookHisto1D("top_phi", 32, 0.0, twopi);
            _h_top_m = bookHisto1D("top_m", 75, 0, 250);

            if (ZChannel != "Invesible") {
                _h_Z_pt = bookHisto1D("Z_pt", 50, 0, 250);
                _h_Z_eta = bookHisto1D("Z_eta", 40, -5.0, 5.0);
                _h_Z_phi = bookHisto1D("Z_phi", 32, 0.0, twopi);
                _h_Z_m = bookHisto1D("Z_m", 75, 0, 150);
                _h_Z_m_allcombination = bookHisto1D("Z_m_allcombination", 100, 0, 150);
            }
        }

        //@}

    private:
        std::string ZChannel;
        std::string TopChannel;

        void ReadSteeringFile(std::string inputFile) {
            std::ifstream f;
            string s;
            f.open(inputFile.c_str());

            if (!f.is_open()) {
                std::cout << "ERROR::Steering file is missing in run folder!!! ---> EXIT." << std::endl;
                exit(0);
            }

            //this to get the first line
            getline(f, s);
            //this to get the second line
            getline(f, s);
            std::stringstream iss(s);
            iss >> ZChannel;
            iss >> TopChannel;
            f.close();
        }

        vector<DressedLepton> _dressedelectrons;
        double _weight;

        FourMomentum _lepton_from_W_boson;

        /// @name Histograms
        //@{

        Histo1DPtr _h_weight;

        // Histo1DPtr _h_jet_HT;

        //Added by Muhammad Alhroob
        Histo1DPtr _h_electron_N;
        Histo1DPtr _h_electron_1_pt;
        Histo1DPtr _h_electron_1_eta;
        Histo1DPtr _h_electron_1_phi;

        Histo1DPtr _h_electron_2_pt;
        Histo1DPtr _h_electron_2_eta;
        Histo1DPtr _h_electron_2_phi;

        Histo1DPtr _h_electron_3_pt;
        Histo1DPtr _h_electron_3_eta;
        Histo1DPtr _h_electron_3_phi;

        Histo1DPtr _h_muon_N;
        Histo1DPtr _h_muon_1_pt;
        Histo1DPtr _h_muon_1_eta;
        Histo1DPtr _h_muon_1_phi;

        Histo1DPtr _h_muon_2_pt;
        Histo1DPtr _h_muon_2_eta;
        Histo1DPtr _h_muon_2_phi;

        Histo1DPtr _h_muon_3_pt;
        Histo1DPtr _h_muon_3_eta;
        Histo1DPtr _h_muon_3_phi;

        Histo1DPtr _h_lepton_N;
        Histo1DPtr _h_lepton_1_pt;
        Histo1DPtr _h_lepton_1_eta;
        Histo1DPtr _h_lepton_1_phi;

        Histo1DPtr _h_lepton_2_pt;
        Histo1DPtr _h_lepton_2_eta;
        Histo1DPtr _h_lepton_2_phi;

        Histo1DPtr _h_lepton_3_pt;
        Histo1DPtr _h_lepton_3_eta;
        Histo1DPtr _h_lepton_3_phi;

        Histo1DPtr _h_jet_N;
        Histo1DPtr _h_jet_1_pt;
        Histo1DPtr _h_jet_1_eta;
        Histo1DPtr _h_jet_1_phi;
        Histo1DPtr _h_jet_1_mass;

        Histo1DPtr _h_jet_2_pt;
        Histo1DPtr _h_jet_2_eta;
        Histo1DPtr _h_jet_2_phi;
        Histo1DPtr _h_jet_2_mass;

        Histo1DPtr _h_jet_3_pt;
        Histo1DPtr _h_jet_3_eta;
        Histo1DPtr _h_jet_3_phi;
        Histo1DPtr _h_jet_3_mass;

        Histo1DPtr _h_jet_4_pt;
        Histo1DPtr _h_jet_4_eta;
        Histo1DPtr _h_jet_4_phi;
        Histo1DPtr _h_jet_4_mass;

        Histo1DPtr _h_bjet_N;
        Histo1DPtr _h_bjet_1_pt;
        Histo1DPtr _h_bjet_1_eta;
        Histo1DPtr _h_bjet_1_phi;
        Histo1DPtr _h_bjet_1_mass;

        Histo1DPtr _h_bjet_2_pt;
        Histo1DPtr _h_bjet_2_eta;
        Histo1DPtr _h_bjet_2_phi;
        Histo1DPtr _h_bjet_2_mass;

        Histo1DPtr _h_bjet_3_pt;
        Histo1DPtr _h_bjet_3_eta;
        Histo1DPtr _h_bjet_3_phi;
        Histo1DPtr _h_bjet_3_mass;

        Histo1DPtr _h_bjet_4_pt;
        Histo1DPtr _h_bjet_4_eta;
        Histo1DPtr _h_bjet_4_phi;
        Histo1DPtr _h_bjet_4_mass;

        Histo1DPtr _h_W_pt;
        Histo1DPtr _h_W_eta;
        Histo1DPtr _h_W_phi;
        Histo1DPtr _h_W_m;
        Histo1DPtr _h_W_mt;

        Histo1DPtr _h_Z_pt;
        Histo1DPtr _h_Z_eta;
        Histo1DPtr _h_Z_phi;
        Histo1DPtr _h_Z_m;
        Histo1DPtr _h_Z_m_allcombination;

        Histo1DPtr _h_top_pt;
        Histo1DPtr _h_top_eta;
        Histo1DPtr _h_top_phi;
        Histo1DPtr _h_top_m;

        Histo1DPtr _h_cosTheta;
        Histo1DPtr _h_MET;
        Histo1DPtr _h_MET_phi;
    };

    // This global object acts as a hook for the plugin system
    AnalysisBuilder<MC_Zt_Truth> plugin_MC_Zt_Truth;

}
