// -*- C++ -*-
#include "Rivet/Analysis.hh"

namespace Rivet {


  class ATLAS_2014_I1304289 : public Analysis {
  public:

    /// Constructor
    ATLAS_2014_I1304289()
      : Analysis("ATLAS_2014_I1304289")
    {    }


  public:

    void init() {

      _h_t_tbar_pt = bookHisto1D(1,1,1);
      _h_ttbar_m = bookHisto1D(2,1,1);
      _h_ttbar_pt = bookHisto1D(3,1,1);
      _h_ttbar_rap = bookHisto1D(4,1,1);

    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const double weight = event.weight();

      // Vectors to hold any status=3 (anti-)top quarks (Pythia 6)
      vector<const GenParticle*> v_status3_top;
      vector<const GenParticle*> v_status3_antitop;

      // Vectors to hold any status=155 (anti-)top quarks (Herwig 6)
      vector<const GenParticle*> v_status155_top;
      vector<const GenParticle*> v_status155_antitop;

      // Vectors to hold any status=11 (anti-)top quarks for Herwig++  
      vector<const GenParticle*> v_status11_top;
      vector<const GenParticle*> v_status11_antitop;
      
      // Vectors to hold any status=22 (anti-)top quarks
      vector<const GenParticle*> v_statusOther_top;
      vector<const GenParticle*> v_statusOther_antitop;

      // Loop over all the HepMC GenParticles and keep the status 3 or status 155 tops.
      foreach (const GenParticle* p, Rivet::particles(event.genEvent())) {

        if(p->status()==3) {
          if(p->pdg_id()==6){ v_status3_top.push_back(p);}
          if(p->pdg_id()==-6){ v_status3_antitop.push_back(p);}
        }

        if(p->status() == 155) {
          if(p->pdg_id()==6) v_status155_top.push_back(p);
          if(p->pdg_id()==-6) v_status155_antitop.push_back(p);
        }


	// for Herwig++: take only the tops that decay into Wb!!!
	if(p->status() == 11){
	  
	  if(p->pdg_id()==6) {
	  
	    GenVertex *vertex = p -> end_vertex();
	    if(vertex -> particles_out_size() == 2)
	      v_status11_top.push_back(p);
	    
	  }

	  if(p->pdg_id()==-6) {

            GenVertex *vertex = p -> end_vertex();
            if(vertex -> particles_out_size() == 2)
              v_status11_antitop.push_back(p);

          }
	}
	
	// modif. Andrea: take only the last top for Py8 and Herwig++ !!!
	if(p->pdg_id()==6) {
	  v_statusOther_top.push_back(p);
	}
	if(p->pdg_id()==-6){	  
	  v_statusOther_antitop.push_back(p);
	}
	
      }
      
      //      std::cout << "\t" << std::endl;

      // If there are more than 1 status 3 tops or anti-tops, only keep the last one put into the vector
      if(v_status3_top.size()>1) {
        MSG_DEBUG("Found more than one status 3 top! Keeping only the last one.");
        v_status3_top = vector<const GenParticle*>(v_status3_top.end() - 1, v_status3_top.end());
      }

      if(v_status3_antitop.size()>1) {
        MSG_DEBUG("Found more than one status 3 antitop! Keeping only the last one");
        v_status3_antitop = vector<const GenParticle*>(v_status3_antitop.end() - 1, v_status3_antitop.end());
      }

      // If there are more than 1 status 11 tops or anti-tops, only keep the last one put into the vector                                                             
      if(v_status3_top.size()>1) {
        MSG_DEBUG("Found more than one status 11 top! Keeping only the last one.");
        v_status11_top = vector<const GenParticle*>(v_status11_top.end() - 1, v_status11_top.end());
      }

      if(v_status11_antitop.size()>1) {
        MSG_DEBUG("Found more than one status 11 antitop! Keeping only the last one");
        v_status11_antitop = vector<const GenParticle*>(v_status11_antitop.end() - 1, v_status11_antitop.end());
      }


      // =======================================
      // Rach: check for Pythia 8 as well
      // If there are more than 1 status 3 tops or anti-tops, only keep the last one put into the vector
      if(v_statusOther_top.size()>1) {
        MSG_DEBUG("Found more than one status 22 top! Keeping only the last one.");
        v_statusOther_top = vector<const GenParticle*>(v_statusOther_top.end() - 1, v_statusOther_top.end());
      }

      if(v_statusOther_antitop.size()>1) {
        MSG_DEBUG("Found more than one status 22 antitop! Keeping only the last one");
        v_statusOther_antitop = vector<const GenParticle*>(v_statusOther_antitop.end() - 1, v_statusOther_antitop.end());
      }
      // =======================================

      Particle* top=0;
      Particle* antitop=0;

      // If there are status 3 tops and no status 155 tops this is probably a Pythia event, so used the status 3s.
      if(v_status3_top.size() == 1 && v_status3_antitop.size()==1 && v_status155_top.size() == 0 && v_status155_antitop.size()==0) {
        top     = new Particle(v_status3_top[0]);
        antitop = new Particle(v_status3_antitop[0]);
      }

      // If there are status 155 tops this must be a Herwig event, so use the status 155s.
      if( v_status155_top.size() == 1 && v_status155_antitop.size()==1 && v_status3_top.size() == 0 && v_status3_antitop.size()==0) {
        top     = new Particle(v_status155_top[0]);
        antitop = new Particle(v_status155_antitop[0]);
      }

       // If there are tops with other status this must be a Pythia8 event, so use them.
      if( v_statusOther_top.size() == 1 && v_statusOther_antitop.size()==1 && v_status155_top.size() == 0 && v_status155_antitop.size()==0 && v_status3_top.size()==0 && v_status3_antitop.size()==0) {
        top     = new Particle(v_statusOther_top[0]);
        antitop = new Particle(v_statusOther_antitop[0]);
      }

      // If there are status 155 tops this must be a Herwig event, so use the status 155s.                                                                           
      if( v_status11_top.size() == 1 && v_status11_antitop.size()==1 && v_status3_top.size() == 0 && v_status3_antitop.size()==0) {
        top     = new Particle(v_status11_top[0]);
        antitop = new Particle(v_status11_antitop[0]);
      }


      if(top && antitop) {
        FourMomentum ttbar = top->momentum() + antitop->momentum();
        double ttbar_mass = ttbar.mass();
        double ttbar_pt = ttbar.pt();
        double ttbar_rap = ttbar.absrap();
        _h_ttbar_m->fill(ttbar_mass, weight);
        _h_ttbar_pt->fill(ttbar_pt, weight);
        _h_ttbar_rap->fill(ttbar_rap, weight);

        _h_t_tbar_pt->fill(top->pt(), weight);
        _h_t_tbar_pt->fill(antitop->pt(), weight);
      } else {
        MSG_ERROR("Did not find both top and anti-top!");
        vetoEvent;
      }

      if(top) delete top;
      if(antitop) delete antitop;

    }


    /// Normalise histograms etc., after the run
    void finalize() {

      normalize(_h_t_tbar_pt);
      normalize(_h_ttbar_m);
      normalize(_h_ttbar_pt);
      normalize(_h_ttbar_rap);

    }



  private:

    Histo1DPtr _h_ttbar_m;
    Histo1DPtr _h_ttbar_pt;
    Histo1DPtr _h_ttbar_rap;
    Histo1DPtr _h_t_tbar_pt;


  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(ATLAS_2014_I1304289);

}
