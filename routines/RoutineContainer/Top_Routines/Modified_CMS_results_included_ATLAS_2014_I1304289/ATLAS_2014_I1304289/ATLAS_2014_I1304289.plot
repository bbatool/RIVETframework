# BEGIN PLOT /ATLAS_2014_I1304289/d0[12345]-x01-y01
GofLegend=0
LegendYPos=0.90
LegendXPos=0.45
DataTitle=\shortstack[l]{ATLAS Data, \\ Phys.Rev.D90 (2014) 072004}
# END PLOT


# BEGIN PLOT /ATLAS_2014_I1304289/d2[0123456789]-x01-y01
GofLegend=0
LegendYPos=0.90
LegendXPos=0.45
DataTitle=\shortstack[l]{CMS Data, \\ Eur. Phys. J. C 73 (2013) 2339}
# END PLOT

# BEGIN PLOT /ATLAS_2014_I1304289/d01-x01-y01
Title=Transverse momenta of parton-level top quarks (ATLAS)
XLabel=$\mathrm{p}_{\mathrm{T}}^t$ [GeV]
YLabel=$1/\sigma\, d\sigma / d\mathrm{p}_{\mathrm{T}}^t$ [GeV$^{-1}$]
LegendXPos=0.46
GofLegend=0
RatioPlotErrorBandColor=lightgray
#RatioPlotMode=mcdata
# END PLOT

# BEGIN PLOT /ATLAS_2014_I1304289/d02-x01-y01
Title=Invariant mass of parton-level $t\bar{t}$ system (ATLAS)
XLabel=$\mathrm{m}_{t\bar{t}}$ [GeV]
YLabel=$1/\sigma\, d\sigma / d\mathrm{m}_{t\bar{t}}$ [GeV$^{-1}$]
GofLegend=0
RatioPlotErrorBandColor=lightgray
#RatioPlotMode=mcdata
# END PLOT

# BEGIN PLOT /ATLAS_2014_I1304289/d03-x01-y01
Title=Transverse momentum of parton-level $t\bar{t}$ system (ATLAS)
XLabel=$\mathrm{p}_\mathrm{T}^{t\bar{t}}$ [GeV]
YLabel=$1/\sigma\, d\sigma / d\mathrm{p}_\mathrm{T}^{t\bar{t}}$ [GeV$^{-1}$]
GofLegend=0
RatioPlotErrorBandColor=lightgray
#RatioPlotMode=mcdata
# END PLOT

# BEGIN PLOT /ATLAS_2014_I1304289/d04-x01-y01
Title=Rapidity of parton-level $t\bar{t}$ system (ATLAS)
XLabel=$|y_{t\bar{t}}|$
YLabel=$1/\sigma\,d\sigma / d|y_{t\bar{t}}|$
GofLegend=0
RatioPlotYMin=0.9
RatioPlotYMax=1.15
RatioPlotErrorBandColor=lightgray
#RatioPlotMode=mcdata
# END PLOT



# BEGIN PLOT /ATLAS_2014_I1304289/d20-x01-y01
Title=Transverse momenta of parton-level top quarks [l+jets] (CMS)
XLabel=$\mathrm{p}_{\mathrm{T}}^t$ [GeV]
YLabel=$1/\sigma\, d\sigma / d\mathrm{p}_{\mathrm{T}}^t$ [GeV$^{-1}$]
LegendXPos=0.46
LogY=0
GofLegend=0
RatioPlotErrorBandColor=lightgray
#RatioPlotMode=mcdata
# END PLOT

# BEGIN PLOT /ATLAS_2014_I1304289/d21-x01-y01
Title=Invariant mass of parton-level $t\bar{t}$ system [l+jets] (CMS)
XLabel=$\mathrm{m}_{t\bar{t}}$ [GeV]
YLabel=$1/\sigma\, d\sigma / d\mathrm{m}_{t\bar{t}}$ [GeV$^{-1}$]
LegendXPos=0.46
LogY=0
GofLegend=0
RatioPlotErrorBandColor=lightgray
#RatioPlotMode=mcdata
# END PLOT

# BEGIN PLOT /ATLAS_2014_I1304289/d22-x01-y01
Title=Transverse momentum of parton-level $t\bar{t}$ system [l+jets] (CMS)
XLabel=$\mathrm{p}_\mathrm{T}^{t\bar{t}}$ [GeV]
YLabel=$1/\sigma\, d\sigma / d\mathrm{p}_\mathrm{T}^{t\bar{t}}$ [GeV$^{-1}$]
LegendXPos=0.46
LogY=0
GofLegend=0
RatioPlotErrorBandColor=lightgray
#RatioPlotMode=mcdata
# END PLOT

# BEGIN PLOT /ATLAS_2014_I1304289/d23-x01-y01
Title=Rapidity of parton-level $t\bar{t}$ system [l+jets] (CMS)
XLabel=$y_{t\bar{t}}$
YLabel=$1/\sigma\,d\sigma / dy_{t\bar{t}}$
LegendXPos=0.46
LogY=0
GofLegend=0
RatioPlotErrorBandColor=lightgray
#RatioPlotMode=mcdata
# END PLOT

# BEGIN PLOT /ATLAS_2014_I1304289/d24-x01-y01
Title=Rapidity of parton-level top-quarks [l+jets] (CMS)
XLabel=$y_{t}$
YLabel=$1/\sigma\,d\sigma / dy_{t}$
LegendXPos=0.46
LogY=0
GofLegend=0
RatioPlotErrorBandColor=lightgray
#RatioPlotMode=mcdata
# END PLOT




# BEGIN PLOT /ATLAS_2014_I1304289/d25-x01-y01
Title=Transverse momenta of parton-level top quarks [dilep] (CMS)
XLabel=$\mathrm{p}_{\mathrm{T}}^t$ [GeV]
YLabel=$1/\sigma\, d\sigma / d\mathrm{p}_{\mathrm{T}}^t$ [GeV$^{-1}$]
LegendXPos=0.46
LogY=0
GofLegend=0
RatioPlotErrorBandColor=lightgray
#RatioPlotMode=mcdata
# END PLOT

# BEGIN PLOT /ATLAS_2014_I1304289/d26-x01-y01
Title=Invariant mass of parton-level $t\bar{t}$ system [dilep] (CMS)
XLabel=$\mathrm{m}_{t\bar{t}}$ [GeV]
YLabel=$1/\sigma\, d\sigma / d\mathrm{m}_{t\bar{t}}$ [GeV$^{-1}$]
LegendXPos=0.46
LogY=0
GofLegend=0
RatioPlotErrorBandColor=lightgray
#RatioPlotMode=mcdata
# END PLOT

# BEGIN PLOT /ATLAS_2014_I1304289/d27-x01-y01
Title=Transverse momentum of parton-level $t\bar{t}$ system [dilep] (CMS)
XLabel=$\mathrm{p}_\mathrm{T}^{t\bar{t}}$ [GeV]
YLabel=$1/\sigma\, d\sigma / d\mathrm{p}_\mathrm{T}^{t\bar{t}}$ [GeV$^{-1}$]
LegendXPos=0.46
LogY=0
GofLegend=0
RatioPlotErrorBandColor=lightgray
#RatioPlotMode=mcdata
# END PLOT

# BEGIN PLOT /ATLAS_2014_I1304289/d28-x01-y01
Title=Rapidity of parton-level $t\bar{t}$ system [dilep] (CMS)
XLabel=$y_{t\bar{t}}$
YLabel=$1/\sigma\,d\sigma / dy_{t\bar{t}}$
LegendXPos=0.46
LogY=0
GofLegend=0
RatioPlotErrorBandColor=lightgray
#RatioPlotMode=mcdata
# END PLOT

# BEGIN PLOT /ATLAS_2014_I1304289/d29-x01-y01
Title=Rapidity of parton-level top-quarks [dilep] (CMS)
XLabel=$y_{t}$
YLabel=$1/\sigma\,d\sigma / dy_{t}$
LegendXPos=0.46
LogY=0
GofLegend=0
RatioPlotErrorBandColor=lightgray
#RatioPlotMode=mcdata
# END PLOT



# BEGIN PLOT /ATLAS_2014_I1304289/top_rap
Title=Rapidity of parton-level top-quarks (MC only)
XLabel=$y_{t}$
YLabel=$1/\sigma\,d\sigma / dy_t$
GofLegend=0
RatioPlotYMin=0.9
RatioPlotYMax=1.15
RatioPlotErrorBandColor=lightgray
#RatioPlotMode=mcdata
# END PLOT

# BEGIN PLOT /ATLAS_2014_I1304289/ttbar_rap
Title=Rapidity of parton-level $t\bar{t}$-system (MC only)
XLabel=$y_{t\bar{t}}$
YLabel=$1/\sigma\,d\sigma / dy_{t\bar{t}}$
GofLegend=0
RatioPlotYMin=0.9
RatioPlotYMax=1.15
RatioPlotErrorBandColor=lightgray
#RatioPlotMode=mcdata
# END PLOT

# BEGIN PLOT /ATLAS_2014_I1304289/ttbar_absrap
Title=Rapidity of parton-level $t\bar{t}$-system (MC only)
XLabel=$|y_{t\bar{t}}|$
YLabel=$1/\sigma\,d\sigma / d|y_{t\bar{t}}|$
GofLegend=0
RatioPlotYMin=0.9
RatioPlotYMax=1.15
RatioPlotErrorBandColor=lightgray
#RatioPlotMode=mcdata
# END PLOT