// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Tools/Logging.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/LeadingParticlesFinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Cuts.hh"


namespace Rivet {


  class MC_fragmentation : public Analysis {
  public:

    /// @name Constructors etc.
    //@{

    /// Constructor
    MC_fragmentation()
      : Analysis("MC_fragmentation")
    {
      setNeedsCrossSection(false);
    }

    //@}


  public:

    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      Cut eta_full = Rivet::Cuts::etaIn(-5.0, 5.0) & (Rivet::Cuts::pT >= 1.0*MeV);
      Cut eta_lep = Rivet::Cuts::etaIn(-2.5, 2.5);


      FinalState fs(eta_full);

      //      IdentifiedFinalState photons(fs);
      //      photons.acceptIdPair(PID::PHOTON);

      // projection to find FS gamma-s 
      std::vector<std::pair<double, double> > eta_gamma;  
      eta_gamma.push_back(make_pair(-3.0,3.0));  
      // IdentifiedFinalState gammas(eta_gamma, 0.100*GeV);  
      IdentifiedFinalState gammas(fs);
      gammas.acceptId(PID::PHOTON);  
      addProjection(gammas, "gammas"); 

      // projection to find the muons
      std::vector<std::pair<double, double> > _eta_m;
      _eta_m.push_back(make_pair(-2.50,+2.50));
      // IdentifiedFinalState muons(_eta_m, 5.0*GeV);
      IdentifiedFinalState muons(fs);
      muons.acceptIdPair(PID::MUON);
      addProjection(muons, "muons");

      // projection to find neutrinos for met
      IdentifiedFinalState neutrinos(-4.5, 4.5, 0.0*GeV);
      neutrinos.acceptNeutrinos();
      addProjection(neutrinos, "neutrinos");

      // Input for the jets: including muons, neutrinos
      //  FinalState fs;
      FastJets jets(fs, FastJets::ANTIKT, 0.4);
      addProjection(jets, "jets");

      // charged particles 
      ChargedFinalState cfs(-3.0, 3.0, 0.500*GeV);
      addProjection(cfs, "CFS");

      /// book histograms
      // # jets matched to B-hadrons
      _h_bhadjet_pT = bookHisto1D("bhadjet_pT", 350, 0, 350);
      _h_bhadjet_N = bookHisto1D("bhadjet_N", 20, 0, 20);     
      
      // b-hadron pT distribution
      _h_bhadron_pT = bookHisto1D("bhadron_pT", 350, 0, 350);
      _h_bhadron_N = bookHisto1D("bhadron_N", 20, 0, 20); 

      // particles close to B:
      _h_gamma_sumpT = bookHisto1D("gamma_sumpT", 1000, 0, 1000);  
      _h_gamma_pT = bookHisto1D("gamma_pT", 1000, 0, 1000);  
      _h_gamma_N = bookHisto1D("gamma_N", 100, 0, 100); 

      _h_muon_sumpT = bookHisto1D("muon_sumpT", 1000, 0, 1000);  
      _h_muon_pT = bookHisto1D("muon_pT", 1000, 0, 1000); 
      _h_muon_N = bookHisto1D("muon_N", 100, 0, 100); 

      _h_charged_sumpT = bookHisto1D("charged_sumpT", 1000, 0, 1000); 
      _h_charged_pT = bookHisto1D("charged_pT", 250, 0, 250); 
      _h_charged_N = bookHisto1D("charged_N", 5000, 0, 5000); 
      
      _h_neutrino_sumpT = bookHisto1D("neutrino_sumpT", 1000, 0, 1000);       
      _h_neutrino_pT = bookHisto1D("neutrino_pT", 250, 0, 250); 
      _h_neutrino_N = bookHisto1D("neutrino_N", 100, 0, 100); 
      
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      
      const double _weight = event.weight();

      
      /// prepare B-hadrons using a patch of ATLAS_2012_I1094568
      /// * Temporary fix to get B-hadrons in evgen files where they don't show up
      ///   in the UnstableFinalState projection e.g. mc10_7TeV.105200.T1_McAtNlo_Jimmy.evgen.EVNT.e598/
      std::vector<HepMC::GenParticle*> _B_hadrons;
      std::vector<HepMC::GenParticle*> _allParticles = particles(event.genEvent());
      for(unsigned int _i = 0; _i < _allParticles.size(); ++_i) {
        GenParticle* _p = _allParticles.at(_i);
        if ( !(Rivet::PID::isHadron( _p->pdg_id() ) && Rivet::PID::hasBottom( _p->pdg_id() )) ) continue;
        if(_p->momentum().perp()*GeV < 5.0) continue;
	_h_bhadron_pT->fill(_p->momentum().perp()/GeV, _weight);
        _B_hadrons.push_back(_p);
      }
      _h_bhadron_N->fill(_B_hadrons.size(),_weight);

      const ChargedFinalState& charged = applyProjection<ChargedFinalState>(event, "CFS");
      const FinalState& muons = applyProjection<FinalState>(event, "muons");
      const FinalState& neutrinos = applyProjection<FinalState>(event, "neutrinos");
      const FinalState& gammas = applyProjection<FinalState>(event, "gammas"); 
      
      // jets
      Jets  _bhadjets;
      foreach (const Jet& _jet, applyProjection<FastJets>(event, "jets").jetsByPt(25.0*GeV)) {
	if (fabs(_jet.eta()) < 2.5) {
	  // jet B-hadron matching
	  foreach(HepMC::GenParticle* _b, _B_hadrons) {
	    FourMomentum _hadronfm = _b->momentum();
	    double _hadron_jet_dR = deltaR(_jet.momentum(), _hadronfm);
	    if(_hadron_jet_dR < 0.4) {
	      _bhadjets.push_back(_jet);
	      _h_bhadjet_pT->fill(_jet.momentum().pT()/GeV, _weight);
	    }
	    continue;
	  }
	}
      }
      _h_bhadjet_N->fill(_bhadjets.size(),_weight);

      // particles close to B-hadrons:
      // jet B-hadron matching
      foreach(HepMC::GenParticle* _b, _B_hadrons) {
	FourMomentum _hadronfm = _b->momentum();
	
	double gamma_sumpT, muon_sumpT, charged_sumpT, neutrino_sumpT;
	gamma_sumpT=muon_sumpT=charged_sumpT=neutrino_sumpT=0.;
	int gamma_N, muon_N, charged_N, neutrino_N;
	gamma_N=muon_N=charged_N=neutrino_N=0;	
	
	foreach(const Particle& _p, neutrinos.particles()) {
	  double _dR = deltaR(_p.momentum(), _hadronfm);
	  if (_dR < 0.4) {
	    neutrino_sumpT+=_p.momentum().pT();
	    _h_neutrino_pT->fill(_p.momentum().pT()/GeV, _weight);
	    ++neutrino_N;
	  }
	}
	_h_neutrino_N->fill(neutrino_N, _weight);
	_h_neutrino_sumpT->fill(neutrino_sumpT/GeV, _weight);
	
	foreach(const Particle& _p, muons.particles()) {
	  double _dR = deltaR(_p.momentum(), _hadronfm);
	  if (_dR < 0.4) {
	    muon_sumpT+=_p.momentum().pT();
	    _h_muon_pT->fill(_p.momentum().pT()/GeV, _weight);
	    ++muon_N;
	  }
	}
	_h_muon_N->fill(muon_N, _weight);
	_h_muon_sumpT->fill(muon_sumpT/GeV, _weight);	

	foreach(const Particle& _p, gammas.particles()) {
	  double _dR = deltaR(_p.momentum(), _hadronfm);
	  if (_dR < 0.4) {
	    gamma_sumpT+=_p.momentum().pT();
	    _h_gamma_pT->fill(_p.momentum().pT()/GeV, _weight);
	    ++gamma_N;
	  }
	}
	_h_gamma_N->fill(gamma_N, _weight);
	_h_gamma_sumpT->fill(gamma_sumpT/GeV, _weight);	
	
	foreach (const Particle& _p, charged.particles()) {
	  double _dR = deltaR(_p.momentum(), _hadronfm);
	  if (_dR < 0.4) {
	    charged_sumpT+=_p.momentum().pT();
	    _h_charged_pT->fill(_p.momentum().pT()/GeV, _weight);
	    ++charged_N;
	  }
	}
	_h_charged_N->fill(charged_N, _weight);	  
	_h_charged_sumpT->fill(charged_sumpT/GeV, _weight);
      }

    } // end of analyze()

    void finalize() {}

    //@}

  private:

    /// @name Histograms
    //@{
    Histo1DPtr _h_bhadjet_pT ;    
    Histo1DPtr _h_bhadjet_N;
    Histo1DPtr _h_bhadron_pT ;    
    Histo1DPtr _h_bhadron_N;
    Histo1DPtr _h_gamma_sumpT ;       
    Histo1DPtr _h_gamma_pT ;    
    Histo1DPtr _h_gamma_N;
    Histo1DPtr _h_muon_sumpT ;       
    Histo1DPtr _h_muon_pT ;    
    Histo1DPtr _h_muon_N;
    Histo1DPtr _h_charged_sumpT ;       
    Histo1DPtr _h_charged_pT ;    
    Histo1DPtr _h_charged_N;
    Histo1DPtr _h_neutrino_sumpT ;       
    Histo1DPtr _h_neutrino_pT ;    
    Histo1DPtr _h_neutrino_N;
    //@}

  };

  // This global object acts as a hook for the plugin system
  AnalysisBuilder<MC_fragmentation> plugin_MC_fragmentation;

}

