# BEGIN PLOT /ATLAS_ttHF/*
RatioPlotYMin=0.0
RatioPlotYMax=2.0
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet.._mass..
Rebin=5
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet12_mass..
Title=Invariant mass of 1st and 2nd $b$-jets
XLabel=$m_{bb}$ [GeV]
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet13_mass..
Title=Invariant mass of 1st and 3rd $b$-jets
XLabel=$m_{bb}$ [GeV]
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet14_mass..
Title=Invariant mass of 1st and 4th $b$-jets
XLabel=$m_{bb}$ [GeV]
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet23_mass..
Title=Invariant mass of 2nd and 3rd $b$-jets
XLabel=$m_{bb}$ [GeV]
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet24_mass..
Title=Invariant mass of 2nd and 4th $b$-jets
XLabel=$m_{bb}$ [GeV]
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet34_mass..
Title=Invariant mass of 3rd and 4th $b$-jets
XLabel=$m_{bb}$ [GeV]
# END PLOT


# BEGIN PLOT /ATLAS_ttHF/bjet.._mass..
Rebin=5
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet..pt..
Rebin=5
LegendXPos=0.2
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet1_pt_dilep_ttb
#Legend=0
LogY=1
Title=1st $b$-jet - ttb di-lepton
XLabel=$p_T$ [GeV]
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet1_pt_dilep_ttb
#Legend=0
LogY=1
Title=1st $b$-jet - ttb di-lepton
XLabel=$p_T$ [GeV]
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet2_pt_dilep_ttb
#Legend=0
LogY=1
Title=2nd $b$-jet - ttb di-lepton
XLabel=$p_T$ [GeV]
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet3_pt_dilep_ttb
#Legend=0
LogY=1
XMax=300
Title=3rd $b$-jet - ttb di-lepton
XLabel=$p_T$ [GeV]
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet4_pt_dilep_ttb
#Legend=0
LogY=1
XMax=300
Title=4th $b$-jet - ttb di-lepton
XLabel=$p_T$ [GeV]
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet1_pt_dilep_ttbb_CERN
#Legend=0
LogY=1
Title=1st $b$-jet - ttbb di-lepton (CERN)
XLabel=$p_T$ [GeV]
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet2_pt_dilep_ttbb_CERN
#Legend=0
LogY=1
Title=2nd $b$-jet - ttbb di-lepton (CERN)
XLabel=$p_T$ [GeV]
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet3_pt_dilep_ttbb_CERN
#Legend=0
LogY=1
XMax=300
Title=3rd $b$-jet - ttbb di-lepton (CERN)
XLabel=$p_T$ [GeV]
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet4_pt_dilep_ttbb_CERN
#Legend=0
LogY=1
XMax=300
Title=4th $b$-jet - ttbb di-lepton (CERN)
XLabel=$p_T$ [GeV]
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet1_pt_dilep_ttbb_DESY
#Legend=0
LogY=1
Title=1st $b$-jet - ttbb di-lepton (DESY)
XLabel=$p_T$ [GeV]
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet2_pt_dilep_ttbb_DESY
#Legend=0
LogY=1
Title=2nd $b$-jet - ttbb di-lepton (DESY)
XLabel=$p_T$ [GeV]
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet3_pt_dilep_ttbb_DESY
#Legend=0
LogY=1
XMax=300
Title=3rd $b$-jet - ttbb di-lepton (DESY)
XLabel=$p_T$ [GeV]
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet4_pt_dilep_ttbb_DESY
#Legend=0
LogY=1
XMax=300
Title=4th $b$-jet - ttbb di-lepton (DESY)
XLabel=$p_T$ [GeV]
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet1_pt_ljets_ttb
#Legend=0
LogY=1
Title=1st $b$-jet - ttb lepton+jets
XLabel=$p_T$ [GeV]
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet2_pt_ljets_ttb
#Legend=0
LogY=1
Title=2nd $b$-jet - ttb lepton+jets
XLabel=$p_T$ [GeV]
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet3_pt_ljets_ttb
#Legend=0
LogY=1
XMax=300
Title=3rd $b$-jet - ttb lepton+jets
XLabel=$p_T$ [GeV]
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet4_pt_ljets_ttb
#Legend=0
LogY=1
XMax=300
Title=4th $b$-jet - ttb lepton+jets
XLabel=$p_T$ [GeV]
# END PLOT


# BEGIN PLOT /ATLAS_ttHF/bjet_mult_dilep_ttb
#Legend=0
LogY=1
XMinorTickMarks=0
Title=$b$-jet multiplicity - ttb di-lepton
XLabel=$N_{b-\mathrm{jets}}$
LegendXPos=0.8
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet_mult_dilep_ttbb_CERN
#Legend=0
LogY=1
XMinorTickMarks=0
Title=$b$-jet multiplicity - ttbb di-lepton (CERN)
XLabel=$N_{b-\mathrm{jets}}$
LegendXPos=0.8
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet_mult_dilep_ttbb_DESY
#Legend=0
LogY=0
XMinorTickMarks=0
Title=$b$-jet multiplicity - ttbb di-lepton (DESY)
XLabel=$N_{b-\mathrm{jets}}$
LegendXPos=0.8
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet_mult_ljets_ttb
LogY=1
XMinorTickMarks=0
Title=$b$-jet multiplicity - ttb lepton+jets
XLabel=$N_{b-\mathrm{jets}}$
LegendXPos=0.8
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/bjet_mult_nocuts
#Legend=0
LogY=1
XMinorTickMarks=0
Title=$b$-jet multiplicity - Before selection cuts
XLabel=$N_{b-\mathrm{jets}}$
YMin=1
YMax=5E8
LegendXPos=0.2
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/weights2
#Legend=0
LogY=0
XMinorTickMarks=0
Title=Sum of weights of all events
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/weights
#Legend=0
LogY=1
XMinorTickMarks=0
Title=Sum of weights of events passing selection
# END PLOT


# BEGIN PLOT /ATLAS_ttHF/softest_bjet_pt_EventsWith3Bjets_ljets_ttb
LogY=1
XLabel=softest $b$-jet $p_T$ [GeV]
Title=Events with exactly 3 $b$-jets (ttb - l+jets)
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/softest_bjet_pt_EventsWith3Bjets_dilep_ttb
LogY=1
XLabel=softest $b$-jet $p_T$ [GeV]
Title=Events with exactly 3 $b$-jets (ttb - dilepton)
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/softest_bjet_pt_EventsWithGT3Bjets_ljets_ttb
LogY=1
XLabel=softest $b$-jet $p_T$ [GeV]
Title=Events with $>3$ $b$-jets (ttb - l+jets)
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/softest_bjet_pt_EventsWithGT3Bjets_dilep_ttb
LogY=1
XLabel=softest $b$-jet $p_T$ [GeV]
Title=Events with $>3$ $b$-jets (ttb - dilepton)
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/NBhadronsInJet_EventsWith3Bjets_..
LogY=1
Title=Number of $b$-hadrons in $b$-jets - Events 3 $b$-jets
XMinorTickMarks=0
XLabel=N $b$-hadrons
NormalizeToIntegral=1
Ymin=5E-3
Ymax=1
# END PLOT

# BEGIN PLOT /ATLAS_ttHF/NBhadronsInJet_EventsWithGT3Bjets..
LogY=1
Title=Number of $b$-hadrons in $b$-jets - Events with $>3$ $b$-jets
XMinorTickMarks=0
XLabel=N $b$-hadrons
NormalizeToIntegral=1
Ymin=5E-3
Ymax=1
# END PLOT
